# Presentación XX

# El derecho de autor mexicano. Una crítica desde Creative Commons

> [name=Salvador Alcántar]

## Mi breve historia

En marzo de 2006 me registré como usuario de Wikipedia, entonces no sabía que mi pertenencia al proyecto se extendería por más de una década. Mi alta en la enciclopedia fue apenas tres meses de graduarme como abogado, así que no es coincidencia que las posturas y filosofía que subyacen a uno de los proyectos libres más grandes del mundo me hayan acompañado en mi crecimiento dentro del derecho.

Escribir en Wikipedia me permitió entender la problemática del acceso universal a la cultura a través de medios digitales de maneras muy concretas: los artículos de la enciclopedia necesitan mejora constante lo que se traduce en la necesidad de fuentes documentales para crearlos y referencias para sostenerlos, sin embargo, aunque existen una gran cantidad de recursos en internet, todavía hay una cantidad inimaginable de espacios vacíos en esa biblioteca digital, que se vuelven más evidentes en la medida en que el artículo es más especializado. Podía encontrar una cantidad enormes de recursos para mejorar un artículo sobre la vida rural romana en el siglo I, pero para mejorar el artículo sobre una divinidad celta poco conocida dependía de una investigación arqueológica escrita en los cincuenta y a la que solo se accede en su versión en inglés a través de JSTOR, siempre y cuando pagara la nada barata suscripción.

Durante mucho tiempo escribía con los recursos que poseía a la mano en bibliotecas y con aquellos libros que podía adquirir, aún cuando el conocimiento especializado no es nada barato y existían prioridades personales que cubrir fuera de Wikipedia. Por entonces, Internet Archive no poseía el acervo que ahora tiene y aún faltaban unos cuantos años para que se lanzara Sci Hub y Library Genesis. Robustecer Wikipedia era una tarea complicada.

No obstante el esfuerzo invertido, el proyecto enciclopédico solicitaba de mí que tras escribir un artículo o mejorarlo, liberara mi trabajo, lo cual me llamó la atención casi de inmediato desde las primeras ediciones. Cuando daba clic en el botón de «Publicar» aceptaba los términos de una licencia «Creative Commons Atribución Compartir-Igual», la cual, como buen abogado, revisé con detenimiento y leí sus condiciones. El derrotero a partir de ahí fue fácil: de la licencia a su creador Lawrence Lessig, de Lessig a sus obras *Code and Other Laws of Cyberspace* y *Free Culture*, pasando por la filosofía y los principios del software libre. Había encontrado otra interpretación de la propiedad intelectual que, según lo veía, de aplicarse generalizadamente quitaría los obstáculos al acceso al conocimiento y que, no obstante, no me habían enseñado en mis clases derecho de autor y que a momentos parecía ser muy diferente a los postulados de la Ley Federal del Derecho de Autor.

La enseñanza del derecho, y específicamente del derecho autoral, por lo común pasa de largo de las formas alternativas de entender lo cultural y exime a quien se forma como abogado de entender la propiedad intelectual bajo esquemas no comerciales. Los derechos de autor en sentido clásico se enseñan y se aplican como meros tributarios de lo mercantil, observan muy de lejos el ámbito de lo cultural y rara vez se entrecruzan con los derechos humanos. No es de sorprender que esas sean, también, las mismas falencias de los argumentos de los abogados que niegan lo libre y lo abierto.

La comunidad wikipédica y luego la comunidad de Wikimedia México, que ayudé a fundar, me permitieron ver de primera mano una gran cantidad de situaciones en donde la legislación mexicana de derecho de autor se volvía difusa, ambigua, contradictoria o insuficiente. Las preguntas fluían: ¿por qué se tienen que pagar las reproducciones de fotografías de sitios arqueológicos que se consideran patrimonio común del país y administradas por un organismo público?, ¿como puedo subir fotografías de monedas mexicanas con la intención de ilustrar artículos sobre numismática o economía sin violentar la ley monetaria?, ¿por qué una editorial tiene derechos sobre la paleografía de un texto virreinal si ya está en dominio público?, ¿por qué los contenidos de algunos sitios gubernamentales dicen «Todos los derechos reservados» si son producidos con presupuesto público y por personas funcionarias en ejercicio de sus funciones?, ¿existe un catálogo de las obras que ya están en dominio público en México?, etcétera.

Dicho de otra forma, el sistema del derecho de autor en México, genera más dudas que certezas para el ambiente de colaboración que se vive hoy en día, no solo en Wikipedia, sino en internet mismo. Las conductas de generación de conocimiento superan por mucho los viejos esquemas del derecho autoral clásico y lo retan cada día.

Al descubrir Creative Commons también encontré una forma de explicar lo libre *desde* el derecho de autor, ya que el acercamiento de lo libre puede pasar por muchas aproximaciones, hasta aquellas que renuncian totalmente a cualquier posibilidad de regular lo cultural, en una especie de anarquismo o nihilismo del derecho de autor, las cuales también poseen puntos de vista bastante válidos y aportaciones valiosas a las que también debe atenderse sin estigmatizarles ni desestimarles.

Al comprometerme a formar parte de Creative Commons México, también tomé responsabilidad con el activismo y la defensa de las licencias libres y renuncié a muchos esquemas obsoletos del derecho de autor heredados por mi formación como abogado, obligándome a repensar muchas cosas. Parte de esas reflexiones se encuentran en este escrito y son una invitación abierta a las y los practicantes del derecho a introducir dentro de su pensamiento un espacio para lo libre.

## Tres objeciones legales a Creative Commons y tres respuestas

En el trabajo de difusión de las licencias Creative Commons en nuestro país, me ha tocado entablar diálogo con otras personas abogadas que defienden una visión más clásica del derecho de autor y quienes opinan, según su propia lectura de las cosas, que la redacción actual de la Ley Federal del Derecho de Autor impide la aplicación plena de las licencias Creative Commons en México. En mi personal reflexión, las objeciones dadas, todas ellas bastante técnicas, no representan mayor conflicto para la eficacia de las cláusulas de las licencias libres, sino que, en el mayor de los casos, sólo representan ajustes en su aplicación.

Las objeciones a que me refiero y que he llegado a colectar en dichos diálogos podrían resumirse en tres:

1.  Dado que la LFDA exige que las licencias deben ser escritas o serán nulas (art. 30), las licencias CC son nulas por no estar dadas por escrito (!).
2. Dado que la LFDA señala que toda transmisión de derechos patrimoniales debe ser onerosa (art. 30) y dicho derecho es irrenunciable (art. 31), por lo tanto las licencias CC son inoperantes en México porque su modelo se basa en la gratuidad.
3. Dado que la LFDA señala que toda transmisión de derechos patrimoniales debe ser temporal (art. 30) y a falta de un término establecido se entenderá un máximo de 5 años en las licencias otorgadas (art. 33), las licencias CC son inoperantes en México dado que se otorgan a perpetuidad.

El primer argumento no debería merecer mucha explicación y podría sorprender a cualquiera que conozca lo que se ha dado por llamar el Código Legal (Legal Code) de las licencias y que constituye una de las tres capas que las conforman,[^1] sin embargo, sigue siendo un argumento popular. Baste enfatizar, que el Código Legal cumple con las funciones de un acuerdo entre partes, con la peculiaridad que la versión escrita de dicho acuerdo se encuentra en una página web y aceptado de manera tácita al momento de usar el objeto cultural licenciado en Creative Commons. La persona que toma un producto con licencia libre y la usa, está diciendo sí a los términos y condiciones de su texto. Es por eso que el texto duro de todas las licencias comienza con la frase: «Al ejercer los Derechos Licenciados (definidos a continuación), Usted acepta y acuerda estar obligado por los términos y condiciones de esta Licencia». O en una forma más breve: usar es obligarse.

El permiso de uso de una creación que se realiza mediante Creative Commons no se otorga a una persona específica sino a un público indeterminado a través de Internet y de manera no exclusiva. Si quisiéramos forzarla a entrar a las categorías de la teoría jurídica mexicana podríamos decir que el licenciamiento CC tiene bastante aproximación a una declaración unilateral de la voluntad sin plazo de aceptación, la cual se acepta por medio de una expresión tácita de la voluntad manifestada a través del uso. (Rojas Amandi, 2008:) Sin embargo, se ha de reconocer que la teoría civil de las obligaciones se ajusta apenas con dificultad, debido a que la teoría obligacionista se ha quedado corta y anticuada ante las prácticas digitales. 

Esta dificultad de entendimiento de la licencia dentro del sistema jurídico mexicano y su separación respecto a otros contratos autorales, proviene también del hecho de que la licencia como instrumento de derecho privado es más bien raro, casi exclusivo de la propiedad intelectual, donde lo encontramos tanto en el ámbito autoral como para marcas y patentes. Antes de los 90, en México, el concepto de licencia se encontraba ceñido únicamente a la idea del permiso de una autoridad para realizar una acción. No fue hasta 1996 que la LFDA la exporta bajo influencia del derecho estadounidense en sus esfuerzos por crear un bloque económico con América del Norte y dar seguimiento a otros compromisos internacionales en materia de derecho de autor.

En la legislación estadounidense por ejemplo, el concepto de licencia, además del concepto admnistrativo, tiene una genealogía dentro del derecho privado (*land law*) como el permiso de una persona propietaria para conceder una acción sobre sus terrenos, bajo cláusulas que él o ella establecían, en su propio formato, con poco espacio para negociación, que no transmitía ni constituía derechos de propiedad, revocable unilateralmente y que bajo ciertas circunstancias surtía efectos con la sola firma de la persona dueña. (Garner, Black, & et. al., 2004: 1002) Con un entendimiento parecido, este término se integró al copyright como un permiso otorgado por el titular de los derechos, que bien puede negociarse o solo se accede a él tal como está estipulado. Incluso hoy, las prácticas autorales reconocen la posibilidad de que una licencia se realicen «*bajo el formato de la persona titular del derecho de autor, con poco o ninguna posibilidad de negociación*». reconociendo, además, la existencia de licencias exclusivas y no-exclusivas, donde estas últimas no necesitan mayores formalidades. (Word Intellectual Property Organization, 2005, p. 5 y 6)

Ciertamente, el entendimiento de una licencia como un acto unilateral, del que no se espera consentimiento del licenciante para perfeccionarse sino que solo se le obliga a actuar conforme a los términos establecidos, posee mucho más sentido que forzarlo a ser un contrato de consentimientos recíprocos. (Hietanen, 2007, p. 6)

Para cerrar el argumento anterior quisiera recordar que en algún momento leí una opinión de un despacho especializado en derecho de autor donde respondía a la solicitud de una autoridad mexicana sobre la pertinencia en el uso de las licencias Creative Commons en repositorios institucionales. En la respuesta se alegaba que las licencias fueron una creación teórica estadounidense, por lo que no encontraban cabida en nuestro derecho. Y bien, dada la explicación anterior, queda claro que desde que la Ley Federal del Derecho Autoral incluyó el licenciamiento como herramienta del derecho civil aún cuando era ajeno a nuestra tradición jurídica, las licencias Creative Commons también obtuvieron carta de naturalización.

En lo que respecta a las objeciones 2 y 3 debemos enfatizar en que la intención legal de las licencias CC no radica en la transmisión de los derechos patrimoniales sino en una autorización para el uso de dichos derechos por un público indefinido. Esta distinción es incluso muy propia del derecho de autor mexicano, ya que existe diferencia entre transferir como tal un derecho y otorgar permiso para su uso. Hasta el artículo 30 de la LFDA opone como diferentes a uno y otro «*transferir sus derechos patrimoniales u otorgar licencias de uso exclusivas o no exclusivas*», el secreto pues está en esa «u». Mientras en el primer acto los derechos salen de la esfera jurídica personal y se abonan a la de otro, en el segundo permanecen con el titular quien solamente extiende su derecho de uso hacia un tercero. Y mientras las transmisiones deben poseer los requisitos de los argumentos 2 y 3, de las licencias la ley solo exige que estén por escrito. (Artega Alvarado, 2008, p. 246) Es importantísimo distinguir que una licencia, al ser un permiso otorgado unilateralmente por una persona, posee una naturaleza distinta a la de un contrato bilateral para la transmisión de derechos.
 
En ambos casos, la intencionalidad es muy importante, y siempre ha quedado claro que la finalidad de las licencias CC es compartir y no generar una renuncia y transmisión de derechos. Pero este compartir sin lucro y sin renuncia es el mismo principio que causa extrañeza al sistema jurídico mexicano que encuadra al derecho de autor como parte del ejercicio mercantil, porque el lucro siempre siempre verá con reservas al altruismo. En la propuesta de Creative Commons, la tónica está puesto en el uso creativo, no en la explotación.

Esta confusión entre la transmisión y el licenciamiento debe provenir, necesariamente, de la práctica corporativa del derecho de autor, ya que las grandes industrias que lucran con la creatividad siempre generan contratos que de alguna forma u otra establecen la transmisión de derechos patrimoniales en contratos predadores que dejan desnuda, jurídicamente hablando, a la persona creadora; pero que al mismo tiempo otorgano los permisos propios de una licencia aunque, por lo general, de forma exclusiva.

Adicionalmente, aún cuando aceptáramos la idea de que cualquiera de las licencias CC implican la transmisión de un derecho patrimonial, ni la gratuidad ni la perpetuidad tampoco representan un verdadero obstáculo para que las licencias surtan efectos en territorio mexicano.

La LFDA establece la remuneración y la temporalidad como principios, pero no como elementos indispensables (*sine qua non*, diríamos pedantemente las personas practicantes del derecho) para la existencia de una transmisión efectiva. Y esto se deduce desde la redacción de la propia ley que omite establecer una consecuencia de nulidad cuando no se cumplen estos principios:

> En ausencia de acuerdo sobre el monto de la remuneración o del procedimiento para fijarla, así como sobre los términos para su pago, la determinarán los tribunales competentes. (Ley Federal del Derecho de Autor, art. 30)

Si la ley quisiera penalizar la ausencia de precio y tiempo de una transmisión o licencia diría algo como «serán nulas de pleno derecho» o «se considerarán inválidas», pero en lugar de eso deja abierta la posibilidad de que esos detalles (que ahora vemos que son menores), se determinen en juicio. Es decir, cuando no hay onerosidad ni tiempo de vigencia la licencia es válida pero con dos detalles dejados a determinación futura. Incluso si no se llegan a determinar jamás, la ley nunca penaliza con nulidad o invalidez estos acuerdos. Al final, es potestad de la persona autora ejercer este derecho a cobrar y establecer un tiempo o no. En el caso de las licencias Creative Commons, la creadora simplemente no hace uso de esos derechos o, dicho de otra forma, los deja perpetuamente indeterminados.

Ya que hemos planteado el «derecho a la onerosidad», tal vez sería tiempo que comenzarámos a pensar también en el «derecho a la gratuidad» o expuesto más extensamente: el derecho personal a otorgar gratuitamente. ¿La autodeterminación socioeconómica de una persona no implica también la posibilidad de otorgar su obra sin esperar una compensación económica a cambio? Siempre y cuando este otorgamiento gratuito sea claro, plenamente informado y sin vicios del consentimiento, nada tendría que obstaculizar para compartir. La ley mexicana, cerrada en su visión comercial y ajena a las formas de hacer comunidad que el Internet ha permitido, obviamente se negó esa posibilidad, pero ya va siendo tiempo en que la ley se abra al altruismo digital y la posibilidad de compartir sin cobrar las creaciones intelectuales. Lo merecemos y lo necesitamos.

Una cosa más queda pendiente, quienes defienden la tercera objeción aportan la idea de que conforma a la LFDA ninguna licencia puede ser otorgada a perpetuidad. Ciertamente, el texto de estas licencias libres otorga el permiso de uso de manera perpetua, y es lógico considerando que el uso se otorga a una generalidad indeterminada a través de los medios digitales, por lo que pueden ser tomadas en cualquier momento, lo que hace el seguimiento del inicio y fin de dicho término casi imposible tanto en el número de casos como en su cronología. Recordemos que ese límite temporal es un remanente de las prácticas de derecho de autor de una época pre internet.

Sin embargo, las licencias en esto también se atienen a lo que dictan las legislaciones locales. Así que esta perpetuidad no afecta la validez o existencia jurídica las licencias Creative Commons en México, sino que, en el peor de los casos, podrían considerarse otorgadas, de inicio, solo por 5 años. No obstante, la no oposición de la persona creadora tras ese término debería entenderse como una renovación indefinida de esos cinco años, en tanto la obra posea licencia Creative Commons, lo que sucede en la mayor parte de los casos. En la naturaleza del compartir al mundo está también la necesidad de hacerlo para siempre. Ya es tiempo de que la ley y sus interpretaciones se abran al altruismo digital.

## El dominio público y sus debilidades

El Manifiesto por el dominio público es una iniciativa europea encabezada por la red COMMUNIA, que establece ciertos principios generales sobre cómo debe entenderse y extenderse el dominio público. Sus conclusiones son valiosas no solo para Europa sino para todo el globo, ya que parte del principio de que el dominio público es una «*riqueza de información*», la cual, a diferencia de otros tipos de riqueza, al estar disponible, beneficia a todos y todas. Sin ánimo de extenderme sobre los detalles de ese valioso documento, me gustaría tomar el primero de sus principios generales como punto de partida de esta sección:

> El dominio público es la regla, la protección del derecho de autor la excepción. (Rosnay y Martin, 2012, p. xxii)

Este principio pone de relieve la forma sesgada en la que el derecho de autor contemporáneo entiende el dominio público: como una simple extinción del derecho privado de autor. Las consideraciones jurídicas clásicas toman al dominio público como una forma negativa del derecho autoral, una suerte de muerte de las obras, cuando la realidad es que el dominio marca el nacimiento de un derecho colectivo, una forma específica del derecho humano del acceso a la cultura. No es casualidad que la Declaración Universal de los Derechos Humanos, en su artículo 27, hable de la «vida cultural de la comunidad» para definir el acceso a la cultura, ya que la comunidad es el único lugar donde la cultura cobra significado y es esa misma comunidad la que hace los reconocimientos pertinentes para generar esos derechos exclusivos de una persona sobre su creación a los que llamamos propiedad intelectual. Sin comunidad, no hay cultura, ni derechos de autor, de ahí que es prerrogativa de la comunidad reclamar la propiedad de los bienes culturales con preferencia al individuo.

El manifiesto establece en su justa medida la verdadera extensión del derecho de autor como privilegio temporal, limitado en sus efectos y en el número de personas que beneficia, que en realidad existe dentro del amplio concepto de la cultura, el cual es un continuo que se extiende por toda la historia humana. Los derechos de autor habitan dentro de la cultura, son un paréntesis dentro de ella, por eso surgen de la comunidad y deben tender hacia la comunidad. El dominio público es solo el punto en que la obra entra a formar parte de la cultura colectiva, cobra vida comunitaria y regresa al punto de partida. 

Algo más debe ser dicho respecto ciertas concepciones jurídicas tan limitantes dentro del derecho de autor, que consiste en que sobre los derechos patrimoniales pesa una relación ideológica muy fuerte: la idea de que el patrimonio sobre lo intangible se equipara en extensión y efectos al patrimonio sobre lo material, esto es, sobre la propiedad del derecho civil. Así es, quienes defienden a ultranza la inextinguibilidad de los derechos autorales quieren revestirlo de las mismas formalidades que se le otorgan a la propiedad sobre una bien físico, una casa por ejemplo. La existencia del dominio público es la gran excepción que demuestra que ambos derechos no pueden equipararse.

(Alerta de lenguaje abogadil: los siguientes tres párrafos poseen terminología jurídica aburrida, si usted no es una persona abogada o simplemente no quiere aburrirse, puede saltárselos.)

La teoría civil clásica reconoce la propiedad privada como una concesión estatal a los privados respecto a la propiedad originaria de la nación (CPEUM, art. 27), es decir, un derecho fundamentado en la preexistencia de un derecho mayor, común a toda la nación. En el caso de los derechos de autor no existe un derecho precedente, sino solo la persona con su capacidad creativa, sin embargo, la ley genera un derecho común una vez que los derechos particulares se extinguen. Así, en el caso de los derechos de autor, la propiedad comunal aparece constituida como un fenómeno *ex post* a diferencia de la propiedad privada donde lo comunal es un fenómeno *ex ante*. Este reconocimiento del momento en que el derecho público hace corte sobre lo privado, es fundamental para entender la diferencia entre la propiedad otorgada a perpetuidad (como en el caso de los inmuebles) y aquella limitada en el tiempo, como la de los derechos patrimoniales autorales. En la primera, la reducción de lo público para la aparición de lo privado que se constituye hacia un futuro indeterminado se traduce en certeza para la persona dueña, de ahí que la perpetuidad sea justificada; sin embargo, la limitación en el tiempo de los derechos patrimoniales, en el caso de las obras creativas, permite la irrupción de lo público, de ahí que su demora es en detrimento del bien colectivo.

Por eso, resulta ridículo defender las extensiones en los términos de vigencia del derecho de autor en los mismos términos en que se defiende la propiedad privada sobre bienes físicos.

Dicho sea claramente, el dominio público es de interés público en mayor medida que lo es el derecho de autor. Esta calidad de derecho colectivo del dominio público hace que su tutela sea obligación del estado, y que cualquier persona tenga capacidad de exigir su cumplimiento, ya de manera individual, ya como acción colectiva. (Abramovich y Courtis, 2014, p. 133)

Terminología jurídica aparte, es importante reconocer que el dominio público es fundamental para la cultura nacional y mundial. Cuando una obra sale del ámbito privado se suma al patrimonio cultural del mundo, permite reproducirse sin miedo a represalias jurídicas, se constituye como recurso educativo sin importar el nivel socieconómico de la persona que estudia, queda como testimonio histórico del pensamiento y vida de un momento específico en el tiempo, alimenta nuevas creaciones y se multiplica la capacidad de conservación gracias a que se puede copiar sin restricciones. En fin, a través del dominio público la creación se vuelve obra, recurso y archivo de manera simultánea.

¿Cómo es entonces que la legislación mexicana y su sistema de derechos autorales deje tan débil a este concepto tan importante? ¿Por qué el estado mexicano deja de lado aquello que suma democratización a su cultura para dar espaldarazo a derechos pasajeros, individualistas y comerciales? En cualquier sistema democrático el bien que beneficia a todos y todas debería ser prioridad frente al derecho de unas cuantas personas.

¿A qué debilidades me refiero en concreto? Enumeremos algunas:

### Los nefastos cien años

Definitivamente los términos de extinción de los derechos patrimoniales son a todas luces excesivos. México es el país con el término más largo en todo el mundo. Los derechos son vigentes por toda la vida de la persona creadora más cien años. Ninguna generación puede esperar ver liberadas las obras de sus contemporáneos. Este término es una mala cortesía de la inventiva del legislativo mexicano que ni siquiera se adapta a los términos sugeridos por tratados internacionales como el Convenio de Berna, el cual propone 50 años de vigencia para los países firmantes. Incluso el reciente Tratado entre México, Estados Unidos y Canadá de 2018 (TMEC) propone 75 años que, en caso de adoptarse, estandarizaría el término entre estos tres países.

Realmente nada justifica la adopción de un centenario como vigencia de los derechos patrimoniales salvo la pleitesía al corporativismo del derecho de autor, es decir, las únicas personas que vivirán para disfrutar la explotación de una obra son las jurídicas, esto es, sociedades mercantiles y de gestión. Solo ellas podrían tener garantizado el disfrute de las ganancias económicas por ser entidades jurídicas y no biológicas.

La defensa de que tales términos se otorgan en atención a las personas herederas de la persona autora también es un argumento vacío. En la práctica, los casos en donde los derechos permanecen dentro de la línea sucesoria de una persona autora más allá de dos generaciones sin venderse a una corporación o sociedad son más bien raros. Además, la base fáctica sobre la que otras legislaciones en el mundo han establecido sus términos es precisamente de dos generaciones humanas, (Anderson, 2007, p. 7) entendidas dentro de un promedio de 30 a 35 años cada una, traduciéndose así en un rango promedio de 70 o 75 años. Pero, ¿por qué resulta necesario que los tataranietos y tataranietas de una persona creadora sigan viviendo de una obra sobre la que no participaron ni intervinieron? Dicho de otra forma, la extensión centenaria de México, en lugar de promover la creación y la cultura, solo genera una economía parasitaria a costa de la cultura nacional. Y nada más aberrante en un país con altos niveles de pobreza y tan necesitada de cultura.

Al mismo tiempo, la duración de los derechos de autor en México genera que éste no se integre de lleno a la sociedad global del conocimiento, ya que las obras que entran al dominio público en otros países tardan varias décadas más para que en México puedan ser libres. Consecuentemente, no podemos reproducir investigaciones científicas, ejecutar partituras, realizar traducciones no autorizadas y usar fragmentos de películas cuyos autores ya pertenecen a todos y todas en sus países de origen. México se ha autoimpuesto una versión cultural del suplicio de Tántalo: sufre de constante hambre por educación y arte, pero no puede alcanzar el fruto delicioso del dominio público que se le ofrece tan a la mano.

Debe decirse, también, que esta dependencia de lo cultural hacia las industrias que lucran con bienes culturales, genera una selección artificial sobre lo que se heredará como cultura. Un ejemplo, claro se da en la industria editorial, la cual rige sus criterios editoriales de acuerdo al número de ventas y popularidad de la que goza un libro. Así, el libro que no genera ganancias no se edita, ni se reedita, no se vende y no se divulga; y, por otro lado, al poseer los derechos sobre el texto de manera exclusiva, acompañadas de prácticas bastante violentas de “defensa” de estos derechos, tampoco permiten que las personas copien o reproduzcan sin autorización. Las industrias terminan siendo como el perro del hortelano: ni comen, ni dejan comer. Esto genera que muchos libros y personas autoras tengan pocas oportunidades de incorporarse como parte de la cultura popular y corren el riesgo de caer en el olvido. Las ediciones escasas, descatalogadas o raras tienden a desaparecer salvo por los esfuerzos heroicos de bibliotecas, archivos y personas bibliófilas. Las generaciones futuras tienen más probabilidades de leer no las obras más significativas de nuestro tiempo, sino las más vendidas. Estamos dejando en manos de las industrias y su criterio comercial la memoria cultural de nuestro país.

Finalmente, los cien años solo abonan incertidumbre a cualquier acervo cultural. El fuego, los microorganismos, la descomposición química, las guerras, los devenires políticos, los cambios de ideología, las purgas, las fallas técnicas, la errónea catalogación, el descuido y, principalmente, el olvido son algunas de las amenazas que pesan sobre nuestros bienes culturales. La experiencia histórica da cuenta de que las obras que sobreviven tanto físicamente como en la memoria colectiva son escasas. Y esta rareza aumentará considerablemente en la medida que pese sobre ellas la prohibición de no copiar ni divulgar sin autorización expresa que nuestro derecho impone. Así, la copia, autorizada o no, se convierte en la mejor apuesta para que nuestras piezas de cultura lleguen a futuras generaciones. El tiempo se encargará de absolver a lo que hoy se sataniza bajo el nombre de piratería y recriminará la ambición desmedida del derecho de autor.

### Los términos interminables

Cien años es mucho y muy malo. Es poco probable que alguien de esta generación esté aquí para disfrutar de la cultura que entonces se liberará. Sin embargo, la incertidumbre es aún peor, ya que ni siquiera tenemos garantía que la ambición de la industria que lucra con la cultura, las sociedades de gestión u otros actores políticos no presionará por mayores extensiones en el futuro.

México y su legislación autoral ha retrasado en diversas ocasiones la entrada en vigor del dominio público, usando una estrategia que se ha empleado en otros países: una legislación establece un plazo tras la muerte de autor para que los derechos patrimoniales se extingan, pero antes de que ese término se cumpla se genera una nueva ley o una reforma a la misma ley que extiende el término unas décadas más. Un breve repaso a la historia de las extensiones al término de duración de los derechos de autor puede ilustrar esto:

* Ley Federal sobre el Derecho de Autor de 1948[^2] estableció por primera vez un plazo de protección tras la muerte de las personas autoras, que se fijó en 20 años.
* Una nueva ley, en 1956, agregó 5 años más al término anterior.
* En 1963, se reformó la ley del 56 para otorgar 30 años de derechos.
* Otra reforma en 1982 envía el inicio del dominio público hasta los 50 años tras el fallecimiento de la persona creadora.
* Una tercera reforma a la ley del 56 se realizó en 1994, para realizar adecuaciones por la entrada en vigencia del TLCAN. México adoptó entonces una protección de 75 años como establecía dicho tratado.
* Dos años después de la reforma anterior, en 1996, se genera la ley que continúa vigente, la cual de principio adoptó el mismo término de 75 años.
* Sin embargo, en 2003, se reformó el artículo correspondiente al término de duración de los derechos patrimoniales en los fatídicos 100 años.

En resumen, ninguna ley o reforma permitía el transcurso del tiempo que se establecía originalmente para la entrada del dominio público antes de generar una extensión de los derechos de autor, creando una especie de efecto de encadenamiento de los términos. O al menos, eso parecía.

Este encadenamiento, en realidad, es un fenómeno que en derecho llamamos retroactividad, es decir, efectos que se producen hacia el pasado, sin embargo, la constitución mexicana prohíbe la retroactividad de una ley cuando es perjudicial a una persona. Y si bien esta retroactividad parecería beneficiar a las personas autoras, termina por perjudicar al resto de la población porque se traduce en el retraso del dominio público y en el retardo del derecho fundamental y colectivo del acceso a la cultura. Nunca resulta más importante que en este momento recordar que el derecho de autor es un privilegio exclusivo y temporal que habita dentro del derecho general y atemporal del acceso a la cultura. Por tanto, me opongo fuertemente a la idea de que una extensión del derecho de autor pueda beneficiar a obras cuyos autores murieron antes de la ley o reforma que establece un nuevo término, porque el retraso de la entrada del dominio público genera una afectación colectiva sobre bienes culturales y de interés público. Así, la reforma de 2003, que impone los cien años, sólo debería entenderse como beneficiosa para las obras de autores muertos a partir del día de vigencia de dicha ley y nunca antes, contrario a lo que se viene considerando.

Si mi argumento anterior no convence, algo más puedo decir sobre ese efecto retroactivo que hemos llamado de encadenamiento.

Es mi parecer que para que este encadenamiento sea perfecto haría falta una disposición expresa que otorgue a las obras creadas durante la vigencia de una ley anterior, el beneficio del nuevo término extendido en una ley posterior. De ordinario, esta disposición se realiza en los artículos llamados transitorios, sin embargo eso no pasó en todos los casos de la cronología hecha más arriba.

| Ley (L) o reforma (R\) |Publicación en DOF[^3] | Entrada en vigor | Duración derechos patrimoniales | Artículo de retroactividad |
|  :------- | :------- | :------: | -----: | :------: |
| L |14/01/1948 | 30/01/1948  | 20 años | Cuarto transitorio |
| L | 31/12/1956  | 30/01/1957 | 25 años | Tercero transitorio |
| R | 21/12/1963 | 22/12/1963 | 30 años | Sexto transitorio (adicionado)|
| R | 11/01/1982 | 12/01/1982 | 50 años| No |
| R | 22/12/1993 | 01/01/1994 | 75 años| Cuarto transitorio |
| L | 24/12/1996 | 24/03/1997 | 75 años| No |
| R | 23/07/2003 | 24/07/2003 | 100 años| No |

**Tabla 1. Extensiones a la duración de derechos patrimoniales en el derecho autoral mexicano**

Dos reformas a la ley del 56 son importantes. La primera llevada a cabo en 1963, adicionaba un transitorio sexto a la ley, que permitía que obras en dominio público por falta de registro se beneficiaran del nuevo término de protección de 30 años si realizaban un registro en la entonces Dirección General del Derecho de Autor dentro del año siguiente. Destacan dos cosas: el beneficio no era general a toda obra sino que se ceñía a obras sin registro previo y, segundo, la condición necesaria era el mencionado registro. Es decir, solamente las obras que haya cumplido con ese requisito dentro de 1964 se benefician de la extensión, mientras que las restantes se encuentran en dominio público.

Ya que las reformas de 1963 entraron en vigor el 22 de diciembre de dicho año y si se considera mi argumentación correcta, todas las obras anteriores a esa fecha que no hayan tenido un registro en 1964 solo tenían como duración máxima de protección 25 años. La conclusión, entonces, sería contundente:

**Todas las obras producidas en México cuyos autores murieron hasta el 21 de diciembre de 1963, entraron al dominio público a más tardar el 22 de diciembre de 1987. Salvo que se demuestre su registro ante la Dirección General del Derecho de Autor.**

La reforma de 1982, que aumentó el término a 50 años, omitió cualquier mención de que esa extensión beneficiaba a las obras creadas o personas fallecidas previamente a su entrada en vigor. Como he dicho, estas extensiones afectan el interés público, por lo que no podrían sobreentenderse. Consecuentemente, la omisión en estas dos reformas generó un corte en esta cadena de términos, por lo que en realidad existen más obras en dominio público por haber entrado cuando la ley que las regía sin beneficiarse de leyes posteriores. Así:

**Las obras producidas en México cuyos autores murieron entre el 22 de diciembre de 1963 y el 11 de enero de 1982 (día anterior a la entrada en vigor de la reforma de ese año) entraron al dominio público a más tardar el 12 de enero de 2012.**

La ley de 1996 sí incluyó un transitorio de retroactividad, que beneficiaría solamente a quienes murieron en la vigencia de la reforma inmediata anterior, porque no habían entrado en dominio público, pero tras ella ninguna otra reforma ha mencionado que los términos nuevos benefician retroactivamente, de ahí que los términos de vigencia deben considerarse caso por caso dependiendo de la reforma o ley vigente al momento de muerte de la persona autora y no necesariamente la vigente.

Cualquier interpretación de los argumentos anteriores debería realizarse teniendo siempre en consideración el interés mayor que representa el dominio público. Pero a falta de mejores argumentos, hoy por hoy, considero que el dominio público es más extenso del que veníamos pensando. 

### ¿Y dónde está el dominio público?

Aún cuando el dominio público fuera bastante robusto en nuestro país ---que no lo es---, subyace el problema de su localización y su uso efectivo. Un derecho que no se emplea es un derecho inútil y vulnerado.

Cuando hablo de su localización me refiero a un término práctico: ¿dónde podemos consultar el catálogo de todas piezas culturales que se han convertido en patrimonio nacional y mundial? 

Esta pregunta podría convertirse en otra: ¿quién se encarga de recopilar, defender y establecer claridad el conjunto de lo que se considera dominio público?

Responder a esta última pregunta podría parecer obvia y se podría responder instintivamente que el INDAUTOR es la institución natural conforme a la ley. Sin embargo, ni en las atribuciones que le otorgan la LFDA y su reglamento, ni en la práctica, este organismo público se encarga de hacer el seguimiento de lo que entra al dominio público. Su papel se limita a consultas caso por caso que ni siquiera se sistematizan o alimentan un registro público de las obras ya liberadas. Ni siquiera en su estructura orgánica, el INDAUTOR dedica una oficina encargada de vigilar que este derecho colectivo esté al alcance de todas las personas. Vaya, que su papel es más bien de observador pasivo en lo que respecta al dominio público.

Como consecuencia, las obras que entran a dominio público caen en el vacío del desconocimiento, quedan a la suerte de que sea descubierto por personas interesadas y corren el riesgo de que personas o sociedades sin escrúpulos lo vuelvan a registrar reincorporando al ámbito privado lo que ya pertenece a todos y todas. 

Sin una defensa proactiva, el dominio público se queda como presa de la voracidad de la ambición de quienes abusan del sistema autoral para generar lucro desmedido y poco ético.

Sin un catálogo eficiente, público y, me atrevería a proponer, digital, las obras en dominio público terminan siendo objeto de arqueología y tema de curiosos, en lugar de tomar su papel importantísimo de bien comunal.

## Para cerrar

Como se ha visto, el derecho de autor (y de autora) posee más un carácter de status quo inmovilizante que un principio posibilitante para la cultura. Existen muchas preguntas, vacíos y oportunidades de mejora, no obstante, se renuncia a reimaginar el derecho de autor, porque con ello se reimanigarían conceptos como la propiedad de lo intangible y la economía de la cultura. Sin embargo, una sociedad del conocimiento, un mundo conectado, ciudades inteligentes y un desarrollo equitativo requieren que la cultura sea libre o al menos más libre de lo que es ahora. Por su parte, el derecho tiene la obligación ética de ser habilitante de las libertades que democraticen los saberes, que descolonicen la ciencia y que integren a todas, todos y todes.

## Bibliografía

Abramovich, V. & Courtis, C. (2002). Los derechos sociales como derechos exigibles. Madrid: Editorial Trotta.

Aliprandi, S. (2012). Creative Commons: guía de usuario: Un manual completo con una introducción teórica y sugerencias prácticas. Ledizioni. doi:10.4000/books.ledizioni.185 

Anderson, C.B. (2018). The History of U.S. Copyright Law and Disney’s Involvement in Copyright Term Extension. Disponible en: https://mountainscholar.org/bitstream/handle/20.500.11919/1453/STUW_HT_2018_Anderson_Clarissa.pdf?sequence=1

Arteaga Alvarado, C. (2008). Marco legal del derecho de autor en México. En López Cuenca, et al. Propiedad intelectual nuevas tecnologías y libre acceso a la cultura. México: Centro Cultural de España en México - Universidad de las Américas Puebla. pp. 235-256. Disponible en: http://www.innopro.es/pdfs/propiedadint.pdf

Garner, B., Black, H. & et. al. (2004). Black's law dictionary. St. Paul, MN: Thomson/West.

Hietanen, H.A. (2007). License or a Contract; Analyzing the Nature of Creative Commons Licenses. NIR, Nordic Intellectual Property Law Review. Disponible en: http://ssrn.com/abstract=1029366Electronic

Rojas Amandi, V.M. (2008). El perfeccionamiento del consentimiento en la contratación electrónica. En Revista de la Facultad de Derecho en México. Tomo LVIII. No. 249. México: UNAM

Rosnay, M. & Martin, J. (2012). The digital public domain: foundations for an open culture. Cambridge: Open Book Publishers.

Word Intellectual Property Organization. (2005). WIPO guide on the licensing of copyright and related rights. Ginebra: WIPO. Disponible en: https://www.wipo.int/edocs/pubdocs/en/copyright/897/wipo_pub_897.pdf

[^1]: Sobre las capas de las licencias Creative Commons, ver: https://creativecommons.org/licenses/?lang=es

[^2]: La historia del derecho de autor puede rastrearse aún más atrás. Sin embargo se toma como punto de partida la primera ley que establece una protección posterior a la vida de la persona autora.

[^3]: Diario Oficial de la Federación

# Fotografía libre y comunidad: Nunca más una historia será contada como si fuera la única

> [name=Eneas de Troya]

Creo que es seguro asumir que casi todo el mundo conoce o ha usado la frase: «una fotografía dice más que mil palabras». Es uno de los lugares comunes a los que se recurre cuando se quiere hablar de la capacidad expresiva que poseen las imágenes y que nace casi al mismo tiempo que la popularización de la fotografía a principios del siglo XX. Esta frase, además, cuenta con equivalentes históricos que se pueden rastrear hasta Leonardo da Vinci, cuando el maestro renacentista compara a la poesía con la pintura: «Si tú, poeta, tuvieras que representar una cruenta batalla, [...] tu pluma se acabaría, [...] tu lengua se secaría y tu cuerpo sería dominado por el sueño y el hambre antes de que pudieras describir con palabras lo que un pintor es capaz de mostrarte en un instante».[^4]

En mi opinión, creo que detrás de este cliché existe una reflexión mucho más estimulante que ocurre cuando se cambia la perspectiva desde el «cuanto dice» hacia las preguntas «qué dice, a quién se lo dice, por qué lo dice». Esto, porque la fotografía manifiesta, esencialmente, el punto de vista de la persona que la produce: es el fotógrafo quien decide el encuadre ---qué elementos incorpora a la fotografía y cuales deja fuera--- y la composición ---cómo es que esos mismos elementos dentro de la fotografía se comunican entre ellos y los significados o símbolos que se generan a partir de ese contacto---. Así, la fotografía no es un reflejo de la realidad, sino un instrumento expresivo a disposición de quien la produce: «son una interpretación del mundo tanto como las pinturas y los dibujos», según Susan Sontag (2008, p.15), quien a lo largo de su vida escribió varios ensayos dedicados a analizar los cambios en la imagen fotográfica. 

Quizás sea por mi formación académica en lengua y literatura, pero considero que la fotografía es, sobre todo, una narrativa: un punto de vista desde donde se nos describe un momento en el tiempo, ya sea este un hecho histórico, un personaje o un paisaje. Es un relato visual del lugar y el tiempo en que nos tocó vivir. «Como cada fotografía es un mero fragmento ---dice Sontag--- su peso moral y emocional depende de dónde se inserta. Una fotografía cambia según el contexto donde se ve» (2008, p.109).

Como siempre que se habla de formas de narrar el mundo que nos rodea, conviene recordar que existen los relatos hegemónicos ---aquellos que perpetúan la moral, valores y cosmovisión de la cultura dominante--- y los puntos de vista alternativos o excéntricos ---empleando esta última palabra en su sentido etimológico de «fuera del centro»--- que están constantemente en pugna. Los relatos hegemónicos, la fotografía que más se reproduce, son aquellos que se crean para y a través de los grandes medios de comunicación, agencias y bancos de imágenes. Son estos puntos de vista los que más llegan a conocerse y, por lo tanto, son los que adquieren mayor preponderancia al momento de informar, como hecho noticioso, y crear criterios, como objetos culturales.

Sólo por mencionar un ejemplo de cómo se forman estas narrativas hegemónicas, me remito a de uno de los concursos de fotografía más prestigiosos del mundo: el World Press Photo, administrado por una organización con base en Ámsterdam y que tiene como valores centrales la «precisión, diversidad y transparencia» para establecer «un enfoque más amplio que conecte al mundo con las historias que importan, y que es necesario para el mundo cambiante que se ha presentado en los últimos meses».[^5] Para ganar este invaluable premio se debe comprobar una pericia técnica excepcional, así como haber sido testigo de los hechos que más hayan marcado al mundo ese año. Por supuesto, salir victorioso significa, no sólo una importante cantidad de dinero como premio, sino una exposición masiva de los trabajos ganadores en una muestra fotográfica que se exhibe en decenas de países del mundo.

Sin embargo, recientemente la organización del concurso ha sido acusada de «racismo institucional» pues su junta directiva está compuesta exclusivamente de personas blancas. Por eso se le ha llamado en redes sociales: «White World Press Photo». El fotógrafo de la India, Chirag Wakaskar, en una entrevista con *The Art Newspaper*, abunda en esta crítica: «Las organizaciones de periodismo visual como World Press Photo, se niegan a aceptar su narrativa supremacista blanca y el sistema patriarcal en el que han prosperado» (Seymour, 2020). Otro fotógrafo, Nana Kofi, quien trabaja en diferentes países de África y, de hecho, fue juez del concurso en 2019, hace un llamado activo a que África «se apropie de sus propias historias» pues «a menudo, cuando occidente se refiere a “el mundo”, África no está en ese mapa» (*Nana Kofi Acquah*, 2020) (una idea con la que, sinceramente, también nos podemos identificar en América Latina).

Desde ese tipo de organizaciones, medios de comunicación e instituciones, es donde se irradian las perspectivas hegemónicas sobre lo que acontece en el mundo: que, aunque son de muy buena calidad técnica, siempre presentarán un punto de vista limitado y sesgado por los criterios de cada equipo editorial.

Aunque actualmente los avances tecnológicos ponen al alcance de cualquier persona la posibilidad de tomar fotografías ---y consecuentemente crea una infinidad de puntos de vista de los acontecimientos de nuestro mundo--- uno de los aspectos que quedan por resolver es cómo se distribuye este trabajo. Por supuesto que cualquier persona puede publicar su punto de vista en alguna red social, pero jamás tendrá el mismo peso que la fotografía que se reproduce millones de veces en todo tipo de soporte digital e impreso. 

Por eso mismo, considero que fomentar la distribución libre de fotografías incide directamente en que aumente la calidad y variedad de narrativas ---notablemente, de esas que están «fuera del centro»--- y, al mismo tiempo, se disminuye el peso de los discursos hegemónicos que pretenden limitar la expresividad y alcance de esas «más de mil palabras» que se cuentan con la fotografía.

## Hacia un modelo abierto 

Lograr esta distribución libre de fotografías no es una empresa fácil: nos enfrentamos a limitaciones tanto tecnológicas como culturales. Por un lado, los prejuicios y dudas dentro de la propia comunidad de fotografía son bastante importantes: «Si distribuyo mis fotografías libremente: ¿Cómo evito que plagien mi trabajo, que alguien más lo venda? ¿Cómo puedo ganar dinero si me pongo a regalar mi trabajo?». Y, por el otro, el modelo dominante se apoya en toda una estructura de poder que incluye un marco legal y un orden jurídico que los sustenta: las regulaciones sobre el derecho de autor, el derecho de copia y la propiedad intelectual, están hechas por los grupos de poder dominantes y se oponen  a la libre difusión de los productos culturales. Son el conjunto de prácticas discursivas, teóricas y de estrategias culturales que comienzan su expansión con el fenómeno del imperialismo en el siglo XIX. Para contrarrestar esas dificultades, quienes nos dedicamos a elaborar discursos visuales, tenemos que generar nuestras propias estrategias que nos permitan manipular las reglas establecidas con la finalidad de poder usarlas para contar nuestras propias experiencias. 

John Berger, un maravilloso novelista británico, escribió a mediados del siglo XX: «Nunca más se contará una sola historia como si fuera la única».[^6] Él se refería a la multiplicidad de voces que comenzaban a escucharse desde los países que alguna vez fueron colonias europeas: historias que se abrían paso para contar el punto de vista de aquellos pueblos que comenzaban a recuperar la voz. Nunca más habría una historia única, hegemónica, sino distintas formas de ver los acontecimientos importantes para una sociedad.

Esos nuevos narradores ---agrupados por mera conveniencia en la categoría de «poscoloniales»--- adoptaron el lenguaje que les impusieron los colonizadores, pero lo «doblegaron» para que fuera capaz de expresar las diferentes realidades de su cultura. Arundhati Roy, una de esas voces, lo pone en términos muy sencillos: «cuando cuento una historia, no lo hago como una ideóloga que quiere enfrentar una idea absolutista contra otra, sino como una narradora que quiere compartir su forma de ver».[^7]

De una forma similar, un modelo de distribución abierta como el que propone Creative Commons no pretende crear un nuevo marco legal para la distribución de fotografías: un nuevo modelo hegemónico que domine sobre otros. Más bien, adapta las reglas existentes para facilitar que los autores puedan otorgar los permisos de uso y distribución de sus obras. El objetivo central de Creative Commons es difundir la cultura y que cada quien tenga la libertad de elegir con quien comparte lo que hace, mientras que los derechos reservados buscan acotar esa posibilidad en pro del beneficio económico de los grupos de poder.

Este modelo abierto va en contrasentido a la tendencia hegemónica de hacer cada vez más restringido el flujo de los productos culturales y, por esa misma razón, ayuda a desmontar esas «ortodoxias» existentes que, muchas veces, no se pueden ni siquiera cuestionar. Lo más importante, sin embargo, es que al mismo tiempo que invita a la creación, instiga a la acción política y social. «Doblega» al modelo institucional y permite que florezcan otras historias al facilitar su reproducción y adaptación.

Como una reflexión al margen: quizás por eso es un poco difícil, aunque muy estimulante, defender las posturas de Creative Commons en foros que son estrictamente de abogados o del ámbito legal. Porque se utiliza el lenguaje que ellos mismos diseñaron para intentar explicar algo que rompe con muchos de sus paradigmas, instalados a través de decenas de años de «educación convencional» sobre los derechos de autor.

## Sembrar las semillas 

Wilson Harris, un escritor de Guyana, reflexiona sobre lo importante que es que se reproduzcan estas otras narrativas: «cada texto contiene las semillas de la comunidad que rompen en pedazos la aparentemente ineludible dialéctica de la historia».[^8] Elaborando un poco sobre este enunciado de Harris, lo que facilitan las licencias libres es que nuestro trabajo llegue a más personas, pero quizás más importante, se permite que otras personas puedan utilizar nuestro trabajo para remezclarlo y redistribuirlo. Un producto cultural compartido libremente se convierte en una herramienta para construir comunidad: una semilla que se planta para que todos puedan cosechar sus frutos.

En el caso específico de la fotografía, su distribución sin restricciones cuestiona la idea de que «la foto es de quien la hace» y declara que, más bien, es «de la comunidad que la adopta»: le pertenece tanto a quien la produce como a quien la mira, la interpreta, y la reutiliza. Así, como diría Harris, cada fotografía liberada lleva en sí la semilla de la comunidad: el potencial de convertirse en algo más, gracias al trabajo colaborativo.

Por su parte, la académica chilena, e investigadora de la fotografía latinoamericana, Ángeles Donoso dice que este potencial para gestar nuevas colectividades constituye un «contrato civil de la fotografía» pues «son los usuarios los que producen la ciudadanía de la fotografía y los que producen nuevos mundos, que no operan bajo la lógica del estado-nación y que producen otra forma de soberanía» (*La mirada como activismo*, 2020). La foto se convierte en una excusa para hacer otras cosas fuera del estricto territorio de lo visual: tomar posturas políticas, mantener viva la riqueza cultural de un pueblo y generar identidad. Ese es el potencial político de la fotografía que se produce cuando el trabajo visual circula libremente. Es una dimensión política tan importante como la dimensión cultural.

## Intertextualidad 

Más aún, trabajar con licencias libres facilita que la producción cultural mantenga una actividad muy dinámica. Una fotografía tiene el potencial de ponernos en contacto con toda una comunidad de productores visuales, a través de la intertextualidad ---el diálogo entre las obras fotográficas--- y con elementos que trascienden a esa comunidad: que evocan un pueblo, una época o una circunstancia de esa cultura. Esta comunicación no se limita sólo a la relación que la obra puede tener con otro autor, sino que también es la relación de esa obra con una cultura específica, con una forma de ver el mundo, misma que se comparte con otros autores. 

La escritora y activista feminista, Audre Lorde lo escribe magníficamente en uno de sus ensayos más famosos: «Sin comunidad no hay liberación, solo el armisticio temporal entre un individuo y su opresión. Pero la comunidad no debe significar un desprendimiento de nuestras diferencias, ni la patética simulación de que estas diferencias no existen. En un mundo de posibilidades para todos nosotros, nuestras visiones personales ayudan a sentar las bases para la acción política. El hecho de que no se reconozca la diferencia como una fortaleza crucial es un fracaso para ir más allá de la primera lección patriarcal. En nuestro mundo, dividir y conquistar debe convertirse en definir y empoderar».[^9] Así pues, la intertextualidad, agitada por el intercambio sin ataduras comerciales, provoca que las comunidades creativas crezcan y se alimenten unas de otras: no sólo para compartir soluciones visuales, sino para alimentar miradas alternativas al mundo.

Voy a ejemplificar estas ideas con el caso práctico de una de mis fotografías que se han difundido más ampliamente.

![Epidemia de pánico](https://lh5.googleusercontent.com/MPY9vGkVr5N-faHFMja4nl0qJM0oGcQKZ5x9NK3ZsDcv57zpJ5v-r4TAOFdabXO8xslTdWusB9uPxGiXHafA=w1920-h951)

Originalmente titulada «Epidemia de pánico», esta foto la tomé en 2009, durante la emergencia de gripe AH1-N1 en la Ciudad de México. Actualmente, ha vuelto a recibir atención debido a la pandemia de Covid-19 que azota al mundo. Fue realizada como parte de un ejercicio documental para una revista ahora extinta. Ellos comisionaron a varios fotógrafos para realizar el foto reportaje. Una vez que la publicaron en su revista, pude subirla a la red con una licencia CC BY 2.0 (así fue el acuerdo con los editores).

Casi inmediatamente, comenzó a ser retomada por diferentes medios de comunicación. Pero al mismo tiempo, también por personas que escribían en sus blogs personales: la foto ayudaba a la gente a dar su punto de vista. Como estaba disponible para ser descargada en resolución completa, también fue adaptada y remezclada en diferentes sitios de internet (e, incluso, publicaciones impresas). Algún tiempo después, esta fotografía fue considerada, incluso, para ser parte de la exposición «México a través de la fotografía», del Museo Nacional de Arte; así como en una exposición sobre la influenza en el *Center for Disease Control and Prevention* en Atlanta, Estados Unidos.

Como mencioné anteriormente, con la llegada del Covid-19, esta fotografía ha vuelto a ser ampliamente utilizada, incluso por agencias de la ONU, como UNICEF, universidades o ministerios de salud hispanoamericanos. Al haber estado disponible para su uso libre, tuve la oportunidad de ayudar a que la información sobre la epidemia de Covid-19 se distribuyera más fácilmente. Un granito de arroz que le da la razón a las palabras de la Doctora Lorraine Lesson: «este es el momento para la solidaridad, no la exclusividad».[^10]

A través de su distribución, uso y apropiación por parte de otros creativos, la fotografía «Epidemia de pánico» ha logrado también narrar una versión alternativa, crear comunidad y que se activen los procesos de identidad que van más allá de lo visual.

En resumen, facilitar que la gente tenga acceso a la fotografía ---a observarla, pero también a crearla--- es muy importante para aumentar la diversidad de voces que narran al mundo. En los bolsillos de casi todos existe una cámara fotográfica, una posibilidad diferente de interpretar nuestra existencia: hoy más que nunca tenemos acceso a decir esas «mil palabras», y tanto más. Sin embargo, queda pendiente facilitar el acceso a un modelo de distribución que nos permita compartir, de forma mucho más eficiente, nuestros puntos de vista con el mundo: comunicándonos, creando comunidad y generando nuevos productos culturales. Estoy convencido, sin embargo, que mientras más creadores usemos modelos libres para distribuir nuestro trabajo, más fácil será superar estos obstáculos.

## Bibliografía 

Janson, H. W., & Janson, A. F. (2003). History of Art: The Western Tradition. Prentice Hall College Div.

Sontag, S. (2008). Sobre la fotografía. DEBOLSILLO.

Seymour, T. (2020). World Press Photo—which has all-white supervisory board—accused of structural racism. The Art Newspaper. https://www.theartnewspaper.com/news/world-press-photo-accused-of-performative-gestures#:%7E:text=World%20Press%20Photo%2C%20the%20Amsterdam,light%20its%20exclusively%20white%20supervisory

Nana Kofi Acquah. (2020). Twitter. https://twitter.com/Africashowboy/status/1287321106865631233

The World Press Photo Foundation enters new phase. (2020). World Press Photo. https://www.worldpressphoto.org/media-center/press-releases/2020/wpp-new-phase/39908

Roy, A. (2002, 30 septiembre). Not Again.  https://ccrma.stanford.edu/%7Epeer/arundhatiRoy.html

Ashcroft, Bill, Gareth Griffiths, Helen Tiffin. 1989. The Empire Writes Back. Londres: Routledge, 1993
La mirada como activismo. (2020, 26 noviembre). [Vídeo]. YouTube. https://www.youtube.com/watch?v=frX7mwW_uHM&feature=youtu.be&ab_channel=CentrodelaImagen

Lorde, A. (1984). «The Master’s Tools Will Never Dismantle the Master’s House». En Sister Outsider: Essays and Speeches (p. 112). Crossing Press.

Prof Lorraine Leeson, Phd. (2020, 15 marzo). [Tweet]. Twitter. https://twitter.com/leesonl/status/1239209133221597185


[^4]: El profesor Horst Waldemar Janson, en su libro «Historia del arte», traduce al inglés uno de los manuscritos del Genio Universal en donde compara a la poesía con la pintura: «If you, poet, had to represent a murderous battle you would have to describe the air obscured and darkened by fumes from frightful and deadly engines mixed with thick clouds of dust polluting the atmosphere, and the panicky flight of wretches fearful of horrible death. In that case the painter will be your superior, because your pen will be worn out before you can fully describe what the painter can demonstrate forthwith by the aid of his science, and your tongue will be parched with thirst and your body overcome by sleep and hunger before you can describe with words what a painter is able to show you in an instant» (Janson y Janson, 2003, p. 631).

[^5]: In a statement, the organisation said its core values are «accuracy, diversity, and transparency» along with «a broader approach for connecting the world to the stories that matter, required for the changed world that has presented itself in recent months» (*The World Press Photo Foundation enters new phase*, 2020).

[^6]:  «Never again will a single story be told as though it's the only one».

[^7]: When I tell a story, I tell it not as an ideologue who wants to pit one absolutist ideology against another, but as a story-teller who wants to share her way of seeing (Roy, 2002)

[^8]: «Although post colonial texts may deal with divisions of race and culture which are apparently obdurately determined, each text contains the seeds of community which crack asunder the apparently inescapable dialect of history» (Cit. en Ashcroft 1989, 35)

[^9]: «*Without community there is no liberation, only the most vulnerable and temporary armistice between an individual and her oppression. But community must not mean a shedding of our differences, nor the pathetic pretense that these differences do not exist. In a world of possibility for us all, our personal visions help lay the groundwork for political action. The failure of academic feminists to recognize difference as a crucial strength is a failure to reach beyond the first patriarchal lesson. In our world, divide and conquer must become define and empower*». (Lorde, 2021, p. 112)

[^10]: «*This is the time for solidarity not exclusivity*». (Prof Lorraine Leeson, PhD, 2020).




# Algunas reflexiones sobre la creación, autoría y propiedad desde una experiencia mixe

> [name=Tajëëw Díaz Robles, Julio César Gallardo Vásquez]

## Resumen

En este escrito quisimos expresar las reflexiones sobre las nociones de creación, autoría y propiedad desde nuestra experiencia como parte de una comunidad mixe. Quienes escribimos el texto hemos tenido la oportunidad de presenciar y participar en momentos en los que entran en conflicto la perspectiva de lo individual contra lo colectivo, lo que nos ha llevado a plantearnos algunas preguntas que hemos intentado responder a lo largo del texto.

**Palabras clave**: ayuujk, comunidad, creación colectiva, propiedad colectiva, autoría

## 1. La creación y autoría colectiva en una comunidad mixe

La comunidad desde la que hacemos estas reflexiones es una comunidad mixe de la Sierra Norte de Oaxaca. Como en otras comunidades mixes, la lengua y la cultura determinan muchas de las acciones cotidianas y extraordinarias, y determinan también la respuesta frente a acciones externas que tengan efecto en la comunidad. Desde una perspectiva histórica, las ideas de lo colectivo y lo comunal no nos son ajenas, y muchos de los procesos ocurridos en la comunidad y que le han dado reconocimiento se fundamentan en lo comunal.

En estos tiempos, nuestra comunidad se encuentra en constante contacto con lo *akäts*, lo «no mixe», frente a lo cual responde de formas variadas que van desde el rechazo hasta la adopción sin cuestionamiento de ideas, prácticas y objetos venidos de fuera. Esto ha sido una constante desde hace ya varias décadas, pero ha aumentado partir de la segunda mitad del siglo XX. Anterior a esa época, nuestra región era considerada una zona periférica tanto para el virreinato novohispano como para el estado mexicano.

Quisiéramos iniciar estas reflexiones desde una pregunta general y meramente descriptiva: ¿Cómo ocurre la creación de ideas, obras u objetos dentro de una comunidad mixe? En principio, afirmamos que en nuestras comunidades el acto de crear existe, y que éste es reconocido como tal por los integrantes de la comunidad. Sin embargo, lo que posiblemente varíe de persona a persona es el proceso creativo, el cual podría estar sujeto a la situación en la que se manifiesta.

Es una idea común que para describir los procesos de creación y autoría dentro de una comunidad indígena se diga que la noción de creación y autoría individual no existen, que en una comunidad estos procesos siempre son colectivos. Pero, ¿es esto cierto? ¿no existe la creación individual en una comunidad indígena? ¿no se adjudican las ideas, obras u objetos a una sola persona que las crea? Creemos que la respuesta dependerá de qué es lo que se está creando.

Si, por ejemplo, nos referimos a ideas, se puede reconocer con facilidad quién dijo, propuso o manifestó una idea, a lo mejor en una asamblea o en una reunión, pero luego esa creación y reconocimiento individual se difumina a medida que la idea es tomada por el resto de la comunidad, transformada o adaptada según los requerimientos. Al final ya no podemos reconocer si la idea resultante puede atribuirse a una sola persona o si es idéntica a la idea inicial. Por otro lado, si nos referimos a obras artísticas o a objetos, en ciertas manifestaciones es fácil identificar a su autor, sobre todo en las obras que son contadas y de introducción reciente, como las piezas de música o la arquitectura; pero en otras obras no es tan fácil identificar quién la creó, como en los textiles o la alfarería, que se producen de forma masiva. De esta manera, cabe preguntarse si existen maneras para hallar el límite entre la autoría individual y la colectiva.

Otra forma de hacer un análisis sobre estas preguntas es explorando desde la lengua mixe. En nuestra lengua no existen palabras específicas para el autor o para el acto de crear, pues los matices están dados por lo que se crea y cómo se crea. Por ejemplo, cuando hablamos de ideas podemos preguntar: *¿Pën yë' jë wyïnmää'ny?* (¿De quién o quiénes es la idea?); las respuestas posibles se pueden expresar tanto en forma singular: *Ëjts yë' jë nwïnmää'ny* (La idea es mía), como en plural: *ËËts yë' jë nwïnmää'ny* (La idea es nuestra), y en primera, segunda o tercera persona. La forma de responder reconoce así que las ideas pueden ser planteadas tanto por individuos como por colectividades, y en este último caso, usualmente como resultado de un proceso de conversación y consenso. Las características gramaticales de la lengua mixe permiten que la pregunta sea ambigua, ya que no especifica si se refiere al creador en singular o en plural; eso se conoce hasta que se responde con un «yo» o con un «nosotros». Y por otro lado, la autoría es expresada mediante el recurso gramatical del posesivo, la marca de persona. No se afirma «yo soy el creador», sino «es mi/nuestra idea».

Cuando se habla de ideas, en la lengua mixe de Tlahuitoltepec no existe un término específico para designar su proceso de creación, pero cuando las creaciones son de tipo artístico sí hay una palabra para nombrar a este proceso creativo: el *kutääy'äjtïn*, palabra que designa a la creación artística de formas no convencionales. Una expresión común es *jate'n yë' kyutääy'aty*, que se refiere a que una persona está encontrando formas sorprendentes de resolver un problema o de manifestar sus pensamientos o sentimientos, es decir, que está siendo muy creativo. Se agrupa bajo el término de *kutääy*[^11] a los pintores, músicos, escultores, tejedores y otros artistas que crean algo excepcional, que provoque admiración entre las personas, sorpresa y también diversión.

Esta palabra no se usa exclusivamente para la creación de objetos artísticos sino que el *kutääy* también es la persona que encuentra soluciones no convencionales a un problema. Y aunque puede aplicarse a colectividades, usualmente la palabra *kutääy* se refiere a un individuo, a alguien que ha creado algo distinto a lo que ya existía, y por lo tanto a ese individuo se le puede reconocer con facilidad la autoría de sus obras. Pero también hay creaciones que no alcanzan el reconocimiento de haber sido hechas por un *kutääy*, aunque sí fueron hechas por individuos. Sucede por ejemplo, con las obras musicales, que si bien se identifica al autor, en vista deque dichas obras no llegan a ser excepcionales, el autor no logra el reconocimiento de la comunidad como un *kutääy*.

Sin embargo, reconocer la autoría de un individuo no quiere decir que se le reconozca su propiedad sobre la idea o sobre el objeto que creó. Esto sucede con actividades de tiempo atrás o en las que están involucradas muchas personas, entre ellas los textiles, la alfarería o la música. Debido a que la naturaleza de la creación en la comunidad implica el mostrarla al resto de las personas, cualquier otra persona de la comunidad que tenga las habilidades necesarias puede copiar y tomar elementos de lo creado para crear a su vez cosas nuevas. En mixe se utilizan términos como *pa'ejxp, nïkojtutp, nï'ejxtutp*, que se refieren a «copiar» o «reproducir» algún objeto, ya sea de forma casi idéntica o con modificaciones de quien lo copia. Sin embargo, el proceso de reproducción nunca es perfecto, no hay copias exactas de lo que alguien más ha creado y en ese mismo proceso hay espacio para nuevas creaciones inspiradas en las anteriores.

La posibilidad de copiar existe porque se asume que quien creó el primer objeto se inspiró de elementos o ideas locales, que su proceso creativo está definido por la comunidad en la que vive, que no hay ideas que surjan de la nada. Por lo tanto, ya que el proceso creativo tiene parte de su fundamento en las prácticas y vivencias de la comunidad, cualquier integrante de la comunidad puede tomar lo creado para hacer algo nuevo. Pero con la salvedad de que, como ya se ha reconocido la autoría a la primera persona, la segunda no podrá adjudicarse como el autor, cuando se trata de una copia. Esto sucede con la música, las obras escritas o las pinturas, es decir, obras que se producen en poca cantidad y en la que no participan muchas personas.

Por otro lado, existen actividades donde hay una producción mayor, continua y de la que participan muchas personas, como los textiles o la alfarería. En esos casos, si bien también se reconocen las autorías individuales, éstas se pierden entre la gran cantidad de obras que se crean dentro de la comunidad. Y cuando los cambios en las reproducciones son mayores, es ya difícil distinguir quién inició la idea (por ejemplo, determinada figura en un textil, cierta ornamentación en una olla), cómo se fue adaptando y todas las variantes que inspiró. Asimismo, hay patrones o técnicas que fueron creadas mucho tiempo atrás y de los que se ha olvidado quién o quiénes participaron en su creación.

Posiblemente estos últimos son el tipo de objetos que con toda propiedad pueden llamarse obras colectivas u obras de creación colectiva o comunitaria. Son el resultado de la acumulación o interacción de diferentes ideas y creaciones de varias personas de la comunidad. Las cuales, en una especie de viaje circular se inspiran en la vivencia de la comunidad, crean, y lo que crean se vuelve parte de la identidad de la comunidad dando origen a nuevas obras. Las obras de creación comunitaria no son idénticas entre sí, hay pequeñas diferencias identificables en cada una, algunas contienen más intervención individual que otras, mientras que en otras lo individual parece haberse perdido.

Al ser obras creadas, mantenidas y reproducidas dentro de la comunidad, que conforman parte de la identidad, del imaginario de un pueblo, entonces se asume que la propiedad le corresponde a esta comunidad o a un conjunto de comunidades. Por ejemplo, a nivel local podemos reconocer cuando hay un objeto u obra creado en otra comunidad, porque sus características son distintas a las de nuestra comunidad. Pero también podemos reconocer que hay objetos compartidos entre distintas comunidades, creaciones que no se han restringido a una sola comunidad y que se comparten con otras, y algunas de ellas, con el tiempo se convierten en objetos regionales, incluso aunque se reconozca que hubo una comunidad de origen o una comunidad donde se creó dicho objeto. Creemos que el hecho de que un objeto se use, se elabore y se comparta en diferentes comunidades, convierte a todas ellas en creadoras y propietarias de esas obras.

## 2. El choque entre lo local y lo externo: apropiación cultural de creaciones comunitarias

En la dinámica local se puede discutir si existen o no los conceptos de autoría y propiedad y cómo se relacionan entre sí, pero no hay que dejar de lado que la apertura de nuestra comunidad al mundo externo representó su inserción dentro de unas dinámicas comerciales y culturales distintas. Esta entrada o apertura al mundo «occidental» ha representado en ocasiones un choque o conflictos en la manera en la que se conciben las cosas, generando distintas respuestas que van desde la incorporación de prácticas e ideas externas, su adaptación o tolerancia, hasta el rechazo de las mismas.

En el año 2015 ocurrió un hecho que tuvo impacto sobre las nociones de creación, autoría y propiedad que hemos mencionado con anterioridad: dos diseñadoras francesas comercializaron la blusa que identifica a las mujeres de nuestra comunidad en un claro ejemplo de plagio y apropiación cultural y por la cual obtuvieron ganancias económicas. El caso fue llevado a los tribunales franceses pues una de ellas demandó a la otra ya que reclamaba la autoría del diseño de la blusa y afirmaba que la segunda la había plagiado a ella. La segunda manifestó en los tribunales que ella no había plagiado a la primera sino que se había «inspirado» en la blusa de nuestra comunidad, con lo cual la demanda de la primera ya no prosperó. Finalmente, ninguna de ellas reconoció que había cometido un plagio y solamente mediante anuncios virtuales y reclamaciones de las autoridades locales una de ellas manifestó que nunca se había apropiado de la blusa, mas no dejó de comercializarla.

Este ejemplo plantea una pregunta importante ¿qué sucede cuando un objeto o idea se convierte en objeto de apropiación por una persona que no pertenece a la comunidad y no tiene ningún lazo con ella? Lo que sucedió en ese entonces puede ser abordado desde distintas perspectivas: por un lado tenemos la respuesta inmediata ante este hecho por parte de la autoridad local que representa a la comunidad; por otro la participación del estado mexicano ya sea por su inacción, la legislación existente o su intervención en este asunto; también está la respuesta material de la comunidad y los efectos comerciales desencadenados y finalmente el impacto en el imaginario de la comunidad sobre las nociones de autoría y propiedad y la modificación de los esquemas existentes sobre lo que se concibe como creación.

Sobre este último punto, si bien este acontecimiento permitió que surgiera la idea de defensa de una prenda que se considera colectiva o comunitaria en tanto que tiene una larga historia de uso y de identificación con la comunidad, fue evidente que también resultó fortalecida la idea de autoría individual. No faltaron personas que al interior de la comunidad reclamaron o se adjudicaron total o parcialmente la existencia de la blusa o alguno de sus elementos gráficos. Aunque esto no ocasionó una disputa en la comunidad, sí dejó en claro que la intención de las diseñadoras francesas de adjudicarse la autoría individual de la blusa tuvo eco a nivel local. Por otro lado, también se introdujeron conceptos que hasta el momento no se conocían mucho, se habló de registros y marcas individuales y colectivas, se conoció que existía un Instituto Nacional del Derecho de Autor e incluso algunos artesanos indagaron los mecanismos para registrar sus obras individuales en dicho instituto.

Sin embargo, la idea de la creación colectiva o comunitaria también resultó fortalecida, y sobre todo el lazo entre las creaciones y la identidad. A pesar de que ahora se identifica de forma más clara quiénes crean las diferentes obras textiles, persiste la idea de que todas ellas continúan enmarcándose dentro de una identidad comunitaria, todas ellas se convierten en «estilos» dentro de una misma manifestación artística que es exclusiva de la comunidad, porque de ella toma los elementos y a la vez los refuerza. Y esto permitió que también se fortaleciera una postura «tradicionalista», que reivindica los objetos como se venían haciendo antes de este choque con lo externo. De esta manera, el acontecimiento iniciado en el 2015 hizo que las ideas sobre la creación o la autoría sí cambiaran hacia una vertiente más individualista, pero no lo suficiente como para abandonar la concepción de creación comunitaria.

## 3. Las opciones de lo colectivo y comunitario

En otro sentido, que algo sea externo no quiere decir que necesariamente implique un choque con la comunidad. Aunque sabemos que existe una dinámica extractiva de objetos y conocimientos en la que se prioriza lo individual sobre lo colectivo, en el mundo «de afuera» también existen mecanismos que apuestan por una perspectiva colectiva y de libre uso. Cabe entonces preguntarse ¿en qué medida coinciden estos mecanismos con las ideas locales de lo comunitario? En primer lugar, coinciden en que las creaciones son libres, que si bien se le debe dar reconocimiento a la persona que las crea, no implica que su creación no pueda utilizarse libremente, sin necesidad de pagar por ellas.

En el [Colmix](https://colmix.org),[^12] el colectivo del que formamos parte, algunas de las actividades que realizamos están enfocadas en la elaboración y difusión de material de lectura para la enseñanza de la lecto-escritura de la lengua mixe. Sin embargo, crear nuevas historias y concebir libros desde cero es una labor que requiere tiempo y demás recursos, por lo que una de las estrategias que  ha facilitado este camino es la traducción de libros ya escritos en otras lenguas, y sobre todo, libros ilustrados. En la búsqueda de materiales con estas características conocimos la plataforma [Story Weaver](https://storyweaver.org.in/), impulsada por la editorial Pratham Books, de la India, en alianza con otras editoriales y plataformas similares.

La plataforma Story Weaver pone a disposición del público cientos de libros digitales para su lectura, además de la opción de utilizar un repositorio de imágenes para crear tus propias historias así como subir tus propias ilustraciones y textos. La herramienta que más hemos utilizado hasta el momento es la plataforma de traducción, que permite seleccionar cualquier libro de su catálogo y traducirlo a cualquier lengua, reproduciendo las imágenes y el esquema del libro. De esta manera, el libro se crea de manera sencilla y rápida, lo cual permite ver el trabajo terminado, listo para ser leído en línea y con la posibilidad de descargar un archivo pdf para imprimir y compartir. Con esta plataforma, el proceso de creación y traducción de textos a la lengua mixe avanzó enormemente. A esta labor se han unido más personas y colectivos de otras comunidades mixes y actualmente se pueden encontrar [más de 100 libros](https://storyweaver.org.in/stories?language=Mixe&sort=Ratings) traducidos al mixe en la plataforma. Si bien es cierto que también representó otros retos que aún seguimos tratando de superar, como la formación en traducción o la sistematicidad en la escritura, la mera existencia de esta plataforma representó un gran avance en el camino para tener material escrito en nuestra lengua.

La apuesta de Pratham Books a través de Story Weaver tiene sus raíces en la filosofía de las licencias abiertas como una forma de proporcionar acceso inclusivo a contenido de este tipo para toda la niñez. La plataforma usa la licencia Creative Commons 4.0, lo que nos da la posibilidad de usar, reutilizar, compartir e incluso construir sobre el trabajo creativo que está disponible, además de que todas sus herramientas son gratuitas. Esta apertura coincide con nuestro propio planteamiento como colectivo, pues pensamos que los contenidos que creamos deben ser accesibles a toda la población hablante o interesada en la lengua mixe, y que los materiales didácticos se puedan utilizar en cualquier espacio sin necesidad de nuestra autorización. 

Sin embargo, el desconocimiento sobre las licencias libres y sus implicaciones ha creado cierta desconfianza en colectivos y personas a las que hemos invitado a hacer uso de la plataforma. Al ver una plataforma de esta naturaleza se piensa inmediatamente que se persigue el lucro, que si se contribuye con una traducción, con ilustraciones o con una historia, la «autoría», el trabajo personal-individual se verán cooptados por un ente desconocido que usará en su beneficio el trabajo que se ha hecho. 

La perspectiva individual es la que ha imperado en el contacto con el exterior, de ahí que no se conozcan mucho acerca de otras formas de compartir información,  la idea común es que «afuera» todos piensan de forma individual, toda creación se registra, todo es pagado y en todo hay una etiqueta de «se prohíbe su reproducción».

En esta interacción con lo externo, también hay debates sobre qué conocimientos deberíamos o no compartir hacia afuera y en qué espacios. Si bien algunos pueblos indígenas del mundo tienen o han generado mecanismos más claros para este tipo de situaciones, para otros ya no es tan sencillo restringir el uso de ciertas plataformas digitales como las redes sociales, donde se puede difundir información que podría considerarse delicada. 

Aunado a lo anterior está la cuestión de que desde una visión en extremo purista, toda idea o planteamiento que no se genere en la comunidad sigue siendo algo externo, por más que tenga muchas coincidencias con un pensamiento colectivo local; esto lleva a preguntarnos si desde la comunidad no se podría generar un sistema de pensamiento o de ideología sobre el libre uso que no haya sido originado desde fuera, nos lleva a reflexionar sobre si estas cuestiones siempre deberán ser planteadas desde el exterior y no existirá la forma de que sea algo generado desde las comunidades.

En esta interacción con lo externo existen también debates sobre qué conocimientos deberíamos o no compartir hacia afuera y en qué espacios. Si bien algunos pueblos indígenas del mundo tienen o han generado mecanismos más claros para este tipo de situaciones, para otros ya no es tan sencillo restringir el uso de ciertas plataformas digitales como las redes sociales, donde se puede difundir información que podría considerarse delicada. El uso de las lenguas indígenas en las plataformas digitales se comienza a problematizar, pues es conocido que las varias redes sociales, sobre todo las más populares, lucran con la información del usuario y si bien cuando usamos nuestra lengua en ellas la posibilidad de descifrar el contenido de lo que compartimos podría disminuir, tampoco es una garantía, considerando que el uso de las lenguas indígenas respecto del español sigue siendo mínimo. Por otro lado, existen iniciativas de localización de aplicaciones como facebook a lenguas indígenas, localización del navegador Firefox,  y otros esfuerzos que se encaminan a desarrollar aplicaciones para celular o para páginas web, o a crear Wikipedias. Al tiempo que estos esfuerzos continúan, deberemos seguir reflexionando cuáles son las implicaciones para nuestras lenguas y comunidades.

La impresión general sobre qué tipo de pensamiento externo se va incorporando a las prácticas locales, es que la autoría individual y lo privativo son predominantes. A pesar de esto, habrá que continuar los diálogos en torno a las posibles coincidencias que pueden haber entre la visión colectiva y comunitaria local y los procesos y organizaciones que promueven el software libre y las licencias libres. Los ejemplos aquí mostrados así como algunos procesos de  comunicación comunitaria nos hablan de que esta posibilidad es real, que se puede fortalecer la idea de creación colectiva y que se pueden utilizar herramientas de libre uso para fortalecer procesos de creación colectiva de los pueblos indígenas.

[^11]: La palabra kutääy tiene este significado en la variante mixe de Tlahuitoltepec, en las variantes lingüísticas de otras comunidades puede tener  otro significado o se usa otro término para nombrar a dicha persona.

[^12]: Ponte en contacto con el Colmix por [correo electrónico](mailto:colmixe@gmail.com).

# Las formas en que la colectividad inunda y rebasa

> [name=Mónica Nepote]

Este texto busca, a su modo, trazar una genealogía entre la noción autoral fincada en estructuras patriarcales. Propone a través de la imagen del hijx no reconocidx la creación de discuros alternos a la forma de ser nombradx y por lo tanto de construir espacios para crear, distribuir y compartir. Busca enlazar genealogías diversas y otras formas entendimiento del mundo como un flujo continuo de intercambios cuya lógica no permite la exclusividad.

## En el nombre del padre

Quiero empezar este texto hablando de amor en tiempos convulsos. El amor es necesario, me pregunto o me recuerdo, porque «el amor todo lo puede» dice una máxima. Quien esté libre de ilusión que venga y diga que no es verdad que en algún momento de su vida abrazó el deseo de formar parte de algo único e indivisible. Muchxs creemos, o quisimos creer en la veracidad de esta frase.

Quiero hablar de amor porque justo en esta palabra se construye una metáfora como un metal precioso, semejante a pequeñas vetas de pureza cuyo brillo nos enceguece, nos deslumbra y nos hace creer en ese ardor como único relato veraz.

Sin embargo habremos de ir con cuidado, porque los terrenos son pantanosos y las ideas y supuestos son precisamente eso, pensamiento colectivo que repetimos sin reparo. Nos abrazamos a ciertas ideas de mundo desde que somos niñxs, dando nuestros primeros pasos en los terrenos de lo dado por hecho, de lo incuestionable e inamovible.  La cultura determina, desde el desarrollo en el vientre de nuestra madre, el color de nuestro vestuario de acuerdo a nuestro género. Una vez nacidxs también determina otras cosas: desde asuntos «banales» como el uso de accesorios o los pronombres que nos definen; así también decide el uso de la primera persona, de nuestros plurales, y toda una serie de constructos ideológicos alrededor de lo que somos, o lo que creemos que somos y, por extensión, de lo que producimos, de lo que dejamos como huella de nosotrxs mismx..

Pero vuelvo al amor en este panorama general. El camino del amor está pavimentado a cuatro manos, porque el amor es, sobre todo, el mito del dúo; esas dos personas que se encuentran, dos personas que por lo general son un hombre y una mujer en edad reproductiva. Todas las historias tienen fisuras, pero la que traigo a cuento para encaminar las ideas que tejen este texto surge precisamente de la idea del amor entre un hombre y una mujer que deciden procrear y extender su legado a otra generación. La herencia al mundo se ha cumplido.

Esta pareja hombre-mujer, es la semilla que analiza con zoom agudo Brigitte Vasallo en el libro *Pensamiento monógamo terror poliamoroso, *una herramienta de disección, un mecanismo de desmontaje de varias estructuras que permean nuestra vida emocional. La pareja monógama posee un carga simbólica tal, que deja apenas algunos espacios para imaginar otras posibilidades, (aunque por fortuna los hechos nos dicen lo contrario). A este símbolo, el de dos, es al que se le atribuye también la continuidad, el linaje, la perdurabilidad en el mundo como especie. Alrededor de la pareja monógama hay mitos y miedos, poderes y presiones.

El sistema monógamo no organiza una forma de supervivencia colectiva, sino que quiere que nos reproduzcamos de manera identitaria y excluyente, con nombres y apellidos, con linaje, con marcas de nacimiento. Es reproducir nuestra casta y ponerle nuestra marca, el copyright, la denominación de origen, el código de barras, para saber exactamente quién pertenece a dónde, y qué pertenece a quién. (Vasallo, 37)

El lente irónico de Vasallo me parece una herramienta precisa para desmenuzar varios mitos por distintas razones, primero porque me ayuda a situar el problema de autoría y el del reconocimiento implícito aunado a estructuras profundamente arraigadas en nuestros imaginarios culturales. Donde el autor(padre) es más importante que el/la hijx. Segundo porque la metáfora de copyrigth tejida con la idea de descendencia me parece un terreno fértil para poder encontrar más fisuras y procesos de desmontaje. Vasallo ubica también otro problema no menor: ese copyrigth pertenece desde luego a la figura del padre, el gran protagonista. La madre quien da a luz, significa apenas, a los ojos del padre y de los muchos padres de familia, un ser *envase*, un medio biológico por el que la obra ha llegado a la tierra, ella misma ha perdido nombre, es la *señora de*... y es el producto de ese *amor de pareja* quien llevará el signo del reconocimiento del padre. Todo funciona ahí más o menos como quiere la cultura pero: ¿qué pasa en los casos de excepción, cuando la/el descendientx no tiene un nombre del padre? Porque padre si tuvo, es decir, macho que lo engendre sí pero no su reconocimiento. Es justo en esa bastardía y esa fisura se generan otras narrativas y aunque son problemáticas, son también potencia.

## Del dicho al hecho

Dejaré un momento la bastardía para retomarla como estrategia más adelante. Doy un paso atrás para volver a los dichos. Retomar uno muy célebre. Se trata de una idea o narrativa muy querida para los productores de textos. Una frase-instrucción que tiene un autor: José Martí, quien entendió que las promesas de la felicidad podían circunscribirse a tres acciones:

> Tener un hijo, sembrar un árbol, escribir un libro.

La palabra de Martí ha sido repetida por generaciones, como una especie del legado del bienpensar que resume en sí los atributos del hombre moral. Si bien la frase no alude directamente a aspiraciones capitalistas, se esconde tras de su fraseo una potente aspiración al poder y la propiedad en el amplio sentido simbólico. Qué más que un hijo, qué más que aspirar a ser *El Autor* y qué más que poseer una tierra donde sembrar, altas y nobles aspiraciones. Pero si hacemos un zoom no podemos sino esbozar una risita nerviosa ante lo ausente pero implícito: una mujer a quien fecundar, una tierra para poseer y la palabra para ser escuchada y venerada para ser honrada y admirada. Transmitir sabiduría, continuar la especie, materializar los bienes, otra lectura. El trabajo de hackeo es arduo, si pensamos que todo esto significan de alguna manera las tensiones que traen a cuento la idea de autoría. Autoría y patriarca parecen estar tan imbricadas que es casi imposible distinguir donde está el cabo del hijo/hilo de cada una. Tan imbricadas que a veces es difícil distinguir entre la base del humanismo y la prolongación de las ideas hegemónicas que, como todo sistema de construcción, anula otros posibles discursos no suscritos. Tal vez sea un buen momento para preguntarnos junto a la filósofa Nadia Cortés[^13], ¿Dónde están las hijas?, que significa por extensión preguntarnos tanto por las voces de las mujeres que no son El autor, como por esas narrativas que alteran el discurso hegemónico. ¿Qué posibilidades existen, han existido no sometidas a esas paternidades autorales?

## Palabras que autonombran

En el ensayo dedicado a Flora Tristan, ( precursora del feminismo en el siglo XIX, mujer mestiza, huérfana, no reconocida por la familia del padre por circunstancias accidentales), la escritora peruana Patricia de Souza se adentra en las muchas raíces del problema de Flora. Su condición como mujer del siglo XIX, mujer sin padre, sin herencia, la vuelve casi invisible, entendiendo invisibilidad como una existencia raquítica, pocas posibilidades de sustento y autonomía. El mundo no era para las mujeres, menos para las mujeres pobres, sin padre o marido. Huérfana desde muy niña en una Europa que no reconoció el matrimonio de sus padres, (un criollo peruano con una mujer francesa), la existencia de Flora entró en una especie de vacío. La única posibilidad de su estar en el mundo, como mujer, era bajo el cobijo de un hombre por lo que se casó muy joven con un maltratador. Flora era artesana, fungía como colorista en un taller de impresos, pero un buen día, harta de los malos tratos de un marido, encargó a sus dos hijxs y decididamente se fue a Perú en busca de su herencia. Paria era el término que la describía y el lugar que tenía reservado Flora en la sociedad, y paria fue la palabra con la que ella se autonombró en su libro *Memorias de una paria* y fue desde ese ejercicio de autonombrarse y como acto de apropiación del término (que no sumisón) para descibirse a sí misma bajo sus propios términos y condiciones.

Las hijas sin linaje, se autonombran y este gesto, potenciar el nombre, fue un acto posible para Flora, como un camino único que desde su aceptación de no contar con ese copyright del padre del que habla 
Vasallo, como detecta un nombre marginal que, precisamente, cumple con lo que parecía que no sería posible: la vuelve visible.

De manera similar a la de Flora Tristan, prácticas al margen del copyrigh patriarcal, *otras* prácticas se erigen como discursos que confabulan esas otras narrativas de la existencia que, en sí mismas, también activan movimientos y flujos alternativos a los canales de distribución. Toda palabra tiene una materialidad al textualizarse y esa materialidad inaugura sus propios canales de distribución y las formas de ser compartidas y reproducidas. Importa el mensaje que se quiere transmitir, importa el cómo, importa a quiénes. Desde esas voces, hijas bastardas, hijas parias, se activan con la misma potencia sus propios canales para llegar a sus lectorxs, comentadorxs.

Estos gestos nos dicen eso otro que importa. Importa desde qué sellos se imprime, importa desde qué programa se diagrama, importa desde qué infraestructura de red se piensa y comparte, importa cómo se hace circular tanto cómo desde dónde se emite porque esa otra construcción del flujo, es otra escritura, la escritura de su política de acceso.

## Otros momentos autorales

Sin duda el origen de todas mis dudas se deriva de una resistencia a suscribir la veneración por la figura autoral. Me tocó saltar al ruedo de la vida profesional en los años noventa, cuando México, en materia de cultura, era un grupo dividido en dos ventanillas hegemónicas en el contexto del entusiasta mercado neoliberal: la banda Nexos, la banda Vuelta. Entre los escritorxs jóvenes pesaba demasiado la figura de Octavio Paz. Se repetían anécdotas casi patéticas de cómo algunxs autores habían sido desdeñadxs por el patriarca y se ensalzaba a aquellxs a quienes el sacrosanto había ungido con su interés. Todavía hace no mucho me tocó ver a medias un documental que veneraba la sombra ausente de un ungido, un cierto poeta rebelde muy rebelde pero adorado por Paz. Esa era la narrativa dominante: el poeta hombre, joven y alcohólico que en su violencia de cantina era adorado por el santo patrono.

Estos cultos me resultan tóxicos, me perturba la insistencia de su continuidad. No encuentro ningún sentido en suscribir a una sola voz por arriba de una pluralidad de voces, tampoco a dedicar espacio mental colectivo a perpetrar estas costumbres. Tener hijos, escribir libros, sembrar árboles.

Repetir los mismos eslabones de la cadena patriarcal hasta el infinito, ¿cómo evitarlos?

La creación de un texto no es un acto puro. Todo texto deriva de otro, todo texto tiene sus propios maestros o se debe a otros más que pueden ser no solo textualidad. Los textos se deben a otras voces, pensares, naturalezas, ramas, caminos, imágenes, milpas, surcos, gestos, dibujos, escenas escudriñadas, restos de películas, palabras escuchadas en el pasillo de las escuelas, en la boca del metro, en las centrales de autobuses, ni hablar de foros en línea, memes, decires y leyendas urbanas del internet y muchas derivas que quizá no puedo imaginar en este listado.

Se busca y se rinde culto a la idea de un *original* afanosamente como si fuera posible, pero en este presente de reproductibilidad técnica, de copias perfectas (cfr. *Dafen dientes falsos* una serie de reescrituras de Pierre Herrera sobre idea de autoría, original y copia, reproducción y distribución en el siglo XX) sabemos también lo que dice Robert Cooper: «no hay nada nuevo bajo el sol excepto su expresión» y añadiría: su representación. En el espacio de la representación se abre entonces un espectro para acudir a la construcción colectiva de una justicia del lenguaje: que *autoriza*, es decir trae a primer plano autorías no vistas. Hablemos de la representación de las otras voces no oficiales, no patriarcales o no reconocidas (como es el caso de Flora). Este acto de escucha y lectura nos enseña a atender a las muchas voces y cuerpos de excepción, a todas esas alejadas de la ley del padre y el reconocimiento, a no olvidar que aquellas voces parias, no han sido aptamente o completamente representadas.

Quien ahora puede hablar no es la voz cantante expresada en cada siglo, quien ahora puede hablar fue, quizá, alguien que años atrás se le arrancó la lengua. Un(a) (no) sujeta(o) con derecho de expresar su mundo, o el dolor de su cuerpo, o pedir un trato justo. Quien ahora habla puede ser una voz inédita, no escuchada antes no porque no existía en su *originalidad*, sino porque centenares de voces y de discursos fueron acallados

¿Cómo y en qué términos, entonces, acercarnos hacia una circulación de ideas que desplacen los lugares comunes de la representación y la circulación?¿De qué forma acudir a esa antipareja feliz de la reproducción? ¿Cómo dar voz y espacio en este mundo del cuerpo corporativo/vizado a esos otros compañarxs (quizá más de algunx no es ni siquiera humanx)?

## Manifiestos de otra índole

Cuidar es una palabra que está ahora en el horizonte. Se organizan espacios de cuidado, mesas de cuidado, debates de cuidado, se tuitea sobre el cuidado, se entregan páginas y páginas sobre el cuidado. Pero a veces, tengo la impresión, no se hila necesariamente con otras prácticas sino que por el contrario se continuan reproduciendo las viejas formas de entender la autoría, la exclusividad y la circulación pensadas como mercancía apta para quien pueda pagar por el acceso.

### ¿Sabremos cuidar pensando el cuidado de otras formas?

¿Qué cuidamos cuándo cuidamos la cultura? ¿El legado, el prestigio, la perdurabilidad o incluso la fidelidad? Nos abrazamos a una idea fantasmagórica como si la huella de un pie muy grande tuviera que ser llenada, pero quizá el *quid* es detenernos a pensar si esa huella no ha pisoteado otra huella debajo, una más pequeña, menos ancha, menos abarcadora: la de otras autorías, la de otras formas y otras voces, esa voz a la que otras voces con más visibilidad median para hacerlas oír.

Habremos de cuidar que la voz extractivista no nos posea del todo, porque la voz del padre siempre clama tierras, quiere capital y quiere turismo, ganancias a toda cosa. No dudará en extraer, en intervenir, modificar la tierra para construir y legar, ya no se siembra el árbol, se tala y se construye; se tiene un hijo, se le desconoce, se le abandona o se le precariza... 

¿Cómo salir de todo esto? ¿Qué rutas o qué fisuras que permitan otros relatos, aquellos que permitan otras potencias? Encuentro un faro en un documento recientemente alojado en internet. [*El Manfiesto de la paloma migratoria*](https://ppmanifesto.hcommons.org/manifiesto-de-la-paloma-migratoria/): 

> Un llamado a las galerías, bibliotecas, archivos y museos públicos y a sus patrocinadores para liberar nuestro patrimonio cultural ya digitalizado. 

El sitio web que lo aloja no dice más, ni de su autoría ni de quién puede estar detrás de su promoción. Es un texto disponible en distintas lenguas: en el inglés original, español, francés e italiano. Es un texto que trae a cuenta debates a la luz de la actualidad, pues cruza líneas temáticas: autoría, justicia reparativa ante la extinción, archivos y acceso a la cultura. Todas, temáticas que hacen sentido con el momento presente. El manifiesto comienza con la historia de exterminio de una especie nativa americana: la paloma migratoria. Todavía visible en los cielos del siglo XIX; la historia de extinción de la especie está tomada como cita desde la voz de un nativo americano: Simon Pokagon (de la nación Potawatomi). Su testimonio doloroso habla de una doble historia de dominación y erradicación, de los suyxs humanxs y de esas especies con quien compartió la tragedia de ser víctima de mecanismos de extinción. Las historias de dominación suelen traer una enorme estela de erradicación de vida que ahora vivimos como consencuencias irreparables. El manifiesto urge a pensar en todas esas especies extintas y en las posibles formas de mirar la historia sin olvido, como un dispositivo que active la memoria y la acción reflexiva y quizá, restaurativa. Habla abrir archivos, esos que contienen las imágenes que dan cuenta y son documento testimonial de todos esos fantasmas que ya no habitan esta tierra pero cuya aura fotográfica perdura, este manifiesto subraya y busca ser un generador de mecanismos de reconocimiento y asunción de estas ausencias.

Pero cómo podemos hablar de justicia restaurativa si no es a partir de contar con el acceso a los archivos. Y cómo hablar de poner al centro todas esas voces acalladas, tengan palabras o no, si no es acentuando la colectividad.

Encuentro en este manifiesto una zona precisa en la que me gustaría hacer hincapié: 

Importa hablar de archivos, de quién detenta su existencia y de cómo estos conjuntos de documentos son un bien común y la forma en qué tendrían que ser accesibles para detonar reflexiones conjuntas a partir de su discusión en espacios educativos, especialmente.

Importa hablar de justicia reparativa, pensar colectivamente en torno a lo que ha pasado para que deje de suceder, sabemos este que este puede ser uno de los fines de todo archivo pero ¿cómo activar entonces esa insistencia que reflexionaen torno a las malas prácticas? La extinción de especies animales está alcanzando un grado escandaloso de alarma, nuestra vida común está bajo serios peligros reales.

Importa hablar de autorías colectivas y quizá de desplazar las autorías basadas en una sola persona para traer al centro lo que importa: esas vidas que fueron. O dejar de ver con ese singular fetichismo el nombre del autor para detectar de qué manera, en esa voz humana pueden hablar comunidades: otros humanxs menos privilegiadxs, el espacio mismo, lo que desea la colectividad o lo que otrxos no humanxs no pudieron decir.

En muchos sentidos, profundos y dolorosos el caso del *Manifiesto de la paloma migratoria* apunta derroteros de la escritura por venir, de la compartición por venir y de la reflexión colectiva.

Donna Haraway en *Seguir con el problema* nos inspira a adquirir otro tipo de parentescos, más allá de familias nucleares y legítimas. Convoca a crear relaciones con otros seres como estrategia de sobrevivencia en un planeta dañado. De nuestras cartografías cercanas, tomo prestadas las palabras del consejo de ancianos de la tribu yoreme de Cohuirimpo, que sitúa el ejercicio humano de pensar enmarañado con el entorno. Así la autoría se esboza como una colectividad real de voces:

> En qué se parece tu pensamiento a una barranca.\
En que se parece a las piedras.\
En qué se parece a una montaña.\
En qué se parece a los manantiales.\
En qué se parece al vapor que se levanta.\
En qué se parece a las nubes pasajeras.\
En qué se parece tu pensamiento a la lluvia fina.\
En qué se parece a un río caudaloso.\
En qué se parece al horizonte.\
En qué se parece tu pensamiento al bosque.\
En qué se parece a un árbol.\
En que se parece a la vegetación.\
En qué se parece a una flor.\
En qué se parece a la tierra reverdecida.

Pensar el pensamiento similar a los entornos, en sintonía con la tierra o los árboles, con el bosque, la montaña, el desierto o el mar, contribuye a la compresión de la autoría como un suceso colectivo, y de verdad descentralizado no solo de la idea de un autor, sino de lo humano. Para quienes están de cara a la defensa de los territorios, quienes piensan el mundo con códigos no occidentales los saberes del cotidiano son tanto humanos como no humanos: «el bosque piensa pero piensa a través de nosotrxs», sugiere el pensamiento amazónico reflejado en el libro *Como piensan los bosques* firmado por Eduardo Kohn pero pensando con la selva y las personas que la habitan. Los documentos que creamos hablan de historias colectivas, de otras genealogías y linajes;  los archivos que las resguardan son un logro colectivo, tenemos una voz que emite y firma pero no es la única que nos compone o nos define. Debería ser esta autoría colectiva una forma más gozosa y liberadora pensada desde el compartir continuo, desde la escritura y la reescritura continua y fluyente, intercambiable y al mismo tiempo, memorable.

[^13]: http://revistas.filos.unam.mx/index.php/theoria/article/view/1128

# El triste final para una muy buena película

> [name=Walter Forsberg, Elena Pardo, Tzutzumatzin Soto]

En la parábola corta de Franz Kafka, *Ante la Ley* (1915), un campesino busca audiencia con la Ley. Impedido de entrar por una puerta hacia la Ley, el hombre le suplica al guardián que lo deje pasar. Es posible, «pero no ahora», responde el portero. El campesino intenta mirar más allá del guardián, pero mientras lo intenta, el guardián se ríe a carcajadas: «Si tanto te atrae, intenta entrar a pesar de mi prohibición. Pero toma nota. Soy poderoso. Y yo solo soy el último de los guardianes. De una habitación a otra hay guardianes, cada uno más poderoso que el otro».

Durante años, al campesino se le niega repetidamente la entrada, pero aún así espera. El hombre gasta todo el dinero que tiene pagando sobornos al guardián, quien los acepta y le dice al hombre que es solo para que, «no crea que ha omitido algún esfuerzo». Aún así, le niega la entrada a la ley. El campesino espera y espera hasta que envejece. Finalmente, moribundo y casi sordo, el hombre pregunta: «Todos buscan la Ley, entonces, ¿cómo es que en todos los años que llevo aquí, nadie excepto yo ha solicitado la entrada?» El guardián responde: «Aquí nadie más puede entrar, ya que esta entrada fue asignada solo a ti. Ahora cerraré». 

Es así que también en el caso de quienes buscan, por ejemplo imágenes, se encuentran ante un guardián, a veces invisible, que da acceso a los materiales que fueron creados por otros y se resguardan en un lugar llamado Archivo. No es a primera vista un público de la Ley, sino un público de obras resguardas y en mayor o menor medida preservadas. 

En la opinión de los archivistas el rescate de la memoria que se realiza en los archivos, el patrimonio nacional en forma de películas, videos, registros sonoros y otros medios antiguos deberían, como en la lógica del campesino estar disponibles para todo el mundo. Sin embargo, tienen que hacer el papel de guardianes de la Ley, una que opera sobre toda la producción cultural humana como si se tratara de un gran Archivo que se rige únicamente por su administración y se margina el proceso que los hace significativos: la relación entre las personas que los conocen, más allá del intercambio económico. 

La encrucijada que aquí presentamos tiene particular relevancia cuando hablamos de archivos públicos y que tienen en su origen una vocación de preservación y obligación de dar acceso. Cuando nos preguntamos si todo en ellos es accesible y quién puede acceder, lo hacemos desde tres condicionantes: si el material es identificable, es decir, si sabemos qué hay y cómo ubicarlo; si sus condiciones de preservación nos permiten acceder a él (aquí la digitalización ha jugado un papel importante en los últimos años y regresaremos a ello más adelante) y por último, a qué le llamamos acceso.

Queremos resaltar que esta última condicionante se entiende en un entramado de relaciones entre agentes y cómo ellos operan entre la preservación, la creación de nuevas obras y la gestión de los derechos de uso. Siguiendo la propuesta de Claudy Op den Kamp en el libro *The Greatest Films Never Seen. The Film Archive and the Copyright Smokescreen* (2018) la forma en la que podemos conocer y hacer uso de obra audiovisual se debería plantear desde una reflexión crítica sobre el patrimonio cultural que preservan los archivos comprendiendo que la comprensión de los límites de la legalidad del uso de obras audiovisuales operan en ocasiones como cortina de humo.

Es decir, que si bien existe una legislación de derechos de autor limitada respecto de lo que se puede hacer o no con las obras audiovisuales en general pero en específico sobre lo que pueden o no permitir los archivos públicos con las obras que resguardan, existe un uso de ellas que se ha ampliado con las posibilidades de su reproductibilidad, primero con la llegada del video y posteriormente con la digitalización y el intercambio de contenidos a través internet. El público, en este contexto no puede verse únicamente como consumidor, sino que de múltiples formas se apropia del contenido para incorporarlo en su bagaje cultural y en ese contexto, existe una comunidad de creadores (alternativo al gremio tradicional de creación de películas comerciales) que disputa el derecho de uso para expresarse y discutir la cultura a la que pertenecen esas obras, haciendo uso de lo que se hace disponible en internet más recientemente, pero desde hace al menos 90 años, de los materiales encontrados, por supuesto no es el caso de todas, pero que en muchos casos fueron desechadas por sus primeros dueños en diversas circunstancias. 

Volviendo al relato de Kafka, una de las primeras preguntas que surgen es sobre las motivaciones que tuvo el campesino para querer acercarse a la Ley. Suponemos que el deseo vino antes de saberlo, sino imposible, si de una opacidad perturbadora y atractiva. La respuesta a esta pregunta nos parece cercana a las motivaciones de quien habiéndose acercado a la entrada del lugar llamado Archivo es dirigido a otra puerta en donde habita el guardián de la Ley, a saber, la Ley Federal de Derechos de Autor en México (LDFA).

Situándonos en el contexto de los archivos audiovisuales (los cuales administran una parte del acervo cultural de la Nación de interés de la LFDA, Artículo 1), la siguiente cuestión se refiere a la administración de las motivaciones, quiénes querrían entrar al Archivo y qué harían una vez dentro ¿es posible acceder y cómo sacar algo para que habite en otros territorios?, hablando material y metafóricamente, por ejemplo en exhibiciones no lucrativas pero también como materia prima para nuevas obras.

Estas preguntas las expresamos mientras nos encontramos aquí, a la puerta del guardián pero con distintas motivaciones. La primera de ellas se  nutre de la posibilidad de conocer, básicamente viendo las películas y en algunos casos hacer que otros las vean, es decir, desde la práctica de la exhibición. Parecería que el público de la obra audiovisual alojada en un archivo nace con la creación del espacio mismo, la realidad es que esa promesa se gestiona desde una actitud activa o pasiva sobre lo que permite la Ley. 

La práctica archivística nos interpela con preguntas acerca de los agentes que participan en el uso de la imagen en movimiento. ¿cómo es que la comunidad de la legislación autoral está tan alejada de lo que ocurre en los archivos? En contraposición con preguntarse ¿cómo traer a la discusión conceptos como «películas huérfanas» y «dominio público» para enriquecer la práctica de preservación de la historia cultural del cine? De tal manera que podamos ampliar nuestro conocimiento del pasado a través de un uso crítico de las obras que nos han legado otros autores y con lo cual contribuiríamos a honrar su legado.

Otra motivación es aquella que desde la creación artística se ha desarrollado como actividad creativa a partir del reempleo desde la casualidad de lo encontrado, práctica que ha desarrollado una reflexión sobre la preservación de aquello que se utiliza y que no signifique su destrucción; así que venir a hablar de lo que permite o no el archivo público y lo que consigna la Ley Federal de Derechos de Autor, se desarrolla en primer lugar a la responsabilidad de hacer uso de lo desechado, por ejemplo con aquello que se encuentra por casualidad en en los tianguis y en segundo lugar como agentes de un diálogo sobre lo que permiten o no los archivos públicos con las obras que preservan. 

A continuación, reflexionaremos en torno a la gestión del uso de obras audiovisuales en contextos de archivo, entendiendo este como el espacio que resguarda y organiza objetos según una estructura y que presupone la intención (o al menos, la promesa) de dar acceso a aquello que resguarda. Recuperamos situaciones en las que se ha hecho evidente cómo el derecho de autor y el derecho patrimonial se han manifestado como una administración de las relaciones de quienes nos identificamos en comunidades alrededor del archivo: coleccionistas, creadores, investigadores, archivistas, administradores y el público y sin embargo, faltan aún espacios para dialogar desde la perspectiva de cada una de ellas.

## Tengo pero no tengo

Imaginemos que hemos encontrado una cinta VHS dentro de una caja que pensaban tirar en la casa familiar, ésta tiene en la etiqueta con el título Ben-Hur rayado con pluma azul y sobre él escrito «Cumpleaños Elena». Intuyendo el contenido pero sin saberlo lo guardas y reúnes junto a la cinta fílmica de super8 que un amigo compró el año pasado en un mercado y que te dio a guardar pensando que podría ser de tu interés. Un día encuentras la manera de reproducir el VHS y el super8, observas que la imagen del actor Charlton Heston como preámbulo de una reunión de niños alrededor de un pastel te trae no solo el recuerdo del día festivo sino el ambiente de época de una película que se grabó del televisor en Semana Santa. El rollo de super8 en cambio, te parece un poco lejano pero no extraño, se trata de un comercial de la marca Pathé para difundir su catálogo de cortos. 

Aquí ha surgido un primer relato, al que no atañe aún nada más que la experiencia, pero quisieras mostrar las imágenes para compartir la historia, ¿surgirían las preguntas sobre si puedes usar las imágenes de los objetos encontrados? O si ¿podrías mostrarlas a otros? ¿Podrías cortarlas y pegarlas según la lógica de la memoria detonada por el acto primitivo de querer narrarlas?  La respuesta claramente es que si tienes las herramientas y las habilidades técnicas lo harías. 

Para entender cómo es que se piensa lo que se puede o no hacer con una obra audiovisual fuera de su espacio natural de primera exhibición tenemos que preguntarnos sobre cómo se han formado las colecciones dentro de los archivos públicos o privados, pero también nuestras propias colecciones, las que conformamos por lo que elegimos conservar. 

Una película es creada para un público inmediato, sea esta un largometraje de ficción o documental, un cortometraje publicitario o una película casera, posteriormente a su divulgación comercial o sin fines de lucro, esta reposa en las bodegas de quien la creó, o bien será destruida por no encontrarle valor alguno. Ante ello, los cinéfilos alrededor del mundo desde la década de 1930, empezaron a resguardar copias, vindicando el valor de los espectadores frente al cine. Algunas experiencias se institucionalizaron en la forma de archivos nacionales casi inmediatamente, pero en otros casos, sobrevivieron como colecciones privadas que hasta fechas muy recientes se han incorporado a espacios de resguardo públicos. 

El entusiasmo de aquellos que encontraron en la recopilación un interés cultural no es muy diferente del que nos motiva a querer una copia de una película que nos ha gustado, o de una imagen que encontramos en internet para luego compartir en una red social nuestras impresiones al respecto, la diferencia sustancial es que en internet se crea la sensación de que todo se puede compartir y estará disponible indefinidamente pero en el caso de los archivos de películas, desde principios del siglo pasado, se tuvo que accionar para evitar la destrucción de las mismas inclusive ante el desinterés de sus creadores. Esa diferencia nos une: la sinceridad de nuestra motivación ante las restricciones del derecho privado.

Veamos un ejemplo. Para la Muestra Reencuentros al margen. El cine hecho en cine, en su segunda edición, en el año 2018 quisimos incluir *La película El ángel exterminador* (Luis Buñuel, 1962) y proyectarla en su formato original 35mm. Aunque existe un negativo en la Filmoteca de la UNAM que podría utilizarse para realizar nuevas copias de exhibición, la única copia que nos prestaban para la Muestra estaba en la Cineteca Nacional. Para conseguir el préstamo de una película de acervo se tiene que contar con la autorización de quien detente los derechos patrimoniales, en el caso del cine mexicano, hay que seguir el camino de los herederos de realizadores y productores para en el mejor de los casos tengamos un número telefónico a donde preguntar. 

En este caso, la Cineteca Nacional es propietaria de una copia adquirida por la donación de un particular, pero esto no la hace acreedora del derecho de uso. La Ley Federal de Derechos de Autor (LFDA) establece que «En virtud del derecho patrimonial, corresponde al autor el derecho de explotar de manera exclusiva sus obras, o de autorizar a otros su explotación, en cualquier forma, dentro de los límites que establece la presente Ley y sin menoscabo de la titularidad de los derechos morales a que se refiere el artículo 21 de la misma» (LFDA, Articulo 24), y en tanto se establece que estos derechos están vigentes durante la vida del autor y, a partir de su muerte, cien años más. (LFDA, Artículo 29). 

Si bien se consideran como autores de las obras audiovisuales al: director realizador; autores de argumento, adaptación, guión o diálogo; los autores de las composiciones musicales; el fotógrafo y los autores de las animaciones (LFDA, Artículo 97), se reconoce que el productor, salvo pacto en contrario, es el titular de los derechos de la obra en su conjunto. En este caso Producciones Gustavo Alatriste.

Es común que los dueños de los derechos patrimoniales no cuenten con copias de las películas de las que son propietarios, esto se debe a varias razones, una de ellas es la venta de derechos de películas sin que exista la venta del objeto. Desde la creación de la Cineteca Nacional en 1974 fue posible ejercer la obligación que indicaba la Ley de Cinematografía de 1949, de que el productor o distribuidor de una 
película que pidiera  permiso de exhibición comercial debía enviar una copia en buen estado para su preservación, sin embargo eso no significa que se cuenta con una copia de  todas las películas exhibidas en México, primero porque en 1982 se quemó el acervo que hasta ese momento se había recopilado y segundo, porque si bien existe la obligación, muchos distribuidores consideran innecesario realizar ese procedimiento. 

Otro caso es cuando se conoce quién tiene los derechos patrimoniales y un archivo público tiene una única copia. Esto sucedió en el homenaje a José José titulado «Cubas and footage» en el que quisimos proyectar la película Gavilán o paloma (Alfredo Gurrola, 1974) en La cueva, una sala independiente para 20 espectadores. Investigamos y encontramos que hay una sola copia positiva en 35mm en la Cineteca Nacional, pero esta copia no puede ser exhibida por lineamientos de preservación. Preguntamos a los dueños de los derechos, en este caso Fundación Televisa, quienes nos autorizaron el alquiler de la película (y el correspondiente pago), pero no contaban con copias fílmicas propias. Esta película iba a ser estrenada el 19 de septiembre de 1985. Por obvias razones este estreno se canceló debido al terremoto que impactó la Ciudad de México y retrasó por un largo tiempo su exhibición. Pensamos que eso influyó en el hecho de que hoy no se ubique más de una copias fílmica, ni siquiera el director de la película, quien amablemente ofreció a prestarnos un DVD con una versión de la película telecinada a video, que finalmente exhibimos a un público muy joven que vio por primera vez esta obra. Procedimos a realizar una exhibición privada sin el permiso oficial de los dueños de los derechos. Al público le pareció muy graciosa, bebimos *cubas* y, junto con el poco público de edad más avanzada, cantamos las canciones de José José. 

Estos dos ejemplos muestran el camino cotidiano de quien quiera hacer el uso básico de una película: exhibirla para un público (con fines lucrativos o no). De forma similar, se espera que quien quiera hacer otro tipo de uso, por ejemplo, utilizarlas en un documental deba transitar la misma práctica: buscar una copia, ubicarla en un archivo público o una colección privada, pedir permiso al dueño y asumir la decisión o las condiciones de la misma.

Se revelan aquí dos cuestiones, la primera es que el derecho patrimonial sobre las obras audiovisuales es distinto de la propiedad física del objeto que la contiene. Por ejemplo, con la aparición del videocassette para la exhibición casera de películas, los distribuidores hicieron notar tal paradoja consignando que la adquisición de la copia otorgaba el permiso únicamente para su exhibición privada y casera. Aunque como hiciera Alfredo Salomón en su cortometraje D.R (2002) al usar las advertencias iniciales de los videos acerca de la prohibición de un uso distinto al deseado por los productores, se muestra el problema entre la limitación de lo que se permite y lo que es posible hacer con ellas. 

En el caso de las películas en formato fílmico, la condición se torna complicada pues la propiedad de una copia en soporte nitrato, acetato o poliéster y su correspondiente cuidado es en sí misma onerosa. ¿La preservación de la copia de una película le otorga algún derecho al custodiante frente a los dueños de los derechos patrimoniales que de otra manera tendrían la propiedad únicamente de la abstracción de una obra?

Un modelo que abre la discusión acerca de cómo puede gestionarse el uso de las películas dentro de los archivos es Archivo Memoria, proyecto creado en el año 2010 en la Cineteca Nacional, el cual convocó a donantes de películas amateurs y caseras en pequeños formatos, quienes a cambio de una copia digitalizada podían otorgar algunos permisos respecto al uso de las imágenes de su colección: uso irrestricto o uso con permiso previo dependiendo de la finalidad del creador solicitante. El planteamiento del intercambio desde el principio de la convocatoria ha permitido que las condiciones de uso sean más o menos claras, en un pacto de confianza en donde los donantes reconocían mediante un contrato ser los poseedores de los derechos de autor de las películas depositadas, en su mayoría registros de actividades familiares en el contexto de fiestas, reuniones y viajes.

La segunda cuestión que se revela de la forma en la que los archivos públicos administran el acceso a sus colecciones atendiendo la protección a los derechos de autor es la creación de una comunidad del archivo. Tomando como ejemplo, nuevamente Archivo Memoria, a diez años de haberse creado, sobresale un problema para la continuidad de su operación: la ruptura de la relación entre la institución y los donantes por las causas más comunes: el cambio de domicilio y la  muerte del donante. En realidad, se trata del mismo problema que subyace a la máxima de «pedir permiso de uso a los dueños de los derechos patrimoniales», y es que eso es solo posible mientras exista la relación cercana entre creadores, dueños de derechos patrimoniales y archivos ¿Algún donante tendrá en la mente incorporar en su testamento sus deseos respecto a una película depositada en una institución pública?

Por otra parte, al ser la protección de los derechos de los autores independiente de la formalidad de su registro, es decir, que «no se requiere registro ni documento de ninguna especie ni quedará subordinado al cumplimiento de formalidad alguna» (LFDA, Artículo 5)  para su protección, la cuestión de consultar sobre los permisos se sobreentiende pero en la práctica es una complicación que no corresponde con la producción cultural en la que se da a conocer. 

Al pensar en el papel de los archivos públicos ante este problema, toma relevancia aquello que Alberto López (2020, p.13) llama a pensar cuando explora el término de «los comunes». Él señala que en la experiencia de lo público estatal, que se percibe como un servicio y lo privado, a la que se ingresa como un cliente, se desdibujan los vínculos de compromiso y responsabilidad alrededor de un bien. En nuestro caso, al hablar de lo que se puede o no hacer con las películas que se albergan en los archivos públicos consideramos poner en primer plano el problema como un asunto de corresponsabilidad para el cuidado, disfrute y significación de las obras audiovisuales como bienes culturales, y no solamente como un tema de legislación de permisos.

## Las películas huérfanas y el dominio público

Como acabamos de exponer, el hecho de obtener el permiso de uso o la negación del mismo proviene del conocimiento de quien tiene los derechos patrimoniales de una obra. En un escenario ideal para la lógica legal de protección de los derechos de autor se haría una búsqueda diligente de los productores y sus herederos, pero la realidad es que eso solo sería posible con las obras que tienen créditos en pantalla identificables y con dueños debidamente identificados, lo que revela que la forma en la que se legisla sobre las obras audiovisuales en México se ha configurado bajo una sola lógica de producción y exhibición, aquella en la que los medios de creación eran exclusivos de las compañías productoras, pero con el abaratamiento de los equipos y el conocimiento de su manejo se popularizó, las experiencias creativas se diversificaron. 

Hablamos aquí, por ejemplo, de realizadores independientes y también de instituciones no dedicadas a la cinematografía exclusivamente, por ejemplo el Taller de Cine Experimental al que pertenecía Alfredo Gurrola o el área de  CineDifusión en la Secretaría de Educación Pública, ambos en la década de los setenta. Por lo pronto, aunque no se ha hecho, es posible ubicar a los autores de las producciones colectivas de estos proyectos, pero muy pronto engrosarán los números de aquellas películas de las que no es posible consultar sobre los permisos de uso.

Cuando una película está aún en el periodo de garantía de los derechos patrimoniales, cien años después de la muerte del autor en la legislación mexicana, pero no se han identificado o localizado al dueño de los mismos, el archivo (y quién desee hacer uso) no tiene posibilidad de pedir permiso para exhibirla o realizar una copia de preservación. Esto se conoce como el problema de las obras huérfanas. Este problema motiva que muchas de las obras preservadas en archivos públicos no puedan ser dadas a conocer, o bien, no se escojan para proyectos de preservación o nuevas creaciones pues no se podrán mostrar los resultados. Esta situación es demasiado común no solo dentro de las instituciones, sobre todo públicas, sino entre los creadores que se han dado a la tarea de coleccionar materiales como parte de su actividad creativa. Un detalle interesante de esta situación es que la incapacidad de encontrar a los dueños de los derechos de una obra audiovisual también ha motivado que quien posee una copia no haga público su catálogo, para evitar reclamaciones indebidas, provocando que un buen número de obras permanezcan ocultas.

Otra definición de una película huérfana es aquella que queda relegada, porque no se le encuentra un valor comercial, porque no se cuenta con los recursos económicos para su preservación o exhibición pero también porque al no ser posible contactar a los dueños de los derechos no se les considera prioritarias para su uso, o bien, en el contexto de resguardo existen otras obras a las cuales se decide dar atención. 

Tal fue el caso de la colección de «las chinas», un lote de películas chinas que formaron parte del catálogo de exhibición de la Embajada de China en México. Estas se encontraban en un limbo de películas avinagradas deambulando entre instituciones que no podían preservaras prioritariamente y los archivos chinos habían confirmado la existencia de mejores copias de ellas en sus acervos. Estas películas en algún momento debieron de haber circulado por cineclubes, casas de cultura, universidades y espacios interesados en la cultura de este país, pero llevaban mucho tiempo sin usarse y sufriendo los embates del clima, se encontraban con distintos grados de descomposición. Pero lo que es basura para unos, es tesoro para otros. En un interés de colaboración, el Laboratorio Experimental de Cine hizo una petición para adoptar estos materiales y darles una segunda vida. Las películas se revisaron y limpiaron. Para la experiencia de cine encontrado que venía realizando este colectivo significó la reflexión sobre cómo utilizar estas películas que habían sido renegadas en su forma original y que podrían ser usadas en nuevas obras de cine expandido. La valoración de ellas y su consecuente cuidado se convirtió en un argumento filosófico frente a la imposibilidad de contar con los permisos para su reempleo.

Un caso distinto fue el «las japonesas». Elena Pardo cuenta que fue invitada como proyeccionista para una proyección en 16mm de algunos clásicos del cine japonés por invitación de la Fundación Japón, se enteró que de forma similar a otros consulados y embajadas (como en el caso chino), tenían colecciones de películas que circulaban por el mundo para difundir su cultura. Algunas de las funciones tuvieron mucho éxito, como la de la película *Funeral de Rosas (Parade of Roses, Toshio Matsumoto, 1969).* Al siguiente año la volvieron a contactar para cotizar un programa más ambicioso de proyecciones en 16mm, pero se canceló porque consideraron que el estado de conservación de las películas no era óptimo y preferían destruirlas. Esto sonaba exagerado por lo que pidió la donación de algunas de ellas para utilizarlas en obras de cine de reempleo, pero se negaron justificando que no se contaba con los derechos de autor para realizar una donación de las copias físicas. Un triste final para unas muy buenas películas.

Como sea, cuando las películas no pueden ser usadas (consultadas o exhibidas) o reusadas (en nuevas obras) de forma legal, algunas veces son usadas después de realizar un análisis de riesgos. Es decir, después de una evaluación sobre las consecuencias de no contar con los permisos y se procede como si se tuvieran, en casos como una restauración y/o digitalización por considerarlo de interés para la cultura nacional, su incorporación en una nueva obra, bajo la modalidad de cita, ilustración o referencia «para la crítica e investigación científica, literaria o artística» (LFDA, Artículo 148), o bien su modificación o manipulación para una obra que la use como materia prima y le de una forma distinta, como es el caso del cine de reempleo. 

Este problema fue abordado en otros países llamando la atención al respecto, estableciendo mecanismos para visibilizar que a pesar de una búsqueda diligente no era posible contar con el permiso de los dueños de los derechos patrimoniales y es que el solo trabajo de búsqueda consume el tiempo de archivistas y creadores sin garantía de éxito y por supuesto con recursos económicos y humanos limitados. 

En el 2011, el proyecto European Film Gateway (EFG) organizó la Conferencia «Taking Care Of Orphan Works». Open Conference on Rights Clearance in European Film Archives con el objetivo de abordar un problema común entre los 22 archivos y cinetecas de 16 países de la Unión Europea que se encontraban en un proceso de digitalización y puesta en acceso de sus colecciones: las obras huérfanas. 

La iniciativa buscaba mostrar cómo se enfrentaba el problema en los distintos archivos que participaban en la iniciativa Europeana (biblioteca digital europea de acceso libre que agrupa objetos digitales y/o digitalizados aportados por importantes instituciones culturales). El segundo objetivo fue construir un modelo apropiado para «limpiar» derechos y facilitar el acceso en línea a materiales considerados huérfanos, el cual considerara una solución legal, acuerdos, extensión de licencias y una fórmula para calcular la gestión del riesgo de incurrir en una contradicción legal, esto en el contexto del sector del patrimonio fílmico. El trabajo del grupo perteneciente al EFG dio como resultado una muestra de buenas prácticas para gestionar las obras huérfanas que permitieran a los archivos ejercer la vocación para la que fueron creadas: contribuir a la cultura a través de la preservación de obras audiovisuales y su puesta en acceso. 

La figura de «obra huérfana» no aparece en la legislación mexicana, haciendo evidente su doble orfandad. 

Una situación similar pero con particularidades que requieren de un tratamiento distinto son aquellas en las que la obra es anónima, la LFDA reconoce dentro del dominio público aquellas en las que no el autor se anónimo «mientras el mismo no se dé a conocer o no exista un titular de derechos patrimoniales identificado» (Artículo 153). Tal sería el caso de obras sin créditos como registros sin editar o películas en las que no sea posible identificar la autoría de sus creadores, ya sea porque no se suele poner la autoría, como en algunos casos del cine experimental o porque el material se encuentra incompleto. 

## Administrar el deseo: distintas valoraciones y usos de una obra

Hemos querido mostrar con la reflexión hasta aquí expuesta que la comprensión y ejecución de la protección de los derechos morales y patrimoniales de una obra se ve afectada por la necesidad de que estas tengan un público que las valorice y por las condiciones de preservación y acceso que lo permiten. Podría parecer que la discusión aquí abordada compete a profesionales que trabajan en los archivos públicos o creadores con más o menos experiencia de producción utilizando obras encontradas para la creación de nuevas piezas, tal vez un tema de un círculo acotado de diálogo, sin embargo queremos resaltar que la especificidad de las preguntas aquí planteadas pueden contribuir a visibilizar que la legislación sobre las producción cultural debería incluir a la diversidad de experiencias de las comunidades creativas. 

Para muestra de esta necesidad pensemos cómo se han implementado los recientes cambios a la LFDA, haciendo guardianes de la Ley a los proveedores de servicios digitales (Artículo 114 Octies) en tanto que pueden (y deben) de manera expedita y eficaz «unilateralmente y de buena fe»: remover, retirar, eliminar o inhabilitar el acceso a materiales o contenidos dispuestos, habilitados o transmitidos sin el consentimiento del titular del derecho de autor o derecho conexo. 

Expusimos líneas arriba escenarios en los cuales la solicitud del consentimiento de los titulares de derechos para la exhibición o uso de una película se veía limitada por la dificultad en la práctica de llevar a cabo las búsquedas, pensando en espacios físicos de encuentro en torno a las obras, en lo aquí abordado en contextos de archivo públicos y en colecciones privadas producidas por la recolección de películas desechadas. 

No hay que perder de vista que la supervivencia de una película en estos contextos es producto de un conjunto de selecciones y descartes, de acciones de valoración ejercidas sobre ella que hacen posible su existencia. Esto vuelve relevante pensar que el papel de la preservación y el uso no se encuentra en oposición a una legislación sobre los derechos de los autores sobre sus creaciones, sino que diversifica la perspectiva sobre el lugar que tienen en las culturas que se las apropian.

Lo que podemos deducir contrastando los problemas de la preservación y creación con obras en contextos de archivo y la eficacia en la implementación de las modificación a la LFDA para el contexto digital, es que se reconoce como indeseable, la práctica social de uso de las obras audiovisuales, y ello constituye un peligro tanto para la supervivencia de las obras como para el reconocimiento de sus autores. Significa en última instancia que limitemos las posibilidades de supervivencia de una obra. 

Debemos aprender del camino recorrido de los archivistas audiovisuales y los creadores con materiales de archivo, aunque ciertamente aún con deudas por resolver, para pensar de qué forma el uso ilegal como espectadores creativos es una contribución a la preservación de la cultura audiovisual, de tal manera que el final de una muy buena película sea su reconocimiento y no su ocultamiento bajo la supuesta protección de su integridad.

## Bibliografía

Hilderbrand, L.(2009) Inherent Vice: Bootleg Histories of Videotape and Copyright. Duke University Press.

Kafka, F (2005) [1915] Ante la ley. De Bolsillo.

López, A. (2020). ¿Por qué están de vuelta los comunes? La postcomunidad de los comunes digitales. *Culturales*, 8.

Op den Kamp, Claudy. (2018).The Greatest Films Never Seen. The Film Archive and the Copyright Smokescreen. Amsterdam University Press.

Taking Care Of Orphan Works EFG Open Conference on Rights Clearance in European Film Archives Presentations: [https://www.efgproject.eu/Amsterdam_Conference_3031May11_presentations.php](https://www.efgproject.eu/Amsterdam_Conference_3031May11_presentations.php)

---
tags: ccmx, derecho autor
breaks: false
---

<style>
@import url('http://fonts.cdnfonts.com/css/raleway-5');

@import url('http://fonts.cdnfonts.com/css/fanwood');

@import url('http://fonts.cdnfonts.com/css/prociono');

@import url('http://fonts.cdnfonts.com/css/junction');

@import url('http://fonts.cdnfonts.com/css/ubuntu');

@import url('http://fonts.cdnfonts.com/css/league-mono');

@import url('http://fonts.cdnfonts.com/css/sorts-mill-goudy');


body {
    margin: 5%;
}

h1, h2 {
    text-align: right;
}

h1 {
    padding-right: 3%;
    border-style: solid;
    border-width: 0 .5em 0 0;
    margin-bottom: 3rem;
}

h1 .bold {
    font-family: "Raleway", serif;
    font-weight: 700;
    font-size: 1em;
}

h1 .black {
    font-family: "Raleway", serif;
    font-weight: 900;
    font-size: 1.2em;
}

h1 .thin {
    font-family: "Raleway", serif;
    font-weight: 250;
    font-size: 1em;
}

h1 .mencionAbigael {
    font-family: "Prociono", sans-serif;
    font-size: .8em;
}

h2 {
    font-family: "Raleway", serif;
    font-weight: 300;
    font-style: italic;
    font-size: 1em;
    padding-right: 3%;
    border-style: double;
    border-width: 0 1em 0 0;
    margin-bottom: 3rem;
}

p {
    text-align: justify;
    line-height: 2em;
}

.carta {
    font-family: "Fanwood", sans-serif;
    font-size: 1.1em;
    margin: 2em 0 2em 0;
}

.citaAbigael {
    font-family: "Prociono", sans-serif;
    font-size: 1em;
    text-align: left;
    padding-left: 7%;
    border-style: inset;
    border-width: 0 0 0 5px;
    margin: 3rem;
}

.tabulador {
    padding-left: 3em;
}

.citaAbigaelProsa {
    font-family: "Prociono", sans-serif;
    font-size: 1em;
    text-align: justify;
    padding-right: 7%;
    border-style: outset;
    border-width: 0 5px 0 0;
    margin: 3rem;
}

.citaFloatRight {
    font-family: "Junction", serif;
    font-weight: 500;
    font-size: .9em;
    width: 50%;
    float: right;
    border: solid .3em;
    padding: .8em;
    margin: 0 0 .5em 2em;
}

.citaFloatLeft {
    font-family: "Junction", serif;
    font-weight: 500;
    font-size: .9em;
    width: 50%;
    float: left;
    border: solid .3em;
    padding: .8em;
    margin: 0 2em .5em 0;
}

.citaGrande {
    font-family: "Ubuntu", serif;
    font-weight: 500;
    text-align: center;
    font-size: 2.5em;
    border-style: double;
    padding: .6em;
    border-width: .3em;
    margin: 2em 0 2em 0;
}

.citaGrandeChica {
    font-family: "Ubuntu", serif;
    font-weight: 500;
    text-align: center;
    font-size: 2em;
    border-style: double;
    padding: .6em;
    border-width: .3em;
    margin: 2em 0 2em 0;
}

.citaGrande b {
    font-weight: 700;
    font-size: 1.5em;
}

.citaGrandeChica b {
    font-weight: 700;
    font-size: 1.5em;
}

.teoria {
    font-family: "Raleway", serif;
    font-weight: 600;
    font-size: 1.5em;
}

.citaAmenaza {
    font-family: "League Mono", monospace;
    font-weight: 500;
    font-size: .9em;
    width: 70%;
    text-align: justify;
    margin: 3em 15% 3em 15%;
}

.censura {
    color: black;
    background-color: black;
}

.losTextos {
    margin-top: 20rem;
}

.amigxs {
    margin-top: 10rem;
}

.cuadroFinal {
    margin: 3em 10% 3em 10%;
}

.bracketLeft {
    float: left;
    font-size: 20em;
    margin: 0;}

.bracketRight {
    float: right;
    font-size: 20em;
    margin: 0;
}

.teoria b {
    font-size: 1.3em;
}

.final {
    font-family: "Sorts Mill Goudy", sans-serif;
    font-size: 1em;
    margin: 0 10% 0 10%;
    padding: 1em 2em 1em 2em;
    border-style: solid;
    border-width: 0 .3em 0 .3em;
}

table {
    font-family: "Sorts Mill Goudy", sans-serif;
    font-size: 1em;
    margin: 0 10% 0 10%;
    table-layout: auto;
    width: 80%;
    text-align: center;
}

table i {
    font-family: "Sorts Mill Goudy", sans-serif;
    font-size: 1em;
    font-style: italic;
}

td, th {
    border: .1em solid silver;
    padding: .5em;
}

tr {
    background-color: silver;
    color: black;
}

tr:nth-child(even) {
    background-color:whitesmoke;
    color: black;
}

th {
    background-color: black;
    color: white;
}
</style>

<h1>
<span class="bold">Esto podría ser un </span><span class="black">manifiesto</span><br />
<span class="bold">pero </span><span class="thin">[sólo] </span><span class="bold">es</span><br />
<span class="black">/</span><br />
<span class="thin">[no es más que]</span><br />
<span class="bold">una </span><span class="black">carta</span><br />
<span class="bold">para </span><br />
<span class="mencionAbigael">Abigael Bohórquez</span>
</h1>

<h2>(Pirateca + Natalia Durand + Otrxs)</h2>

<p class="carta">¿Cómo hablarte, Abigael? ¿Cómo dirigirnos a ti, alguien de quien sólo quedan sus libros? ¿Cómo escribirte una carta si no sabes quiénes somos? Pero sí nos conocimos, Abigael, aunque nosotrxs arribáramos al mundo cuando tu cuerpo ya no habitaba esta Tierra: nuestros afectos se encontraron con los tuyos en tu poesía; nos conociste, te conocimos. A través de tus letras, juntxs, inventamos una comunidad provisoria, una comunidad de amigxs. Y hoy queremos contarte esa historia.</p>

<p class="carta">Nuestro primer encuentro con tu obra fue casi un accidente. Sucedió mientras estudiábamos la vida y penurias del creador cuya música más ocupaba nuestros oídos en ese momento, cuyos gestos y sonidos más nos afectaban. En algún momento una amiga nos dijo que un poeta sonorense había escrito un poema titulado «Palabras por la muerte de Silvestre Revueltas». En ese entonces nada sabíamos sobre literatura: nuestra formación musical nos había mantenido ajenxs a ella, de modo que sin saber bien por qué, nos adentramos en la lectura de tu poema:</p>

<p class="citaAbigael">
Digamos que<br />
<span class="tabulador">él era un hombre</span><br />
<span class="tabulador">marcado por el filos de la música.</span><br />
Digamos que<br />
<span class="tabulador">él siempre solitario e infinito,</span><br />
<span class="tabulador">que él siempre infortunado.</span><br /><br />

Digamos que<br />
<span class="tabulador">Donde ponías tus pies y tu palabra</span><br />
<span class="tabulador">ibas dejando al mundo como recién nacido:</span><br />
<span class="tabulador">–cómo te supo a tierra la hiel de los amigos–;</span><br />
<span class="tabulador">desde adentro, la música en acecho</span><br />
<span class="tabulador">te crecía en espejos desquiciados;</span><br />
<span class="tabulador">suspendías diluvios de locura</span><br />
<span class="tabulador">en los muelles mordidos de tu alma,</span><br />
<span class="tabulador">reinos de la maceración y el desencanto,</span><br />
<span class="tabulador">cuando barcos terrestres</span><br />
<span class="tabulador">te atracaban adentro.</span><br /><br />

Lo pusieron delante de la vida.<br />
Se lo llevó la vida por delante.<br />
Siempre tuvo Silvestre dónde caerse muerto,<br />
pero jamás Revueltas dónde caerse vivo.<br />
Nunca tuvo el amor tan cumplida escultura<br />
y jamás la violencia tal golpe de martillo.<br />
Nunca tuvo que dar y lo dio todo,<br />
pero estuvo en la carne donde estuvo.
</p>

<p class="citaFloatRight">El sujeto –como el autor–  no es algo que pueda ser alcanzado directamente como una realidad sustancial presente en alguna parte; por el contrario, es aquello que resulta del encuentro y del cuerpo a cuerpo con los dispositivos en los cuales ha sido puesto en juego. Ya que también la escritura es un dispositivo, y la historia humana no es quizá otra cosa que el incesante cuerpo a cuerpo con los dispositivos que ellos mismos han producido: antes que ninguno, el lenguaje.</p>

<p class="carta">Fue con tu poema que lo entendimos todo: sentimos al fin conocer a Revueltas. Ninguna biografía, ensayo o escuela nos había dado esa transparencia. Ni el estudio formal de sus partituras nos dijo tanto sobre su música, su persona y su cosmos como lo hicieron tus versos. Volvimos a escucharle con otros oídos, esta vez más atentos, más amorosos. Pero también fue así como descubrimos la poesía, su potencia para ser conocimiento sensible, su capacidad para tocarnos y hacernos escuchar. De pronto tuvimos la certeza de que nos conocíamos, que juntxs, habíamos escuchado la música de Revueltas; sentimos tu dolor por su muerte, por su vida en la pobreza, por el olvido y abandono en el que vivieron sus creaciones.</p>

<p class="citaAbigael">
Y me encuentro de pronto en medio de morirme.<br />
–Tira a llorar y llora!–<br />
Despierto de improviso,<br />
cercenada de un tajo mi voz:<br />
–Silvestre ha muerto!–<br />
En el más alto sueño no lo es bastante nada;<br />
sólo correr a tientas<br />
desde los negros escondrijos de la repleta oscuridad,<br />
hasta el grito más próximo.<br />
–Llora a matar y olvida!–<br />
Busco la luz<br />
y me hallo ciego a la mitad del cuarto.<br />
Pero la calle existe. Hasta que el alba muge<br />
en la estruendosa necesidad.<br />
¿A dónde ir?<br />
Pero la calle existe.<br />
Ahora son los párpados sin fondo<br />
bebiéndose el camino.<br />
Los pasos van ahondando, ahondando, ahondando.<br />
En la calle, el silencio<br />
duele dentro del pulso lento de la neblina<br />
y un pedazo de cielo se tumba en la ciudad.<br />
Entonces llueve.<br />
Pero siempre habrá alguien como yo<br />
plantado bajo la tormenta.
</p>

<p class="citaFloatLeft">Artista o poeta no es aquel que tiene la facultad de crear, que un buen día, a través de un acto de voluntad u obedeciendo un mandato divino (la voluntad, en la cultura occidental, es el dispositivo que permite atribuir las acciones y las técnicas a un sujeto como de su propiedad), decide, no se sabe ni cómo ni por qué, poner en obra. Al igual que el poeta y el pintor, también el carpintero, el zapatero, el flautista y, en fin, cualquier persona, es titular trascendente no de una capacidad de actuar o de producir obras: más bien son seres vivientes que, en el uso y sólo en el uso de sus miembros así como del mundo que los rodea, tienen la experiencia de sí y se constituyen como formas de vida.</p>

<p class="carta">Después comenzamos a leerte más, Abigael, nos adentramos en tu obra y la encontramos fascinante: empezamos a sumergirnos en ti, en tu palabra. Buscamos poemas tuyos en internet, fuimos a bibliotecas y librerías, preguntamos a nuestrxs amigxs y profesorxs. En ese entonces fue difícil encontrarte. Tu voz nos llegaba a cuentagotas. Tal vez eso nos permitió leerte con una atención especial, cada vez con más cariño. Descubrimos en ti una sensibilidad que imitamos hasta el plagio. Sentimos que compartíamos pensamientos políticos, que luchábamos y gritábamos las mismas consignas, que pensábamos juntxs y compartíamos nuestrxs deseos; que nos entristecíamos, amábamos y reíamos juntxs.</p>

<p class="citaGrandeChica">El arte no es sino el modo en que el anónimo al que llamamos artista, manteniéndose en constante relación con una práctica busca constituir su vida como una forma de vida: la vida del pintor, del carpintero, del arquitecto, del contrabajista, en las que, como en toda forma-de-vida, lo que está en cuestión es nada menos que su felicidad.</p>

<p class="citaAbigaelProsa">¿Por qué un artista, un creador, ha de sufrir hambres y miserias? Aquí descansa entre nosotros el secreto del fracaso de la cultura de México como pueblo. Somos un país de descamisados y zánganos… Se desprecia al músico, al poeta, por considerarlo como a los bufones de los burócratas. Pero es que se les hace bufones por la fuerza del hambre. ¿Es una ambición innoble poder estar en paz con el pan para poder crear mejor?</p>

<p class="citaAbigael">
(¿Y la poesía?<br />
Bien.<br />
Dentro del hambre.<br />
Gracias).
</p>

<p class="carta">No entendíamos por qué era tan difícil conseguir tus libros. ¿Por qué no eras famoso? Para nosotrxs era tan claro que tu poesía debía estar en todos lados. Entonces empezamos a investigar más sobre tu historia. Fue así como supimos que el canon literario te había marginado: no podían soportar la desnudez de tus palabras. Esos hombres que se cubrían de aromas limpios no toleraban que la fetidez y la ternura devinieran una misma en tus páginas. Nada querían saber del amor entre cuerpos «masculinos». Eran esos hombres que se entregaban premios entre ellos, los mismos que habían crecido rodeados de inmensas bibliotecas familiares y no «leyendo de prestado», como te escribieron en otra carta póstuma, así como nosotrxs te escribimos ahora.</p>

<p class="citaAbigael">
Porque hay otro camino.<br />
Gritar.<br />
Hasta que sólo después de muertos...
</p>

<p class="carta">Poco después la UACM publicó dos libros con poemas tuyos, luego el Instituto Sonorense de Cultura editó tu poesía reunida e inédita. Ahí descubrimos <i>Poesida</i>. Ese poemario conmocionó nuestro mundo. Compartimos el texto con nuestrxs amigxs y, tal vez porque todxs ellxs buscaban practicar deseos y corporalidades no hegemónicas –como tú y como nosotrxs–, tu palabra nos conmovió hasta el estupor: era el acontecimiento de lo imprevisible.</p>

<p class="citaAbigael">
Estáis muertos. Pero,<br />
¿en verdad estáis muertos,<br />
promiscuos homosexuales?<br />
MUERTOS SIEMPRE DE VIDA:<br />
Dice Vallejo,<br />
EL CESAR.
</p>

<p class="carta">Experienciar en nuestra propia carne tu poesía, vivir la piel humana de tu palabra, la dermis de la realidad, fue vital para nosotrxs. Algún tiempo después, influenciadxs por ti y por muchas otras amistades –lejanas y cercanas, en vida y muertas de vida– quisimos hacer algo que pudiera multiplicar esa experiencia que así como tuvimos contigo, también vivimos con otros textos. Sentimos la necesidad de hacer llegar esa vida sensible inherente a la filosofía y la poesía a tantos espacios como pudiéramos: queríamos, como tú nos enseñaste, habitar el pensamiento y la sensación trascendiendo leyes y convenios, partidos políticos y conflictos sociales: entregando el habla al cuerpo. Pero, ¿cómo hacer?</p>

<p class="citaGrande">
No <i>¿qué hacer?</i><br />
Sino <i>¿cómo hacer?</i><br />
La cuestión de los medios.<br />
No la de los fines, los objetivos.<br />
La cuestión del cómo. La atención al cómo.<br />
Volverse atento al tener-lugar de las cosas, de los seres.<br />
A su acontecimiento.<br />
¿Cómo hacer?<br />
<b>¿Cómo permanecer en guerra sin perder la ternura?</b><br />
</p>

<p class="citaFloatRight">El archivo (y el texto) habla, se activa a través de nuestro cuerpo. Con nuestra danza hacer danzar al archivo. El deseo.</p>

<p class="carta">Después entramos a la facultad y sentimos que ese germen se expandía a través de muchos más textos y cuerpos: tuvimos amigxs que nos mostraron otros mundos, otras corporalidades, otras formas de amar, de leer, de sentir. Leímos, leímos mucho, pero nunca dejamos de leer como la primera vez que lo hicimos contigo: escuchando desde el cuerpo. Cada texto nos atravesaba y nos transformaba: no sabíamos que esas potencias nos habitaban. Fue así como nos dimos cuenta de algo en lo que ahora creemos fervientemente: leer no es, nunca había sido otra cosa que la experiencia común de nuestros cuerpos encontrándose, aunque no siempre se tocaran.</p>

<p class="citaFloatLeft">El lenguaje y la escritura no son otra cosa que un constante flujo de saberes y signos, de cuerpos y afectos. Leer o escribir implica siempre trazar en relieve los soportes de lo escrito; componer y dejarse impregnar por imágenes y conceptos.</p>

<p class="carta">¿Cómo continuar ese movimiento? ¿Cómo dar cauce al flujo para que no se detuviera? ¿Cómo cuidar del acontecimiento de la amistad? Entonces descubrimos que el libro mismo es ya una amistad. En el acto mismo de leer y escribir hay siempre una complicidad secreta, una comunidad provisoria. Siempre leemos y escribimos con otrxs, juntxs: la lectura y la escritura son, invariablemente, una actividad inapropiable.</p>

<p class="teoria">Ninguna escritura puede ser excluyente. Mi escritura existe porque otrxs existen. Mi escritura es siempre la de otrxs.</p>

<p class="citaGrande">La escritura es precisamente el arte de ejecutar artísticamente un asunto descubierto por otrxs.</p>

<p class="teoria">La escritura y, en general todo conocimiento y cultura, no son otra cosa que la acumulación micélica de todo aquello que lo humano (y no-humano) ha creado a través de su habitar en el mundo.</p>

<p class="carta">Nosotrxs decidimos replicar a toda costa la formación de ese libro-comunidad. Para nosotrxs, sencillamente, no puede ser de otra manera. Al mundo que pretende arrancar la experiencia sensible del micelio –siempre múltiple– de la lectura y escritura para transformarlo en una actividad privada, individual, excluyente, capitalizable y mercantilizable, nosotrxs decidimos oponernos molecularizándolo todo. Hacer del estrato-libro, del objeto-libro-mercancía una molécula integrante de un cosmos impersonal, ajeno a cualquier forma de propiedad.</p>

<p class="teoria">Toda escritura es inapropiable. Nada en ella puede ser propiedad porque como indica el origen latino de la palabra, toda propiedad implica un «poner en privado», sacar de lo público, estratificar, paralizar el flujo. Cualquier intento de privatización textual implica siempre arrancar la experiencia sensible del texto, convirtiéndolo en una ficción individualizadora.</p>

<p class="citaGrande">Nunca es común una propiedad, sólo lo es lo inapropiable. El acto de compartir este inapropiable es el amor.</p>

<p class="carta">Un día empezamos a escanear los textos que nos provocaban para así compartirlos con nuestrxs amigxs. Eventualmente nos hicimos de una colección amplia que no circulaba en la red. Entonces, sin pensarlo mucho, hicimos una especie de biblioteca pública con nuestros archivos, a la que llamamos Pirateca. Ni siquiera fue 	que fundáramos algo, ¿sabes? Sencillamente no podíamos responder de otra forma a eso que sentíamos por los textos que leímos juntxs: <i>necesitábamos</i> compartirlos. Si te somos sincerxs, no hay punto de quiebre, no hay gran acontecimiento ni gran historia. Sólo éramos un grupo de amigxs atravesados por sus lecturas; amigxs buscando más amigxs. En ese sitio web, que de algún modo para nosotrxs existía desde antes, empezamos a subir nuestros libros para que cualquier persona con acceso a internet pudiera leer eso que nos conmovía. En vez de «así fue» preferimos decir: «así ha sido siempre».</p>

<p class="citaFloatRight">¿Qué podemos devolver al mundo que nos da la totalidad de lo dado, la totalidad del don?</p>

<p class="carta">Nos entregamos a escanear y subir libros a internet sin esperar nada a cambio: fuimos a librerías y bibliotecas, hicimos nuevxs amigxs en el proceso, compartimos saberes y afectos, aprendimos cosas nuevas. Sin esperarlo, nos dimos cuenta de que somos muchxs quienes buscamos compartir las experiencias que posibilitan los libros sin buscar una retribución, sino por la pura alegría de compartir. De pronto se hizo evidente que nuestras prácticas tenían un potencial político, así que comenzamos a asumirlo y ejercerlo. Comenzamos a delinear un nuevo <i>nosotrxs</i>. Escribimos, leímos y escuchamos.</p>

<p class="teoria">El objeto que en Occidente llamamos libro poco tiene que ver con el texto, con su experiencia sensible. El objeto-libro, con todo el aparato inherente a su producción, comercialización y legislación, no es otra cosa que un producto más, una mercancía.</p>

<p class="teoria">Todo discurso que fetichiza el objeto-libro para ver en él un contenedor unívoco de conocimientos y saberes, invisibiliza el <b>flujo corporal-sensitivo</b> que corre por el texto.</p>

<p class="teoria">Nosotrxs llamamos a la muerte del objeto-libro para recuperar de su cadáver el flujo de vidas y habitares que existen en la escritura.</p>

<p class="citaGrande"><b>El Libro</b>, en la medida en que se situaba frente al lector de la misma forma que el Sujeto clásico ante sus semejantes, fingiendo ser una entidad completa y una cerrada autosuficiencia, <b>es</b>, al igual que la figura clásica del «Hombre», <b>una forma muerta.</b><br />
Más allá de su aspecto de clausura, que los grandes libros nunca hayan dejado de ser aquello que lograba <b>crear una comunidad</b>, que, dicho de otro modo, el Libro haya tenido siempre su existencia <b>fuera de sí</b>, es un hecho en suma sólo admitido en fechas recientes.<br />
La experiencia permitirá constatar que <b>el fin del Libro</b> no <b>significa</b> su brutal desaparición de la circulación social sino, por el contrario, <b>su proliferación absoluta.</b></p>

<p class="carta">Poco a poco nos quedó claro que ese nosotrxs, esa práctica, ese habitar la escritura y la lectura desde los afectos era una acción política y que, por serlo, tendría detractores con afectividades opuestas a las nuestras, con ideas y sentires antagónicos. De pronto fue evidente que nuestras actividades ponían en cuestión algo mucho más grande de lo que hubiéramos imaginado.</p>

<p class="citaFloatLeft">Entre las latitudes extremas de la comunidad y de la hostilidad se extiende la esfera de la amistad y de la enemistad. La amistad y la enemistad son nociones ético-políticas. Que la una y la otra den lugar a intensas circulaciones de afectos, revela únicamente que las realidades afectivas son objetos de arte, que el juego de formas-de-vida puede ser <i>elaborado</i>.</p>

<p class="carta">Nos dimos cuenta que nuestras formas de ejercer el amor, la amistad, la lectura, la escucha y la escritura eran parte de una una forma-de-vida sin lugar en la subjetividad capitalista. Descubrimos que para esa estructura nuestro amor era inmoral, nuestros saberes inútiles y nuestro acto de compartir era robo. Tal vez por esto los abrazamos con más fuerza. No vimos otra posibilidad, mas que asumir la cualidad subversiva de nuestros afectos. Decidimos seguir amándonos como aprendimos a amarnos contigo, Abigael, a seguir cultivando nuestros gestos y saberes; compartiendo todo lo que estaba a nuestro alcance. Elegimos asumir y acuerpar esa postura política, decidimos que la construcción del <i>nosotrxs</i> tendría que ser lo más radical posible, que habría que resistir, que habría que luchar. Decidimos hacerlo, juntxs.</p>

<p class="teoria">Concebimos la muerte del objeto-libro no como una acción específica y situada, sino como un quehacer permanente, múltiple y colectivo.</p>

<p class="citaGrande">Lo común no son nunca los objetos, son las relaciones sociales, son los <i>hacer-común</i>. Lo importante no es la riqueza material compartida sino el acto de compartir en sí y los vínculos de solidaridad que se crean en el proceso.</p>

<p class="teoria">Queremos llamar a centrar nuestros esfuerzos en liberar los textos de la prisión del objeto-libro para volver a poner en circulación los saberes y afectos y, de esta forma, revitalizar su potencia creadora, corporal y sensible; para que nuevos cuerpos sean tocados, para que nuevas sensibilidades surjan, para que los flujos vuelvan a cobrar presencia en nuestros cuerpos.</p>

<p class="carta">Nos pasaron, a Pirateca le pasaron muchas cosas desde su gestación. Hubo encuentros y desencuentros; aprendimos a cuidarnos y a ser menos osados (bueno, sólo a veces). Definitivamente nuestra historia podría ser contada de muchas maneras, pero justo ahora queremos hablarte del incidente que te puso una vez más en el centro de nuestra atención. Como era lógico, un día decidimos subir el libro de tu poesía reunida e inédita a nuestro sitio: tenías que estar ahí. Anunciamos nuestro próximo escaneo y casi de inmediato recibimos este mensaje:</p>

<p class="citaAmenaza">Soy dueña de los derechos de autor de Abigael Bohórquez, ya que soy su <span class="censura">xxxxxxx</span> <span class="censura">xxxxxxx</span> y veo que quieren divulgar uno de los libros de mi <span class="censura">xxx</span>.<br />
Ya lo estoy mirando con mi asesor legal.<br />
La obra de Abigael está protegida por la Ley Federal de Derechos de Autor y por los tratados internacionales y es irrenunciable en su difusión en todos los soportes. Le pido por favor que se pongan en contacto con <span class="censura">Xxxxxxx</span> <span class="censura">Xxxxxxxxxx</span>, quien es <span class="censura">xxxxxx</span> <span class="censura">xxxxxxxxx</span> de Abigael, en Latinoamérica, Estados Unidos, pues ya tiene conocimiento del caso. Si ustedes desean difundir alguna de las obras de mi <span class="censura">xxx</span> debe hacerse a través de él.</p>

<p class="carta">Y minutos después, este otro:</p>

<p class="citaAmenaza">Por este medio me pongo en contacto con ustedes con la intención de informales que los derechos de autor de la obra de Abigael Bohórquez están protegidos, por lo que les solicito en mi carácter de <span class="censura">xxxxxx</span> <span class="censura">xxxxxxxxx</span> del autor que no suban el libro escaneado en sus redes sociales o plataformas, ya que esto viola la Ley Federal de Derechos de Autor, el Convenio de Berna y los demás tratados internacionales en materia de derechos de autor. Agradezco mucho el interés en la obra del autor, pero los derechos están protegidos y reconocidos plenamente a favor de su <span class="censura">xxxxxxx</span> <span class="censura">xxxxxxxxx</span>. Como <span class="censura">xxxxxx</span> <span class="censura">xxxxxxxxx</span> del autor, puedo sugerirles que pueden dar a conocer algún poema o fragmento de obra, pero no el libro completo.</p>

<p class="carta">¿De qué te protegían, Abigael? De verdad, de verdad, no sabemos ni cómo decirlo: <i>no podíamos entender</i>. «Proteger» era un eufemismo burdo para adueñarse de tus poemas, para mantenerlos ocultos y a la mano sólo de unos cuantos. ¿Protegerte de qué, Abigael? ¿De que al fin te leyeran más personas? ¿«Proteger»? Tal vez «esconder» sería una mejor palabra; «apropiarse», sí, eso. Lo que ellos querían era hacer de los afectos vitales de tu poesía un objeto cualquiera del capital, una posesión más. Nos pareció aberrante. Donde ellos veían propiedades, nosotrxs veíamos amor.</p>

<p class="citaAbigael">mi poesía es sólo eso: amor. y entiéndase con ello todo de lo que es capaz el amor. este ser hombre para el amor nada más.</p>

<p class="teoria">Ciertas estructuras políticas, aparatos legales y fabriles que defienden al objeto-libro nombran a toda línea de fuga que escapa a la materialidad mercantil, como robo. Nada más equivocado: la palabra «robo» tiene su origen del alemán antiguo <i>rauben</i>, que significa despojar, privar a alguien de un objeto, desposeerlo.</p>

<p class="teoria">El texto está lejos de ser un objeto absolutamente material, mercantilizable.</p>

<p class="teoria">Multiplicar y difundir un texto, expandir sus capacidades afectivas y sensoriales no despoja a nadie de la posibilidad de generar afectos y emociones al relacionarse con él. No hay despojo en liberar los afectos de la prisión del objeto-libro.</p>

<p class="teoria">Hay otra palabra más acertada para referirnos a este acto: de las partículas latinas ex-pro-privus, <b>«expropiar» significa</b> sacar de lo privado, <b>poner-en-común</b>. La escritura siempre es inapropiable, nunca puede ser privada.</p>

<p class="teoria">Por otro lado, el objeto-libro, producto mercantil y lujo de clase, por nacer y formar parte de todo el aparato capitalista, es inherentemente privado, apropiable, pero también, propenso a la expropiación, al robo.</p>

<p class="teoria">Nuestro llamado a <b>la muerte del objeto-libro</b> tiene el camino de la expropiación, del «robo».</p>

<p class="citaGrandeChica">El goce del texto, de los saberes, como el goce de la vida, no se mendiga, se toma.<br />
El «robo» al que nosotrxs llamamos es más bien la restitución, la recuperación de la posesión común, la reapropiación y, por ende, la multiplicación y diseminación de los afectos y sensibilidades que se producen al contacto con una obra.<br />
La expropiación es nuestro medio de rebelión para combatir <b>el más inicuo de todos los robos: la propiedad privada</b>.</b></p>

<p class="carta">Por supuesto, subimos tu libro al sitio web. Muy pronto recibimos este otro mensaje:</p>

<p class="citaAmenaza">Les solicito amablemente que retiren de forma inmediata el libro que subieron escaneado, ya que viola la Ley Federal de Derechos de Autor, particularmente en su artículo 27. Entiendo la intención de la página, pero le informo que la obra de Abigael Bohórquez será de dominio público hasta el año 2095. Antes, tiene herederos. El desconocimiento de las leyes no exime a nadie de responsabilidades. Amablemente le pido que retire la obra para evitar problemas legales que puedan perjudicar en lo económico. Insisto, entiendo la postura, pero no por encima de la legalidad, no en hombre de la familia de Abigael Bohórquez y no en nombre del propio Abigael Bohórquez.</p>

<p class="carta">Y también este otro, más violento, ya sin retóricas amables de por medio:
</p>

<p class="citaAmenaza">
Ya están procediendo legalmente y créeme que haré hasta lo imposible por que esa demanda proceda.<br />
Es esfuerzo mío mi trabajo y el de mi editor.<br />
Te crees o se creen muy listos.<br />
Pero si alguien no les había dado batalla yo sí.<br />
Porque es algo que hicimos con mucho esfuerzo y dedicación.<br />
Lo hablé claro.<br />
Es lo único que voy a repetir.<br />
No tienen derecho para hacerlo y están en un problema.<br />
Quisiera que hicieran dedicaran y esforzaran su vida en algo.<br />
Pero no pasarán de ser lo que son.<br />
Una página que daña a terceros.
</p>

<p class="carta">¿Qué era todo eso? No podíamos creer lo que leíamos. Todo nos sonaba tan ajeno: a nosotrxs, a tu poesía. Definitivamente no hablábamos la misma lengua, ni afectiva ni discursiva. ¿Acaso querían que te mantuvieras en el olvido, como cuando estabas vivo? ¡¿Esperar hasta el 2095?! Qué temerarios se vieron en afirmar que habrá mundo para entonces...</p>

<p class="carta">No mentimos al decir que nunca hubo claridad de hacia dónde íbamos, que nuestra única certeza era la sensación. Pero lo que vino luego fue tan inesperado que devino inefable. Estaba en las antípodas de esas tristes, tristísimas intimidaciones. Y es tan sencillo, tan obvio: muchxs empezaron a leerte, Abigael. Personas de lugares y orígenes tan distintos, ¡te estaban leyendo, se estaban encontrando a través de tu poesía! Si en vida tus obras permanecieron marginadas, hoy, 26 años después de tu muerte, había mucha gente escribiendo de ti, publicando fotos o leyendo en voz alta sus poemas favoritos. De pronto, al fin, habías alcanzado a tus lectorxs, querido Abigael. Y nosotrxs no éramos nadie en medio de eso, apenas unxs intercesorxs cualquiera, da igual: fuiste tú el que congregó las fuerzas del mundo para poner a vibrar, en conjunción, los afectos de tantxs.</p>

<p class="citaAbigael">
Nació, digo,<br />
de nuevo.<br />
Para que sepan todos.<br />
</p>

<p class="carta">Y sí, claro que tuvimos miedo. También es cierto que no era la primera vez que alguien nos amenazaba. Y tampoco será la última. Porque nuestros afectos siempre fueron repudiables para ellos. Tal vez tengamos eso en común contigo, tal vez por eso te sentimos siempre tan cerca. ¿Te imaginas? Nosotrxs, un grupo de amigxs con libros, internet y un escáner, de algún modo estábamos atentando contra cierto orden establecido. Porque si hablamos con simplicidad, no somos tanto más que eso. Quién lo diría: al parecer la amistad sí representa un enfrentamiento al sistema en marcha durante este tiempo que ya no te tocó vivir, querido Abigael. Mientras escribimos esto, de pronto para nosotrxs algo se vuelve totalmente evidente, es hasta ahora que esta realidad se transparenta: nuestra forma de leerte, de sentirte, de escucharte y de compartirte es ilegal.</p>

<p class="carta">Tampoco podemos negar que nos entristece la existencia de esos otros afectos. Nos duele que exista alguien que al leerte, al enfrentarse a tus poemas, vea una en ti una herencia, una suma de dinero, un reconocimiento público. Nos aflige saber que esas afectividades sean las predominantes. Pero algo más pasó en nosotrxs: esos mensajes amenazantes nos permitieron situar nuestra propia potencia. El juego de intensidades debía ser elaborado, ahora. Ellos, esas personas concretas no eran nuestros enemigos: era su forma-de-vida la que no aceptábamos. Nuestra estrategia para combatirla fue compartir tu libro. Así lo hicimos. Y lo seguiremos haciendo.</p>

<p class="teoria"><b>Nuestra apuesta es micropolítica</b>. Aborrecemos todo lo que tenga intenciones macropolíticas porque esa forma de hacer política invisibiliza los flujos de afectos que corren por los grupos humanos, para ver en ellos conjuntos manejables de «preferencias políticas», votos, izquierdas o derechas.</p>

<p class="teoria">No queremos la construcción de ninguna «sociedad» porque aborrecemos <i>la sociedad</i> como concepto unificador. Hemos renunciado a toda lucha macropolítica, a todo movimiento social, a toda revolución universalizante. No queremos construir sociedades porque éstas requieren formular un programa establecido, un proyecto, un objetivo único.</p>

<p class="teoria"><b>Nosotrxs preferimos</b> el camino largo: un camino que piensa en fragmentos, que pone atención a <b>los gestos</b> y a partir de ellos inventa estrategias, situaciones, espacios habitables. En vez de concretar objetivos políticos, nosotrxs queremos <b>poner en relación nuestros afectos</b>, y así generar nuevos espacios, prácticas y habitares; otras formas-de-vida.</p>

<p class="citaGrandeChica">La disolución del aparato capitalístico no es el objetivo de la acción micropolítica, sino su consecuencia: su objetivo central son los devenires. En esta operación micropolítica de combate, las fronteras entre política, clínica y arte se vuelven indiscernibles, cada gesto de insurrección se vuelve, en sí mismo, un movimiento de resurrección de la vida.</p>

<p class="teoria">Proponemos radicalizar las potencias subversivas que nos habitan. Proponemos transformar en hogares y formas-de-vida todos los destellos revolucionarios que se asoman en nuestros cuerpos. Creemos que es posible construir otros mundos si comenzamos a habitarlos.</p>

<p class="teoria">Creemos que <b>okupar</b> nuestros cuerpos y nuestros ecosistemas tiene una potencia subversiva que buscamos radicalizar hasta las últimas consecuencias. A la conformación de una sociedad oponemos la <b>okupación</b> del mundo de manera des-organizada, molecular, sin órganos, sin jerarquías ni programas. A la construcción de principios, oponemos el ejercicio de la vida desde la multiplicidad provisional.</p>

<p class="teoria">No queremos ser líderes ni coordinadorxs, no queremos devenir ejemplares ni ser mártires. No queremos tomar la voz de nadie, ni que nuestras voces sean tomadas por otros. Aborrecemos la idea de «hablar por los que no pueden». Lo único que deseamos es movilizar todxs juntxs nuestras potencias singulares en entramados revolucionarios radicales. <b>Buscamos aliadxs</b>. Sabemos que nuestrxs aliadxs están ya aquí, que se nos han adelantado; es la atmósfera que se respira. <b>Queremos respirar juntxs, nosotrxs, hoy</b>.</p>

<p class="teoria">Nuestro llamado es a cuestionar nuestros afectos, nuestras corporalidades, nuestros saberes y deseos. A <b>expropiar</b>, a hacer-común todo lo que tenemos. Nuestro llamado es a nuestrxs amigxs: lxs existentes y lxs que vienen. Lxs amigxs a quienes nos une el rechazo al estado de cosas actual y el deseo de <b>imaginar un habitar distinto</b>; o incluso, a esxs amigxs que nos une la posibilidad de compartir por el puro placer de encontrarnos. Creemos que abrir fragmentos de tiempo y sensibilidad para hacer amigxs desde el deseo es una forma de gestar otros devenires.</p>

<p class="teoria">Por eso, nuestro llamado es a <b>desobedecer</b>. A no esperar que el proyecto civilizatorio-democrático avale, permita y legalice nuestras lecturas y escrituras. Nuestro llamado es a habitarlas, nosotrxs, hoy.</p>

<p class="teoria">Leer y escribir sin autorización y en desobediencia. <b>Habitar</b> la desautorización y <b>la ilegalidad</b> como un comienzo de otras lecturas, otras escrituras posibles; otro(s) mundo(s).</p>

<p class="carta">Aunque siempre ha sido así, hoy sabemos que tus textos, que todos los textos, forman parte del mismo entramado. Mientras escribimos estas letras y también cuando dejemos de hacerlo, tu poesía estará viajando para encontrar a sus amigxs, querido Abigael. Por fin tu poesía fluye más libre, más dispersa. Nosotrxs también nos dispersamos en este trayecto que continúa: porque hoy se trata de ti, pero esta lucha va más allá de cualquier nombre.</p>

<p class="teoria">Nos gustaría que Pirateca fuera un llamado a escuchar nuestros cuerpos y nuestros deseos para formular <b>gestos radicales</b> que generen nuevas formas de concebir nuestra vida y nuestro habitar en el mundo. Que sea un llamado a desobedecer, a destruir todo proyecto universalizante, a encontrar nuevas formas de compartir, a inventar otras escrituras. Que sea un llamado a <b>resistir</b> y a <b>re-existir</b>. Creemos que no hay resistencias pequeñas: hay cuerpos que insisten en que es posible configurar otras formas de habitar el mundo. Y <b>para nosotrxs siempre es esto, o morir</b>.</p>

<p class="citaGrande">Nuestra lucha no acabará hasta que todos los seres pongan en común sus alegrías y sus penas, sus trabajos y sus riquezas; <b>hasta que todo pertenezca a todxs.</b></p>

<h1 class="losTextos"><span class="black">¿De dónde vienen los textos?</span></h1>

<p class="final">Este texto, estos textos, fueron escritos desde la multiplicidad. Porque nosotrxs creemos en la apropiación afectiva más que en la citación académica des-corporizada, nos gustaría explicitar nuestro mecanismo de escritura. Aquí aparecen muchas citas de aquellxs que amamos: todas ellas están intervenidas; algunas dispersas en recuadros y otras, asimiladas en los párrafos. Sólo con los poemas de Abigael Bohórquez mantuvimos fidelidad con los originales, dejando un espacio en blanco para indicar que habíamos omitido algún fragmento del poema. Nuestro mecanismo consiste en decir junto a quiénes escribimos estas líneas; en nombrar a nuestrxs amigxs, nada más. Por eso aquí abajo, en <i>Otrxs amigxs</i>, colocamos sus nombres y los de aquellos textos que tomamos prestados, cada uno con su link de descarga para que así el flujo pueda continuar. Para decirlo mejor, nos adherimos a estas otras palabras que no son nuestras (y que tampoco son de nadie, como todas las palabras) de la <i>Manifiesta para despatriarcalizar el archivo</i>:<br /><br />

<b>Repensar, replantear, copiar, cortar, fragmentar, pegar, reescribir, reapropiar, imaginar, cuidar, ficcionar, intuir, inventar, empatizar, amar... En esos actos se encuentra también nuestra lucha.</b></p>


<h1 class="amigxs">
<span class="black">Otrxs amigxs</span><br />
<span class="thin">(en orden de aparición)</span>
</h1>

<table class="citas">
<tr>                
    <th>Texto</th>
    <th>Descarga</th>
</tr>
<tr>
    <td>Abigael Bohórquez, <i>Poesía reunida e inédita</i></td>
    <td><a href="http://pirateca.com">la.pirateca.com/ab</a></td>
</tr>
<tr>
    <td>Giorgio Agamben, <i>Profanaciones</td>
    <td><a href="http://pirateca.com">la.pirateca.com/ga1</a></td>
</tr>
<tr>
    <td>Giorgio Agamben, <i>Creación y anarquía</i></td>
    <td><a href="http://pirateca.com">la.pirateca.com/ga2</a></td>
</tr>
<tr>
    <td>Silvestre Revueltas, <i>Silvestre Revueltas por él mismo</i></td>
    <td><a href="http://pirateca.com">la.pirateca.com/sr</a></td>
</tr>
<tr>
    <td>Jorge Luis Borges, <i>Palabras ante la tumba de Macedonio Fernández</i></td>
    <td><a href="http://pirateca.com">la.pirateca.com/jlb</a></td>
</tr>
<tr>
    <td>Antonin Artaud, <i>El cine</i></td>
    <td><a href="http://pirateca.com">la.pirateca.com/aa</a></td>
</tr>
<tr>
    <td>Tiqqun, <i>¿Cómo hacer?</i></td>
    <td><a href="http://pirateca.com">la.pirateca.com/tq1</a></td>
</tr>
<tr>
    <td><i>Manifiesta para despatriarcalizar el archivo</i></td>
    <td><a href="http://pirateca.com">la.pirateca.com/ma</a></td>
</tr>
<tr>
    <td>Marie Bardet, <i>Hacer mundos con gestos</i></td>
    <td><a href="http://pirateca.com">la.pirateca.com/mb</a></td>
</tr>
<tr>
    <td>Macedonio Fernández, <i>Museo de la Novela de la Eterna</i></td>
    <td><a href="http://pirateca.com">la.pirateca.com/mf</a></td>
</tr>
<tr>
    <td>Giorgio Agamben, <i>El uso de los cuerpos</i></td>
    <td><a href="http://pirateca.com">la.pirateca.com/ga3</a></td>
</tr>
<tr>
    <td>Michel Serres, <i>El contrato natural</i></td>
    <td><a href="http://pirateca.com">la.pirateca.com/ms</a></td>
</tr>
<tr>
    <td>Tiqqun, <i>Teoría del Bloom</i></td>
    <td><a href="http://pirateca.com">la.pirateca.com/tq2</a></td>
</tr>
<tr>
    <td>Tiqqun, <i>Introducción a la guerra civil</i></td>
    <td><a href="http://pirateca.com">la.pirateca.com/tq3</a></td>
</tr>
<tr>
    <td>Silvia Federicci, <i>Reencantar el mundo
    </td>
    <td><a href="http://pirateca.com">la.pirateca.com/sf</a></td>
</tr>
<tr>
    <td>Alexandre M. Jacob, <i>Por qué he robado y otros textos</i></td>
    <td><a href="http://pirateca.com">la.pirateca.com/aj</a></td>
</tr>
<tr>
    <td>Suely Rolnik, <i>Esferas de la insurrección</i></td>
    <td><a href="http://pirateca.com">la.pirateca.com/sr</a></td>
</tr>
<tr>
    <td>Gilles Deleuze, <i>Conversaciones</i></td>
    <td><a href="http://pirateca.com">la.pirateca.com/gd</a></td>
</tr>

</table>

# Maquinarias de escritura

## Apuntes sobre la crisis de la autoría

En esta primera mitad del siglo XXI la industria del libro está en crisis. El «desplome» es [en la producción](https://pad.programando.li/s/qTK4mA7Ys) y [en la venta](https://aristeguinoticias.com/2703/kiosko/la-caniem-solicita-apoyo-del-estado-y-lo-insta-a-que-reconozca-situacion-de-emergencia/) de publicaciones. De semejante caída se responsabiliza a un virus y a la piratería, cuyos «[efectos dañinos](https://caniem.online/mas-de-treinta-asociaciones-mundiales-firman-una-carta-para-inspirar-un-futuro-sostenible-para-el-sector-editorial/)» han afectado «los derechos de los editores y creadores de contenido».

Aunque no lo mencionen, el puente que va de quien lee a quien escribe y viceversa desde hace décadas se percibe que está en deterioro por las personas que transitan bibliotecas y librerías, como estudiantes, profesores y lectores habituales en general. En México, por ejemplo, la falta de acceso a la bibliografía ha llevado a casi todas las personas con educación media superior y superior a ser criminales ---según la opinión de estos «editores y creadores»---; es decir, a descargar gratuitamente o a fotocopiar material protegido con derecho de autor. Las páginas legales, los foros de autodenominados expertos y las ferias lo repiten hasta el cansancio: «todos los derechos reservados» y «copiar es delito».

Esta avidez incontrolada por el acceso a la información es un gesto que permite percibir que existe un interés de quien lee ---y cada vez más de quien escribe--- por tener a disposición este puente. Sin embargo, «el puente» es una metáfora que quizá no comparta semejanzas con la manera en como se ha establecido la cadena de producción y de consumo de libros.

Una manera de examinar el desfase entre la metáfora y los modos de producción de libros es a través de una figura jurídica y literaria que ha estado presente desde la época moderna de las publicaciones: la autoría. 

==TODO==

Para reproducir las maquinarias de escritura basta con que ejecutes este documento con [RuWEB](https://pad.programando.li/ruweb):

```sh x
ruweb https://pad.programando.li/K0-f6xQ5TvyHUK6xUmxcJA/download
```

```sh
echo "Hola"
```

==TODO==

## Algún cierre

En los tiempos de la reproducción mećanica de genes gracias al silicio, la energía eléctrica y una máquina de escribir «[que se convirtió en el prototipo de cada computadora concebible](https://gitlab.com/NikaZhenya/bibliografia/-/blob/master/todo/kittler2018a.pdf)», la autoría está en crisis.

## Por incorporar

¿Pueden las máquinas ser autoras? Por el contexto literario del que partimos suponemos que la autoría implica la capacidad de escribir y de leer. Pero no solo eso, para un reconocimiento social de la autoría además se sobreentiende que lo escrito contiene «unidades mínimas» de significado o de sentido inteligibles a través de la lectura. El nombre «Shakespeare» es uno de los nombres [más conocidos](https://en.wikipedia.org/wiki/List_of_best-selling_fiction_authors) relacionados a un nombre de autor. El nombre «Octavio Paz» también es un nombre de autor, que resuena menos que «Shakespeare» pero, para la literacidad general requerida entre los hispanohablantes, el conocimiento de su nombre es un requisito en diversas currículas escolares, aunque la lectura de su obra no sea una condición necesaria para su reconocimiento como nombre de autor. «Celia Amorós» también es un nombre de autor, pero en varias ocasiones su reconocimiento hace un mayor eco en el mundo hispanohablante de la filosofía, la literatura o el feminismo.

El reconocimiento de alguien como autor a través de un nombre expone una cuestión relevante: el texto escrito yace en un segundo plano debido a que lo único necesario para el reconocimiento de un nombre de autor es que _algo_ haya sido escrito. Si no hay necesidad de leer o de conocer los textos que fueron escritos para el reconocimiento efectivo de aquello cuyo nombre constituye la autoría de dichos textos, tampoco es menester revelar, encontrar, consensuar o imponer «unidades mínimas» de significado o de sentido en el texto para poder hablar de un nombre de autor. El tejido de un texto no es una condición necesaria para que un autor exista sino, a lo sumo, para verificar que tal nombre es, en efecto, un nombre de autor y, para diversos géneros o clases de crítica literaria, para constituir el supuesto sustrato por el cual un sujeto se convierte en fundamento ontológico, epistemólogico o histórico para su posterior disociación o puesta en relación con el objeto de estudio de estas críticas: la obra literaria y su presunción atributiva o apropiativa por parte de un autor.

Claro que puede argumentarse que sin texto no hay autor y, por ende, que para todo autor hay un texto que respalde su nombre sin importar que este sea ininteligible, innacesible, jamás leído o que el mismo autor nunca haya escrito. Sócrates es un autor que jamás escribió textos, pero que gracias a la atribución hecha en los diálogos platónicos, en la actualidad se le reconoce como autor de conceptos o de ideas. Platón es otro autor reconocido por aquello que fue escrito y publicado como diálogos platónicos, pese a que se ha demostrado o argumentado que algunos diálogos son [textos apócrifos](https://es.wikipedia.org/wiki/Di%C3%A1logo_plat%C3%B3nico#El_canon_antiguo). Y si Sócrates y Platón se consideran ejemplos anacrónicos de ciertas prácticas heterogéneas de escritura o de su ausencia que muestran la dificultad de vincular a un homínido con lo que se supone que es _su_ obra, tómese en cuenta el quehacer editorial y de escritura [realizado por Edith Stein](http://dadun.unav.edu/bitstream/10171/4083/1/166_3.pdf) para la publicación de _Ideas II_ y _Lecciones de  Fenomenología: la conciencia interna del tiempo_, las cuales su nombre de autor es «Husserl» y, con ello, una evidencia presente en el canón de la filosofía occidental de todos estos tipos de escrituras que aún perviven en la (re)producción del conocimiento hecha en universidades, corporaciones, organizaciones o colectivos.

Quizá aquí sean necesarias dos distinciones para evitar mayores confusiones. Cuando hablamos de «autor» por lo general hacemos referencia a por lo menos una persona en donde la atribución o apropiación de un texto se da de manera clara y unívoca. Sin embargo, una sinonimia a partir de un nombre de autor, que a su vez denota una entidad cuyo «[puntero](https://es.wikipedia.org/wiki/Puntero_(inform%C3%A1tica))» es una persona, permite obviar a estos intermediarios de la referenciación. Dichos intermediarios adquieren visibilidad cuando el deseo de trascender el nombre es insatisfecho, sea porque en realidad no conocemos ni podremos tener contacto con esa persona, sea porque esta es una entidad ficticia o sea debido a que el nombre no corresponde a las entidades que en realidad han escrito ese texto y que, si cuentan con un poco de suerte, sus nombres perviven en un crédito en la página legal, una nota al pie, una referencia u cualquier otro tipo de acotación o de reconocimiento presente en la obra del autor referenciado o en los textos de quienes la analizan críticamente. El debate entre Foucault y Barthes en torno a la muerte del autor sigue presente, pero con un matiz que delimita la posibilidad de que las máquinas sean autores: la acepción general de la autoría es más bien la excepción de la práctica editorial que en sus incesantes procesos de escritura, edición, publicación y difusión de textos también producen un nombre de autor que aglutina un conjunto de prácticas y de entidades que quedan tras bambalinas en pos de la atribución o la apropiación de las obras. Si esto es así, ¿por qué las máquinas no pueden ostentar este tipo particular de nombre propio que permite hablar de la autoría?

La segunda distinción se centra en la sinonimia entre la obra y el texto. Cuando hablamos de «texto» por lo general nos retrotraemos al proceso o al producto de tejer ideas, afecciones, palabras y cadena de caracteres. Mientras tanto, cuando hablamos de «obra» en varias ocasiones hacemos referencia a una supesta selección de textos. Si bien la problematización de los criterios de selección es necesaria para la crítica de textos o de cánones, para la constitución de un nombre de autor solo se requiere suponer la existencia de dicha selección. El enfoque se desplaza de la «selección» al «supuesto» para denotar que al hablar de «obra» hacemos referencia a algunos textos cuya existencia queda sobreentendida. En consecuencia, no todos los textos son obras pero todas las obras implican ya al menos un texto supuesto. Otra diferencia entre la obra y el texto se da con el vínculo al nombre de autor, ya que para hablar de texto no es una condición necesaria o suficiente la mención a un autor; sin embargo, al hablar de una obra se lleva a cabo una función atributiva o apropiativa entre el nombre de autor y el supuesto texto seleccionado. Este nombre puede hacer referencia a una persona o a un personaje ---como en un seudónimo---, pero también a un [objeto nulo](https://en.wikipedia.org/wiki/Null_object_pattern) ---como a alguien desconocido o que decidió permanecer en el anonimato---. Esto permite otro matiz que delimita la posibilidad de que las máquinas sean autores: como la autoría referenciada a través de una obra puede ser cualquier entidad real, ficticia o nula, porque la referencia inicial es hacia un nombre; como el otro extremo referencial de la obra es la selección del producto textual sobreentendido, ya que solo requiere suponer su existencia, ¿por qué las máquinas no pueden ser esa entidad que a través de la obra se da la atribución o la apropiación de un texto?

<figure>
    
```graphviz x
digraph tríada {
  rankdir=LR;
  edge [arrowhead=none];
  label="* socialmente reconocido";
  labeljust=left;

  subgraph cluster_autor {
    style=striped;
    label="autor*";

    subgraph cluster_sinonimia_nepa {
      style=dashed;
      label="sinonimia nombre-entidad-persona-autor";

      personas [shape=hexagon];
      entidad [shape=rect];
      real [shape=diamond];
      ficticia [shape=diamond];
      nula [shape=diamond];
      denotación [shape=plain];
      nombre [shape=rect];
      // Ver: https://stackoverflow.com/questions/65764188/graphviz-invert-two-nodes-position-inside-a-cluster
      node [label="" shape=point width=.01] 
      // bogus1 & bogus2 are needed to flip/swap/invert the funciones & producción nodes
      bogus1 bogus2;
    }
    
    subgraph cluster_fp {
      label="";
      style=invis;
      funciones [label="atribución o\napropiación", shape=plain];
      producción [shape=plain];
    }

    máquinas [shape=hexagon];

    {personas máquinas} -> real [arrowhead=normal];
    {real ficticia nula} -> entidad [arrowhead=normal];
    entidad -> denotación ->  nombre
  }

  subgraph cluster_sinonimia_oa {
    style=dashed;
    label="sinonimia obra-texto";

    subgraph cluster_texto {
      style=striped;
      label=texto;
      selección [shape=rect];
    }

    obra [shape=rect, style=striped];
    supuesto [label="supuesto\nexistencial", shape=plain];
  }

  nombre -> funciones -> obra -> supuesto -> selección;
  producción -> selección;
  entidad -> bogus1 [headclip=false];
  bogus1 -> bogus2 [tailclip=false, headclip=false];
  bogus2 -> producción [tailclip=false];
}
```

  <figcaption>Figura 1. Tríada del autor, la obra y el texto.</figcaption>
</figure>

En la conjunción de ambas sinonimias son perceptibles dos dualismos, del autor y la obra así como de la obra y el texto, y una tríada del autor, la obra y el texto. Por claridad se ha dispuesto la Figura 1, la cual requiere un mayor análisis. Por ejemplo, si la autoría es producto de un acto del habla, vale la pena preguntarse si las entidades productoras de texto forman parte del campo de la autoría o si se encuentran al exterior. Otra pregunta a plantear es si en realidad es posible trascender al nombre o si su denotación no es mas que una pretensión. Además, en [_Creación y anarquía_](https://pirateca.com/filosofia/creacion-y-anarquia-giorgio-agamben/) Agamben menciona la falta de simplicidad del término «obra», así como la posibilidad de su desaparición, donde esta queda sustituida por la actividad productiva, como en el _performance_. Esto implica que el «texto» del esquema podría ser cualquier producto ya no supuesto sino efectivo y que, en la ausencia de la «obra», la función atributiva o apropiativa se da de manera análoga a como el titular hace de la actividad productiva su propiedad, una reivindicación del dominio, según escribió Agamben, que por estar en relación con un título consiste en un acto del habla. Sin embargo, sea que no todos los elementos «entidad», «nombre», «obra» y «selección» estén presentes, que sus campos estén más delimitados a lo supuesto en un principio, como el caso del «autor», o que tengan otro nombre, como puede ser el «producto» en lugar del «texto», persisten ciertas acciones o resultados mínimos para poder establecer alguna clase de titularidad y, con ello, la posibilidad para hablar de la autoría: la producción, la apropiación y la atribución.

<!-- TODO: desarrollar más aquello de la producción, la apropiación y la atribución, principalmente la última. -->

¿Pueden las máquinas producir textos?  Si por producción de texto en una primera faceta se hace referencia a la capacidad de tejer sin importar la inteligibilidad de las «unidades mínimas» de significado o de sentido, las máquinas producen texto y lo escriben de manera incesante. Esta capacidad de escritura es tan vertiginosa que no hay homínido capaz de mantener el mismo ritmo. Por ejemplo, esta [Maquinaria de Escritura 1](https://jsfiddle.net/bwmkejc6/) ([ME-1](https://gitlab.com/-/snippets/2063472)) tiene la capacidad de escribir +150 palabras (+1,000 caracteres) por segundo. Esta cantidad puede ser mayor según los recursos de cómputo disponibles y la optimización del código. En una segunda faceta, las máquinas también son capaces de producir textos con «unidades mínimas» de significado o de sentido y lo escriben muy amenudo. En la Figura 2 hay cuatro ejemplos de maquinarias de escritura en proyectos de animación ([Synfig](https://www.synfig.org)), audio ([Audacity](https://www.audacityteam.org)), gráficos ([Inkscape](https://inkscape.org)) y publicaciones ([Scribus](https://www.scribus.net)). El _software_ guarda los datos como texto por lo general en metalenguaje [XML](https://es.wikipedia.org/wiki/Extensible_Markup_Language). Las [interfaces](https://es.wikipedia.org/wiki/Interfaz_de_usuario) de los programas pueden permitir al usuario escribir o tener otras [experiencias](https://es.wikipedia.org/wiki/Experiencia_de_usuario) de uso que las máquinas vierten en texto al guardarse los datos en un archivo; es decir, cualquier medio audiovisual, gráfico o textual puede producirse mediante la escritura. Las «unidades mínimas» yacen en ese texto estructurado en pos de su correcto procesamiento. Una consecuencia de estos procesos de escritura de textos debajo del capó es que las «unidades mínimas» pueden tener significado o sentido para las personas que lo leen o para las máquinas que lo procesan. Un antropocentrismo presente en la concepción de la producción de texto es el principal impedimento para observar que la escritura no siempre implica la lectura, o bien, que las máquinas procesan y «leen» texto.

<figure>
    
![](https://i.imgur.com/DhBUv1f.jpg)

  <figcaption>Figura 2. Maquinarias de escritura en proyectos de 1) animación, 2) audio, 3) gráficos y 4) publicaciones.</figcaption>
</figure>

En la actualidad existen varias tentativas para que las máquinas escriban con «unidades mínimas» de significado y de sentido más próximas a las expectativas de un texto literario. Los ejemplos más notables en esta tercera faceta son los textos generativos que de manera general comprenden un proceso de producción a partir de unos datos de entrada que al procesarse por una máquina dan como resultado otros textos. Algunas técnicas implementan [gramáticas libres de contexto](https://es.wikipedia.org/wiki/Gram%C3%A1tica_libre_de_contexto) por las cuales se establecen ciertas reglas de formación que al ser aplicadas producen una diversidad de textos. [Tracery](http://www.tracery.io), desarrollado por [GalaxyKate](http://www.galaxykate.com), es una herramienta de [código abierto](https://es.wikipedia.org/wiki/C%C3%B3digo_abierto) que permite la aplicación de esta técnica con el lenguaje de programación [JavaScript](https://es.wikipedia.org/wiki/JavaScript) ---no confundir con [Java](https://es.wikipedia.org/wiki/Java_(lenguaje_de_programaci%C3%B3n))---, por lo que es posible su uso en cualquier navegador _web_ moderno. Un ejemplo de programación usando Tracery es el siguiente:

```json x
{
  "origin": ["Soy #entidad# que escribe de manera #adjetivo#."],
  "entidad": ["una máquina","una persona","una computadora","un humano","un autor","Shakespeare","Octavio Paz","Celia Amorós"],
  "adjetivo": ["incesante","interesante","innovadora","extraña","técnica","fantástica","problemática","programada"]
}
```

Con esta notación de objetos en [JSON](https://es.wikipedia.org/wiki/JSON) son posibles 64 textos distintos (8 entidades × 8 adjetivos) con la siguiente muestra de resultados:

```
Soy un humano que escribe de manera programada.
Soy Shakespeare que escribe de manera problemática.
Soy una máquina que escribe de manera incesante.
Soy Octavio Paz que escribe de manera extraña.
Soy un autor que escribe de manera innovadora.
Soy Celia Amorós que escribe de manera interesante.
Soy una computadora que escribe de manera fantástica.
Soy una persona que escribe de manera técnica.
```

Tracery ofrece un tutorial y un editor en línea, además de manuales técnicos, un [_fanzine_](https://es.wikipedia.org/wiki/Fanzine) y  un artículo científico. En nuestra región, [Daniel M. Olivera](https://0xacab.org/eliand) ha experimentado con esta herramienta y ofrecido talleres como el de «[Bots literarios. Poesía narrativa por procedimientos en Tracery](https://0xacab.org/eliand/taller_bots_literarios)», impartido en el [Centro de Cultura Digital](https://www.centroculturadigital.mx) (CCD) y cuyo resultado fueron unos bots para Twitter o [Mastodon](https://joinmastodon.org), por ejemplo, [@Bestiarium2](https://twitter.com/Bestiarium2), [@abotlina](https://twitter.com/abotlina), [@bot_cholo](https://twitter.com/bot_cholo) y [@NostalgiaHipst1](https://twitter.com/NostalgiaHipst1).

<figure>

![](https://i.imgur.com/zxBMIJJ.png)

  <figcaption>Figura 3. Apartado del editor de Tracery donde son visibles las reglas de composición de los textos.</figcaption>
</figure>

Por la influencia de la metodología de Tracery y del trabajo de Olivera, la organización [Programando LIBREros](https://twitter.com/proLIBREros) desarrolló dos bots de Twitter para las campañas [#SalvemosInternet](https://twitter.com/search?q=%23SalvemosInternet&src=typed_query) y [#NiCensuraNiCandados](https://twitter.com/search?q=%23NiCensuraNiCandados&src=typed_query) impulsadas por la Red en Defensa de los Derechos Digitales ([R3D](https://r3d.mx)), Creative Commons México, Wikimedia México, [Artículo 19](https://articulo19.org), [Derechos Digitales](https://www.derechosdigitales.org), Mozilla México, [LIDSoL](https://www.lidsol.org), [Tierra Común](https://tierracomun.org) y otras ~30 entidades. Estos bots, no afiliados a las entidades mencionadas, [estuvieron activos](https://twitter.com/proLIBREros_bot) para el envío de correos con asuntos y contenidos autogenerados y firmados con el nombre de los usuarios de Twitter que hicieron RT a sus [respectivas publicaciones](https://twitter.com/proLIBREros_bot/status/1278833901478113280). Ambos bots fueron programados con [Ruby](https://es.wikipedia.org/wiki/Ruby) y sus archivos se encuentran disponibles en los [repositorios](https://gitlab.com/programando-libreros) de esta organización. El [bot](https://gitlab.com/programando-libreros/bots/salvemosinternet) para #SalvemosInternet envío ~40 correos al Instituto Federal de Telecomunicaciones (IFT) del Estado mexicano en pos de la [neutralidad de la red](https://salvemosinternet.mx) y en contra del «[Anteproyecto de lineamientos para la gestión de tráfico y administración de red](http://www.ift.org.mx/sites/default/files/industria/temasrelevantes/13791/documentos/1documentoenconsultapublicaanteproyectodelineamientos.pdf)» por considerarse que puede abrir la puerta a la censura, la priorización pagada y la invasión de la privacidad en internet. En la campaña se enviaron un total de [~150,000 correos electrónicos](https://twitter.com/R3Dmx/status/1362460032303063051), por lo cual el [18 de febrero del 2021](https://r3d.mx/2021/02/18/tribunal-concede-amparo-a-r3d-y-ordena-al-ift-emitir-lineamientos-para-proteger-la-neutralidad-de-la-red-a-mas-tardar-el-30-de-junio-de-2021/) «el Segundo Tribunal Colegiado de Circuito en Materia Administrativa, Especializado en Competencia Económica, Radiodifusión y Telecomunicaciones resolvió el Amparo en Revisión [308/2019](http://alturl.com/iqkvi) desechando los argumentos del [IFT] y ordenando a dicho órgano autónomo emitir Lineamientos para proteger la neutralidad de la red». El [bot](https://gitlab.com/programando-libreros/bots/nicensuranicandados) para #NiCensuraNiCandados envío ~200 correos a la Comisión Nacional de Derechos Humanos de México para la [interposición](https://participa.nicensuranicandados.org) de una [acción de inconstitucionalidad](https://www.cndh.org.mx/sites/default/files/documentos/2020-08/COM_2020_245.pdf) a la reforma de la Ley Federal del Derecho de Autor (LFDA), decretada el 1 de julio de 2020, «por posibles violaciones a la libertad de expresión, al derecho de propiedad, a la libertad de comercio o trabajo y a derechos culturales, entre otras prerrogativas». Gracias a esta campaña, que resultó en un envío total de [~70,000 correos electrónicos](http://alturl.com/c9gkj), la Suprema Corte de Justicia de la Nación deberá determinar la inconstitucionalidad de las reformas en los próximos meses.

Otro ejemplo de implementación técnica de gramáticas libres de contexto para la producción de textos es [Biteratura](https://github.com/srsergiorodriguez/aventura), una herramienta de código abierto desarrollada con JavaScript para generar historias interactivas. [Sergio Rodríguez Gómez](https://github.com/srsergiorodriguez) es su desarrollador y en el archivo [README](https://es.wikipedia.org/wiki/README) en español define «biteratura» como «textos literarios generados por computador». En el mismo documento se encuentra la documentación de esta herramienta así como ejemplos de un [generador de poemas](https://srsergiorodriguez.github.io/autopoeta) ([código](https://github.com/srsergiorodriguez/autopoeta/blob/master/docs/assets/autopoetadb.json)) y de una [historia interactiva](https://srsergiorodriguez.github.io/aventura/ejemplos/historia_interactiva) ([código](https://github.com/srsergiorodriguez/aventura/blob/master/docs/ejemplos/historia_interactiva/main.js)) basada en [_El dedo pulgar del ingeniero_](https://es.wikipedia.org/wiki/El_dedo_pulgar_del_ingeniero), escrito por Arthur Conan Doyle.

<figure>
    
![](https://i.imgur.com/dx4Lfbm.jpg)

  <figcaption>Figura 4. Interfaces y código de dos ejemplos de biteratura, 1) un generador de poemas y 2) una historia interactiva.</figcaption>
</figure>

La implementación de las [cadenas de Márkov](https://es.wikipedia.org/wiki/Cadena_de_M%C3%A1rkov) es otra técnica para la producción de textos generativos. Estas cadenas son un proceso que a partir de unos eventos iniciales se obtienen las probabilidades de un pŕoximo evento. Sus aplicaciones están presentes en la economía, las finanzas, la metereología, los modelos epidemiológicos, la genética, la música y, de manera general, en cualquier actividad lúdica o de investigación que requiera modelos de simulación para la predicción de eventos. Para su aplicación literaria se utilizan textos como datos de entrada que la máquina procesa para predecir y producir nuevos textos.

El proyecto editorial «[Aparato CiFi. Ciencia ficción escrita entre el río Bravo y el río Suchiate, de 1692 a 1947](http://aparatocifi.press)», bajo la coordinación de [Juan Pablo Anaya](https://www.escritores.org/biografias/12369-anaya-juan-pablo) y [Mayra Roffe](https://twitter.com/mayra_roffe) y con la asesoría legal de [Salvador Alcántar Morán](https://twitter.com/salvador_alc), es un ejemplo interdisciplinario en el que se experimentó con las cadenas de Márkov entre el 2019 y 2020. A partir de [la digitalización a PDF y la reedición en _ebooks_](https://gitlab.com/aparato-cifi/ac-libros) de los textos antologados, trabajo encomendado a Programando LIBREros, se levantó un sitio y se programaron varios bots, donde el soporte técnico fue realizado por [Canek Zapata](https://www.canekzapata.net), [Pierre Herrera](http://www.elem.mx/autor/datos/109626) y [Dorian Alexis Velázquez](https://gitlab.com/rexmalebka) y que asimismo contó con la participación de [Libia Brenda](https://twitter.com/tuitlibiesco) y [Gabriela Damián](https://es.wikipedia.org/wiki/Gabriela_Dami%C3%A1n_Miravete). Además de la impartición de un taller, llevado a cabo por [Verónica Gerber Bicecci](https://es.wikipedia.org/wiki/Ver%C3%B3nica_Gerber_Bicecci), las reediciones digitales de los textos antologados también se utilizaron para la selección de palabras que alimentan al bot [Raquel 1692](http://aparatocifi.press/raquel/scripts/static/index.html). Esta maquinaria de escritura, programada por Velázquez con [Python](https://es.wikipedia.org/wiki/Python) y [disponible públicamente](https://gitlab.com/rexmalebka/cifi-bot), genera textos de manera predictiva a partir de dicha selección mediante la implementación de las cadenas de Márkov. En su interfaz, el usuario introduce una [cadena de caracteres](https://es.wikipedia.org/wiki/Cadena_de_caracteres) y una cantidad de palabras que Raquel emplea para predecir y producir texto. Como se observa en la parte inferior de la Figura 5, a diferencia de las técnicas que implementan las gramáticas libres de contexto, en los procedimientos de las cadenas de Márkov las reglas de formación de textos no están predefinidas por la persona que programa la máquina. En su lugar, esta persona programa el algoritmo que la máquina ejecuta para formar textos según el modelo de Márkov. Las reglas para el tejido de un texto siguen presentes, pero dejan de ser definidas por una persona y, para nosotros, se prestan a arbitrariedades en su significado o en su sentido debido al carácter dinámico de las predicciones.

<figure>
    
![](https://i.imgur.com/fwMAtEu.png)

  <figcaption>Figura 5. Interfaz de Raquel 1692.</figcaption>
</figure>

[UnBotMás](https://editorial.centroculturadigital.mx/pieza/unbotliterario) es otro ejemplo de implementación técnica de las cadenas de Márkov para la producción de texto. _El libro vacío_ de [Josefina Vicens](https://es.wikipedia.org/wiki/Josefina_Vicens), _Cartucho_ de [Nellie Campobello](https://es.wikipedia.org/wiki/Nellie_Campobello), _Las vírgenes terrestres_ de [Enriqueta Ochoa](https://es.wikipedia.org/wiki/Enriqueta_Ochoa) y _Balún Canán_ de [Rosario Castellanos](https://es.wikipedia.org/wiki/Rosario_Castellanos) son utilizados como datos de entrada para la generación de publicaciones en la cuenta [@botliterario1](https://twitter.com/botliterario1) de Twitter. Este bot fue programado para la exposición _Máquinas de escritura_ que se realizó en el CCD de junio a agosto de 2017 y en la actualidad continúa publicando una vez al día.

<figure>
    
![](https://i.imgur.com/4qLjvwJ.png)

  <figcaption>Figura 6. <i>Feed</i> de Twitter de UnBotMás.</figcaption>
</figure>

Como es perceptible, esta faceta de las maquinarias de escritura aún está lejos del imaginario donde las máquinas escriben por sí solas textos con «unidades mínimas» de significado y de sentido adecuadas a las expectativas literarias de los homínidos. Los ejemplos aquí expuestos son una muestra básica de implementaciones cada vez más sofisticadas llevadas a cabo por las personas entusiastas de la [inteligencia artificial](https://es.wikipedia.org/wiki/Inteligencia_artificial) y del [_machine learning_](https://es.wikipedia.org/wiki/Aprendizaje_autom%C3%A1tico). Pese a los [importantes avances](https://openai.com/blog/better-language-models/#fn2) en este campo, la arbitrariedad de los resultados, al borde de [lo cómico](https://www.youtube.com/watch?v=LY7x2Ihqjmc&feature=emb_title), hacen que esta faceta sea recibida con [un amplio escepticismo](https://danialexis.net/2020/01/29/creativity-by-markov-chain-or-why-predictive-text-isnt-the-novel-writing-shortcut-youre-looking-for/) dentro de los gremios de escritores. No obstante, si el estado actual de las maquinarias de escritura se percibe como el presente histórico, en el que el imaginario desempeña un rol de guía, de límite o de utopía para la acción, entre su imperfección y equívocos existen ya implicaciones políticas, sociales y teóricas de esta manera de producir textos.

<!-- TODO: explicitar más estas implicaciones, principalmente las últimas dos. -->

Los bots en pos del activismo en línea son una modalidad de empleo político de la escritura. Sin embargo, se trata de una implementación minoritaria en comparación a los bots programados para la propaganda y la desinformación. El trabajo de [Wiesenberg y Tench](http://libgen.rs/scimag/10.1016%2Fj.ijinfomgt.2019.102042) sirve de punto de partida para advertir los distintos usos de los bots como son el seguimiento y el monitoreo de la actividad de los usuarios; la republicación, el reenvío o la reacción a favor o en contra de contenidos; la publicación de respuestas o de comentarios, así como la generación y la publicación de contenidos. Estos usos permiten que los bots sean utilizados para el trabajo, el comercio, la educación, el activismo y la propaganda. El uso ético o no de los bots depende de la manera en como fue programado, por lo que la reflexión ética gira en torno a los propósitos de sus propietarios y no en relación con su capacidad de automatizar tareas, como es el ejercicio de la escritura para la literatura o la desinformación. Varios investigadores como Wiesenberg y Tench han encontrado evidencia de la influencia que los bots y las «_fake news_» han tenido en, por ejemplo, [las elecciones de medio término de EE. UU. de 2010](http://libgen.rs/scimag/10.1145%2F3292522.3326016), [las elecciones federales de México de 2012](https://www.bbc.com/mundo/noticias-america-latina-44302996), [las elecciones presidenciales de Sri Lanka de 2015](http://libgen.rs/scimag/10.1177%2F0270467617736219), [el referéndum sobre la permanencia del Reino Unido en la Unión Europea de 2016 (_brexit_)](http://libgen.rs/scimag/10.1145%2F3292522.3326016), [las elecciones presidenciales de EE. UU. de 2016](http://libgen.rs/scimag/10.1145%2F3292522.3326016), [las elecciones presidenciales de Francia de 2017](http://libgen.rs/scimag/10.2139%2Fssrn.2995809), [las elecciones federales de Alemania de 2017](http://libgen.rs/scimag/10.1126%2Fscience.357.6356.1081), [el referéndum para la independencia de Cataluña de 2017](https://www.pnas.org/content/115/49/12435), [las elecciones generales de Italia de 2018](http://libgen.rs/scimag/10.1038%2Fs42005-020-0340-4), [las elecciones federales de México de 2018](https://www.forbes.com.mx/de-bots-trolls-fake-news-y-las-elecciones-2018/), [las elecciones presidenciales de Brasil de 2018](http://libgen.rs/scimag/10.1108%2Fjices-12-2019-0134) y más campañas de desinformación en al menos [48 países](http://comprop.oii.ox.ac.uk/wp-content/uploads/sites/93/2018/07/ct2018.pdf). 

En este [burdo ejemplo](https://jsfiddle.net/0x7t6n9p/) de [ME-2](https://gitlab.com/-/snippets/2066565), la cual es otra aplicación técnica de las gramáticas libres de contexto, una gramática redactada con tan solo 72 palabras y una implementación en JavaScript de menos de 100 líneas de código hacen posible obtener 61,120 publicaciones irrepetibles para la (des)información en redes sociales. Este es el [objeto](https://developer.mozilla.org/es/docs/Web/JavaScript/Guide/Trabajando_con_objectos) con las oraciones redactadas por un homínido:

```
{
  origin: ["#EsDeMexicanos ~phrase1~ o ~phrase2~", "~phrase1~ o ~phrase2~ #EsDeMexicanos", "#EsDeMexicanos ~phrase1~", "~phrase1~ #EsDeMexicanos", "#EsDeMexicanos ~phrase2~", "~phrase2~ #EsDeMexicanos"],
  phrase1: ["~verb1~ ~el1~ a todo", "a todo ~verb1~ ~el1~"],
  verb1: ["ponerle", "meterle", "colocarle"],
  el1: ["chile", "pero", "tortilla", "bolillo", "salsa", "picante", "tamal", "chela", "cerveza", "duda"],
  phrase2: ["~verb2a~ ~adjective2~ para ~verb2b~", "para ~verb2b~, ~verb2a~ ~adjective2~", "~verb2b~ ~adjective2~ para ~verb2a~", "para ~verb2a~, ~verb2b~ ~adjective2~"],
  verb2a: ["comer", "reírse", "tomar", "llorar", "gritar"],
  verb2b: ["cotorrear", "salir", "enfiestarse", "disfrutar", "ser feliz"],
  adjective2: ["mucho", "un chingo", "demasiado", "un montón", "en exceso"]
}
```

Cada [cadena de caracteres](https://es.wikipedia.org/wiki/Cadena_de_caracteres) entre virguillas (`~`) se utiliza como [clave](https://es.wikipedia.org/wiki/Base_de_datos_clave-valor) para otro [conjunto](https://es.wikipedia.org/wiki/Vector_(inform%C3%A1tica)) del mismo objeto que la ME-2 sustituye por su [valor](https://es.wikipedia.org/wiki/Base_de_datos_clave-valor). Los niveles de anidamiento son ilimitados, aunque en esta implementación se usaron tres, por ejemplo, `~phrase1~ ⇒ ~verb1~ ⇒ ponerle`. En esta aplicación se descuidó el entrelazamiento entre las palabras y las claves; no obstante, con 72 palabras esta maquinaria de escritura produce 654,400 palabras o 2,618 cuartillas de 250 palabras cada una en un par de segundos. En una conjunción de las capacidades de dos entidades distintas, una máquinaria incesante de escritura y un homínido que introduce sus reglas de formación, una computadora personal tiene la capacidad de producir textos con «unidades mínimas» de significado o de sentido inteligibles a través de la lectura. Las máquinas no requieren de un «despertar» para «programarnos», ya lo hacen según los intereses de una minoría titular de su código y propietaria de las infraestructuras necesarias para la propagación de los contenidos. No hay legislación, ni siquiera la nueva reforma a la LFDA, que regule la actual capacidad de producción de textos en pos de la [influencia de la opinión pública](http://libgen.rs/scimag/10.1016%2Fj.physa.2020.124163).

<figure>
    
![](https://i.imgur.com/pnz5Gie.png)

  <figcaption>Figura 7. Mil muestras aleatorias de 61,120 publicaciones únicas producidas por ME-2 con una gramática de 72 palabras.</figcaption>
</figure>

Con mucha probabilidad la LFDA no es la legislación apropiada para este tipo de regulaciones sino tal vez alguna cuya aplicación administrativa corresponda al IFT. Sin embargo, la posibilidad de amplificación de un solo discurso a través de las nuevas tecnologías de la información y la comunicación sí queda protegida por esta legislación. En los [artículos 12 y 13](http://www.diputados.gob.mx/LeyesBiblio/pdf/122_010720.pdf) (A12-3) de la LFDA se establece a la persona titular del código, por ejemplo, la programadora de bots o su empleador, como el autor y _su_ obra. En esta legislación, el «autor» es la persona física _creadora_ de obras (A12), por lo cual los derechos patrimoniales le pertenecen (A25) o son compartidos con su empleador (A26;84;103), así como la «obra» es la _creación original_ (A3), la cual se otorga su protección cuando se fija en un soporte material y sin necesidad del cumplimiento de alguna formalidad (A5). Por obra original en este contexto se entiende a que no es derivada de otra obra y, por ello, que no cuenta con un precedente o, de tenerlo, que la producción se diferencia de manera suficiente al punto de permitir la afirmación de su originalidad (A4CI).

Para las legislaciones actuales las máquinas no son autores ya que estas no son consideradas sujetos jurídicos sino un objeto _creado_ por un autor. No obstante, varios autores socialmente reconocidos tampoco lo son bajo estos términos. Por ejemplo, un sócrates contemporáneo, es decir, un autor de conceptos o de ideas pero no de textos, los cuales son los soportes materiales bajo protección, no puede gozar de las garantías concedidas por la LFDA. Una stein contemporánea, una escritora que parte de una selección de textos, como un conjunto de notas de otra persona, tampoco sería considerada una autora según la LFDA si no hay condiciones suficientes que afirmen su diferenciación. El autor de esas notas, un husserl, podría considerarse el autor de las obras escritas por una stein si y solo si se obvian dos hechos: que este husserl no escribió esos soportes materiales y que lo escrito por esta stein carece de originalidad.

Una consecuencia de ambas eludiciones es que, para dar paso a la autoría en un sentido jurídico, la producción hecha por un solo individuo, como sucede de manera regular con la escritura, no sería la intermediaria entre la creación de ideas o del discurso y su fijación en un soporte material unitario y divulgado públicamente. ¿Cuál es, entonces, la actividad de un husserl que permite ir de una creación de notas a una obra suya, si a través de su selección, disposición y reescritura una stein produce el texto en ausencia de un contrato? ¿Dónde quedan los procesos editoriales y de escritura únicos e irrepetibles de una stein que permiten al lector percibir una hilación de argumentos en la obra de ese husserl en lugar de una concatenación de notas? Si el nexo suficiente o necesario entre la creación y la fijación no es la producción individualizada del soporte material, ¿se está hablando aún de una actividad ejercida por un sujeto que puede demostrarse y protegerse legalmente o más bien de una presuposición sobre la producción cultural, por ejemplo, las ideas que rigen la concepción antropocéntrica y egocéntrica del acto de escritura? En una legislación que requiere de un nexo unívoco entre creación, producción y fijación para la garantía de unos derechos, hay una laguna si, mediante una suposición en lugar de una demostración, un husserl escritor de notas se considera el único autor de una obra escrita sin contrato por una stein.

<!-- TODO: agregar más consecuencias. -->

Con las definiciones de «autor» como «creador» y de «obra» como «creación original fijada» la LFDA evita el equívoco social y recurrente de suponer que el derecho de autor para nuestro caso, o el _copyright_ para el contexto anglosajón, pretende proteger a las ideas como propiedad, en lugar de un objeto tangible. Sin embargo, estos actos del habla son en realidad un traslado del significado y del sentido de estas nociones a una concepción de la «creación» que en la actualidad se reproduce en la convivencia social. La introducción en el discuro del término «creación» no fue fortuito ni inmediato ni reciente. En palabras escritas por Agamben, «su condición de posibilidad se da» entre el fin del mundo clásico y el inicio del medieval, donde la «fuerza de esta concepción» consistió en que «tenía su modelo en la creación divina». Aunque en un contexto secularizado es posible hablar de creación sin hacer mención a una divinidad, hoy en día el modelo persiste con vigor porque sin un remanente teológico o metafísico que la respalde, la creación es un significante vacío si se percibe allende a la producción e imprescindible para la constitución de la autoría.

Por ejemplo, Agamben nos narra que, en seguimiento con Tomás de Aquino, dios creó al mundo conforme a una idea preexistente en su mente. En la LFDA está patente una presuposición social que percibe a la producción cultural como la materialización de ideas preexistentes en una mente. Debido a que el derecho positivo no puede legislar las conciencias, la creación por sí misma no tiene significado ni sentido jurídico al menos que se complemente con las nociones de «originalidad» y de «fijeza». El sintagma «creación original fijada» da condiciones de posibilidad para el ejercicio del derecho positivo, aunque en su camino también traslada el significado y el sentido de la «originalidad» y la «fijeza» a las sociedades sobre las que hace efectivo su dominio.

Otro ejemplo es el supuesto presente en los derechos morales por el cual la obra es el fruto «único, primigenio y perpetuo» de un creador (A18). Estos derechos tienen [su antecedente](https://maestria.perrotuerto.blog/tesis/html/tesis.html?v=bce867fa#del-problema-de-la-interpretacion-al-de-la-teorizacion) en las interpretaciones de juristas decimonónicos en torno a Kant y Hegel. Para el primer caso, estos derechos se fundan a través de [una distinción kantiana](https://pad.programando.li/s/Sy-1K3WYw) entre _opus_ (el libro o el texto) y _opera_ (el discurso), que a su vez es [una referencia expresa](https://www.scielo.br/scielo.php?script=sci_arttext&pid=S0101-31732006000200002) a las virtudes aristotélicas del alma. Para la segunda referencia, los derechos morales se constituyen gracias a [una concepción hegeliana](https://maestria.perrotuerto.blog/tesis/html/tesis.html?v=bce867fa#la-teoria-personalista-hegel-kant-y-mas-hegel) sobre la externalización de la voluntad que permite a la persona [existir como Idea](http://tomgpalmer.com/wp-content/uploads/papers/palmer-morallyjustified-harvard-v13n3.pdf). Sin la inyección en el discurso de conceptos como «virtud», «alma», «voluntad» o «Idea», no hay una explicación efectiva que sustente la unicidad y especificidad del vínculo entre el autor y su obra en un contexto donde también cada lector establece un nexo único e irrepetible con los objetos de su lectura y donde el productor del texto puede ser una persona distinta al creador y autor de una obra.

<!-- TODO: extender y sistematizar ejemplos donde la creación es un significante vacío sin un remantente teológico o metafísico. -->

La creación se perfila con un idealismo que raya en la fantasía de un arquetipo divino. Sin embargo, una noción materialista de la creación es posible. La creatividad puede considerarse como un proceso productivo indiscernible para un momento histórico en específico, un proceso en el que todavía es difícil distinguir los elementos y sus interacciones que lo constituyen. Se trataría de una incógnita que permite pasar de una fase productiva A a una B, una interrogante que es resultado de nuestra incapacidad de inteligir la manera en como llega a ser ese cambio cuantitativo o cualitativo. No hay en la creación inserta en la producción la suposición de una ininteligibilidad trascendental análoga a nuestra insuficiencia o insatisfacción de comprender «la creación del mundo» a partir de un modelo preexistente en la mente de un arquitecto divino. Tampoco estaría presente el género masculino, pretendidamente neutro, cada vez que se invoca a semejante divinidad. La creación no deja de ser una actividad ejecutada por una entidad, un ejercicio fascinante que genera nuevos nexos entre ideas y conceptos o que da valor a bienes, solo se le asigna una marca temporal y espacial como actividad productiva, como un proceso parte del incesante curso del quehacer cultural.

No obstante, esta noción materialista de la creación desata varias consecuencias. Si la creación es un proceso productivo indiscernible, quiere entonces decir que hay ocasiones en la producción donde habrá un salto cuantitativo o cualitativo que en un momento histórico determinado no puede ser explicado sino supuesto. En esa situación, el nexo productivo entre una entidad y una selección de textos (Figura 1) más bien sería un supuesto productivo: ¡eureka!, hay producción aunque en la actualidad no exista la granularidad suficiente para comprenderla. El resultado de esta arbitrariedad es una noción no mecanicista de la producción y los procesos que la componen, por lo que surge la exigencia de reflexionar su significado y sentido. En los datos de entrada y las reglas de formación por las cuales las máquinas producen textos se llega a la arbitrariedad de sus resultados y a la indeterminación de sus procesos. Para los homínidos es claro que no hay arquetipo divino que intervenga en este modelo productivo. Sin embargo, la bruma aparece en el horizonte conforme nos aproximamos al mar abierto por el pujante avance de la capacidad de cómputo, hasta el punto donde no hay claridad sobre cómo es que se da este modelo confeccionado por las máquinas. La producción indiscernible de las máquinas es un «evento» creativo que no exige «inteligencia» para que las máquinas puedan ser autoras. Una posible consecuencia de gran calado es que esta concepción materialista de la creación deja de ser teológica y metafísica, pero también humana.

En los [tiempos antiintelectuales](https://pad.programando.li/s/ryONAasrv) de un mundo envuelto en una crisis global que delante suyo avanza el cambio climático y de unos homínidos que poco a poco empiezan a normalizar y hasta exigir la existencia de aparatos productores de cuerpos, poco a poco comienzan a establecerse las condiciones de posibilidad donde el «creador» y la «creación original fijada» de la LFDA apunte a las máquinas. En medio de esa referenciación se entreve la disputa entre los juristas, los legisladores y el público de nuestro tiempo sobre su estatus jurídico. ¿Pueden las máquinas ser un sócrates, una stein o un husserl? Quizá el debate del derecho de autor para el siglo XXI no será sobre un derecho a la subsistencia a través de la producción, la apropiación y la atribución, sino la punta de lanza del derecho de las máquinas a ser sujetos, el caballo de troya que desarticulará la actual fantasía antropocéntrica presente en la escritura, la autoría y el derecho de autor.

# Cierre